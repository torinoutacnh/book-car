import "./configPage.scss";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import AuthContext from "context/AuthContext";
import { CarClassType } from "interface/car";
import { ConfigContext } from "provider";
import { UpdateListConfig } from "./updateListConfig";
import { useContext, useState } from "react";
import { publicEndpoints } from "routers/apiEndpoints";
import { Button } from "@mui/material";
import { UpdatePriceConfig } from "./updatePriceConfig";

function ConfigPage() {
    const { auth } = useContext(AuthContext);
    const { configCar } = useContext(ConfigContext)

    const [isShowUpdate, setIsShowUpdate] = useState(false)
    const [titleUpdateConfig, setTitleUpdateConfig] = useState("")
    const [pathApi, setPathApi] = useState("")
    const [keyList, setKeyList] = useState("")
    const [labelUpdate, setLabelUpdate] = useState("")
    const [listUpdate, setListUpdate] = useState<string[]>([])

    const onClickShowUpdateConfig = (title: string, path: string, key: string, label: string, list: string[]) => {
        setIsShowUpdate(true)
        setTitleUpdateConfig(title)
        setPathApi(path)
        setKeyList(key)
        setLabelUpdate(label)
        setListUpdate(list)
    }

    const onClickCloseUpdateConfig = () => {
        setIsShowUpdate(false)
        setTitleUpdateConfig("")
        setPathApi("")
        setKeyList("")
        setLabelUpdate("")
        setListUpdate([])
    }

    const [isShowUpdatePrice, setIsShowUpdatePrice] = useState(false)
    const onClickShowUpdatePrice = () => {
        setIsShowUpdatePrice(true)
    }
    const onClickCloseUpdatePrice = () => {
        setIsShowUpdatePrice(false)
    }

    return (
        <div className='configPage'>

            <div>

                <table>
                    <thead>
                        <tr>
                            <th>Phân khúc</th>
                            <th>Ngày thường (VND/Ngày)</th>
                            <th>Cuối tuần (VND/Ngày)</th>
                            <th>Ngày lễ (VND/Ngày)</th>
                            <th>số km giới hạn trong tháng</th>
                            <th>Vượt giới hạn km (VND/Km)</th>
                            <th>Vượt giới hạn giờ (VND/Giờ)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {configCar?.carCreate?.map((item, index) => (
                            <tr >
                                <td><p className='text-center'>{CarClassType[item.carClass as number]}</p></td>
                                <td><p className='text-center'>{item.generalInfo.priceForNormalDay.toLocaleString()}</p></td>
                                <td><p className='text-center'>{item.generalInfo.priceForWeekendDay.toLocaleString()}</p></td>
                                <td><p className='text-center'>{item.generalInfo.priceForHoliday.toLocaleString()}</p></td>
                                <td><p className='text-center'>{item.generalInfo.limitedKmForMonth.toLocaleString()}</p></td>
                                <td><p className='text-center'>{item.generalInfo.pricePerKmExceed.toLocaleString()}</p></td>
                                <td><p className='text-center'>{item.generalInfo.pricePerHourExceed.toLocaleString()}</p></td>
                            </tr>
                        ))}

                    </tbody>
                </table>

                <div className="text-center w-full mt-[15px] pb-3">
                    <Button
                        variant="outlined"
                        size="small"
                        startIcon={<BorderColorIcon />}
                        className="whitespace-nowrap min-w-[100px]"
                    >
                        Cập nhật
                    </Button>
                </div>
                <hr />

            </div>


            <div className='grid grid-cols-2 md:grid-cols-5 gap-3 mt-5'>

                <div className='tbl-config-page'>
                    <h3>Hãng xe
                        <span
                            onClick={() => {
                                onClickShowUpdateConfig(
                                    "Cập nhật hãng xe",
                                    publicEndpoints.updateBrand,
                                    "carBrandConfigs",
                                    "Hãng xe",
                                    configCar.carBrand
                                )
                            }}
                        >
                            <BorderColorIcon />
                        </span>
                    </h3>
                    <ul>{configCar?.carBrand?.sort().map((item, index) => (<li key={index}>{index + 1}. {item}</li>))}</ul>
                </div>

                <div className='tbl-config-page'>
                    <h3>Kiểu dáng xe
                        <span
                            onClick={() => {
                                onClickShowUpdateConfig(
                                    "Cập nhật kiểu dáng",
                                    publicEndpoints.updateStyle,
                                    "carStyleConfigs",
                                    "Kiểu dáng",
                                    configCar.carStyle
                                )
                            }}
                        >
                            <BorderColorIcon />
                        </span>
                    </h3>
                    <ul>{configCar?.carStyle?.sort().map((item, index) => (<li key={index}>{index + 1}. {item}</li>))}</ul>
                </div>

                <div className='tbl-config-page col-span-2 md:col-span-1'>
                    <h3>Màu xe
                        <span
                            onClick={() => {
                                onClickShowUpdateConfig(
                                    "Cập nhật màu xe",
                                    publicEndpoints.updateColor,
                                    "carColorConfigs",
                                    "Màu xe",
                                    configCar.carColor
                                )
                            }}
                        >
                            <BorderColorIcon />
                        </span>
                    </h3>
                    <ul>{configCar?.carColor?.sort().map((item, index) => (<li key={index}>{index + 1}. Màu {item}</li>))}</ul>
                </div>

                <div className='tbl-config-page col-span-2'>
                    <h3>Bãi đậu xe
                        <span
                            onClick={() => {
                                onClickShowUpdateConfig(
                                    "Cập nhật bãi đậu xe",
                                    publicEndpoints.updateParkingLot,
                                    "carParkingLotConfigs",
                                    "Bãi đậu xe",
                                    configCar.carParkingSlot
                                )
                            }}
                        >
                            <BorderColorIcon />
                        </span>
                    </h3>
                    <ul>{configCar?.carParkingSlot?.sort().map((item, index) => (<li key={index}>{index + 1}. {item}</li>))}</ul>
                </div>

            </div>

            <div className='grid grid-cols-2 md:grid-cols-3 gap-3 md:mt-5 xs: mt-4'>

                <div className='tbl-config-page'>
                    <h3>Nhiên liệu sử dụng
                        <span
                            onClick={() => {
                                onClickShowUpdateConfig(
                                    "Cập nhật nhiên liệu sử dụng",
                                    publicEndpoints.updateFuelUse,
                                    "carFuelUseConfigs",
                                    "Nhiên liệu sử dụng",
                                    configCar.carFuelUse
                                )
                            }}
                        >
                            <BorderColorIcon />
                        </span>
                    </h3>
                    <ul>{configCar?.carFuelUse?.sort().map((item, index) => (<li key={index}>{item}</li>))}</ul>
                </div>

                <div className='tbl-config-page'>
                    <h3>Chỗ ngồi
                        <span
                            onClick={() => {
                                onClickShowUpdateConfig(
                                    "Cập nhật số chỗ ngồi",
                                    publicEndpoints.updateSeat,
                                    "carSeatConfigs",
                                    "số chỗ ngồi",
                                    configCar.carSeat
                                )
                            }}
                        >
                            <BorderColorIcon />
                        </span>
                    </h3>
                    <ul>{configCar?.carSeat?.map((item, index) => (<li key={index}>{item} chỗ</li>))}</ul>
                </div>

                <div className='tbl-config-page col-span-2 md:col-span-1'>
                    <h3>Phương thức truyền động
                        <span
                            onClick={() => {
                                onClickShowUpdateConfig(
                                    "Cập nhật phương thức truyền động",
                                    publicEndpoints.updateTransmission,
                                    "carTransmissionConfigs",
                                    "phương thức truyền động",
                                    configCar.carTransmission
                                )
                            }}
                        >
                            <BorderColorIcon />
                        </span>
                    </h3>
                    <ul>{configCar?.carTransmission?.sort().map((item, index) => (<li key={index}>{item}</li>))}</ul>
                </div>

            </div>

            <UpdateListConfig
                stateProps={isShowUpdate}
                close={onClickCloseUpdateConfig}
                title={titleUpdateConfig}
                pathApi={pathApi}
                keyList={keyList}
                label={labelUpdate}
                listConfig={listUpdate}
            />

            <UpdatePriceConfig
                close={onClickCloseUpdatePrice}
                stateProps={isShowUpdatePrice}
            />

        </div>
    );
}

export default ConfigPage;