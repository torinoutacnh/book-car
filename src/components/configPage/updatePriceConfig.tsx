import { Alert, Box, Button, FormControl, FormControlLabel, IconButton, InputLabel, MenuItem, Modal, Radio, RadioGroup, Select, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext, FormEvent, ChangeEvent, createElement, useRef } from "react";
import "./configPage.scss"
import TextField from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { useForm } from "react-hook-form";
import { RoleUser } from "interface/authenticate";
import UploadFile from "@mui/icons-material/UploadFile";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import { Contract } from "interface/contract";
import { contractGroupEndpoints, publicEndpoints } from "routers/apiEndpoints";
import { ConfigContext } from "provider";
import { useMutation } from "react-query";
import { CarClassType, CarCreateConfig, ConfigCar, GeneralInfoCar } from "interface/car";

interface PropsUpdatePriceConfig {
    stateProps?: boolean,
    close?: any
}

export const UpdatePriceConfig = (props: PropsUpdatePriceConfig) => {
    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 3,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const { refetchConfig } = useContext(ConfigContext)
    const [isShow, setIsShow] = useState(false);
    const [price, setPrice] = useState<CarCreateConfig[]>()
    const [count, setCount] = useState(0)

    useEffect(() => {
        setIsShow(props.stateProps);
        (async () => {
            const res = await fetch(process.env.REACT_APP_API + publicEndpoints.config, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    // Authorization: "Bearer " + auth.accessToken,
                },
            })
            const data = await res.json();

            if (!res.ok) {
                return
            }
            setPrice(data.data.carCreate)
        })()
    }, [props.stateProps, count])

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }



    const { auth } = useContext(AuthContext);


    const updateConfig = async () => {

        const res = fetch(process.env.REACT_APP_API + publicEndpoints.updateCarCreate, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify({ carCreate: price })
        })
        return res
    }

    const mutationUpdateConfig = useMutation(updateConfig, {
        onSuccess: async (res, variables, context) => {
            const data = await res.json()
            if (!res.ok) {
                // console.log("!ok update config => ", data)
                handleOpenNotify(data.message, "error")
            }
            else {
                refetchConfig()
                setCount(count + 1)
                handleOpenNotify("Cập nhật thành công", "success")
            }
            props?.close()
        },
        onError: (error, variables, context) => {
            // console.log("error update config => ", error)
            props?.close()
        }
    })


    const handleOnChange = (key: keyof GeneralInfoCar, data: CarCreateConfig, event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {

        data.generalInfo[key] = Number(event.target.value)

        setPrice([
            ...price.filter(item => item.carClass !== data.carClass),
            data
        ])
    }

    return (
        <>

            {isShow ?
                <Modal
                    open={isShow}
                    onClose={() => { props.close(); }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box
                        className="Box_update_config"
                        sx={style}
                        component="form"
                        noValidate
                        autoComplete="off"
                    >
                        <Typography id="modal-modal-title" variant="h6" component="h2" className="title-modal">
                            Cập nhật giá tạo xe
                        </Typography>

                        <div className="listPriceCreate">
                            {
                                price?.sort((a, b) => Number(a.carClass) - Number(b.carClass)).map((item, index) => {
                                    return (<>
                                        <div className="itemPriceCreate">
                                            <span>Hạng xe: {CarClassType[Number(item.carClass)]}</span>
                                            <TextField
                                                className="TextField"
                                                type={"number"}
                                                id="outlined-basic"
                                                label={"Giới hạn km/tháng"}
                                                variant="outlined"
                                                size="small"
                                                value={item.generalInfo.limitedKmForMonth}
                                                onChange={(event) => { handleOnChange("limitedKmForMonth", item, event) }}
                                            />
                                            <TextField
                                                className="TextField"
                                                type={"number"}
                                                id="outlined-basic"
                                                label={"Giá thuê ngày lễ"}
                                                variant="outlined"
                                                size="small"
                                                value={item.generalInfo.priceForHoliday}
                                                onChange={(event) => { handleOnChange("priceForHoliday", item, event) }}
                                            />
                                            <TextField
                                                className="TextField"
                                                type={"number"}
                                                id="outlined-basic"
                                                label={"Giá thuê tháng"}
                                                variant="outlined"
                                                size="small"
                                                value={item.generalInfo.priceForMonth}
                                                onChange={(event) => { handleOnChange("priceForMonth", item, event) }}
                                            />
                                            <TextField
                                                className="TextField"
                                                type={"number"}
                                                id="outlined-basic"
                                                label={"Giá thuê ngày thường"}
                                                variant="outlined"
                                                size="small"
                                                value={item.generalInfo.priceForNormalDay}
                                                onChange={(event) => { handleOnChange("priceForNormalDay", item, event) }}
                                            />
                                            <TextField
                                                className="TextField"
                                                type={"number"}
                                                id="outlined-basic"
                                                label={"Giá thuê cuối tuần"}
                                                variant="outlined"
                                                size="small"
                                                value={item.generalInfo.priceForWeekendDay}
                                                onChange={(event) => { handleOnChange("priceForWeekendDay", item, event) }}
                                            />

                                            <TextField
                                                className="TextField"
                                                type={"number"}
                                                id="outlined-basic"
                                                label={"Giá vượt thời gian"}
                                                variant="outlined"
                                                size="small"
                                                value={item.generalInfo.pricePerHourExceed}
                                                onChange={(event) => { handleOnChange("pricePerHourExceed", item, event) }}
                                            />
                                            <TextField
                                                className="TextField"
                                                type={"number"}
                                                id="outlined-basic"
                                                label={"Giá vượt km"}
                                                variant="outlined"
                                                size="small"
                                                value={item.generalInfo.pricePerKmExceed}
                                                onChange={(event) => { handleOnChange("pricePerKmExceed", item, event) }}
                                            />
                                            <br />
                                        </div>

                                    </>)
                                })
                            }
                        </div>

                        <br />
                        <Button onClick={() => mutationUpdateConfig.mutate()} startIcon={<UploadFile />}>Cập nhật</Button>
                        <Button onClick={() => { props.close(); setCount(count + 1) }} color="warning" endIcon={<CloseIcon />} style={{ marginLeft: "5px" }}>Huỷ bỏ</Button>
                    </Box>
                </Modal>
                : <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}