import { Alert, Box, Button, FormControl, FormControlLabel, IconButton, InputLabel, MenuItem, Modal, Radio, RadioGroup, Select, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext, FormEvent, ChangeEvent, createElement, useRef } from "react";
import "./configPage.scss"
import TextField from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { useForm } from "react-hook-form";
import { RoleUser } from "interface/authenticate";
import UploadFile from "@mui/icons-material/UploadFile";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import { Contract } from "interface/contract";
import { contractGroupEndpoints, publicEndpoints } from "routers/apiEndpoints";
import { ConfigContext } from "provider";
import { useMutation } from "react-query";

interface PropsUpdateConfig {
    stateProps?: boolean,
    close?: any,
    title: string,
    pathApi: string,
    keyList: string,
    label: string,
    listConfig: string[]

}

export const UpdateListConfig = (props: PropsUpdateConfig) => {
    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 3,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const { configCar, refetchConfig } = useContext(ConfigContext)
    const [isShow, setIsShow] = useState(false);
    const [listConfigCar, setListConfigCar] = useState<string[]>()
    const [input, setInput] = useState("")


    const [state, setState] = useState<PropsUpdateConfig>()

    useEffect(() => {
        setIsShow(props.stateProps)
        setListConfigCar(configCar?.carBrand)


        const resetValue = {
            title: props?.title,
            pathApi: props?.pathApi,
            keyList: props?.keyList,
            label: props?.label,
            listConfig: props?.listConfig
        }
        setState(resetValue)
    }, [props.stateProps, configCar])

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }



    const { auth } = useContext(AuthContext);

    const handleAdd = () => {
        setState({
            ...state,
            listConfig: [...state.listConfig, input]
        })
        setInput("")
    }

    const handleDelete = (value: string, index: number) => {
        const listTmp = state?.listConfig?.filter((item, i) => (item !== value && i !== index))
        setState({
            ...state,
            listConfig: listTmp
        })
    }

    const updateConfig = async () => {
        const dataUpdate: any = {}

        dataUpdate[state?.keyList] = state.listConfig

        const res = fetch(process.env.REACT_APP_API + state?.pathApi, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(dataUpdate)
        })
        return res
    }

    const mutationUpdateConfig = useMutation(updateConfig, {
        onSuccess: async (res, variables, context) => {
            const data = await res.json()
            if (!res.ok) {
                // console.log("!ok update config => ", data)
                handleOpenNotify(data.message, "error")
            }
            else {
                refetchConfig()
                handleOpenNotify("Cập nhật thành công", "success")
            }
            props?.close()
        },
        onError: (error, variables, context) => {
            // console.log("error update config => ", error)
            props?.close()
        }
    })

    return (
        <>


            {isShow ?
                <Modal
                    open={isShow}
                    onClose={() => props.close()}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box
                        className="Box_update_config"
                        sx={style}
                        component="form"
                        noValidate
                        autoComplete="off"
                    >
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center", minWidth: "300px" }}>
                            {state?.title}
                        </Typography>
                        <br />
                        <div className="boxInput">
                            <TextField
                                type={state?.pathApi === publicEndpoints.updateSeat ? "number" : "text"}
                                id="outlined-basic"
                                label={state?.label}
                                variant="outlined"
                                size="small"
                                value={input}
                                onChange={(event) => { setInput(event.target.value) }}
                            />
                            <Button
                                variant="outlined"
                                onClick={() => handleAdd()}
                            >
                                Thêm
                            </Button>
                        </div>
                        <br />
                        <div className="list">
                            {

                                state?.listConfig?.map((item, index) => {
                                    return (
                                        <div className="item" key={index}>
                                            <span className="title">{item}</span>
                                            <IconButton
                                                aria-label="delete" color="error"
                                                onClick={() => { handleDelete(item, index) }}
                                            >
                                                <DeleteIcon />
                                            </IconButton>
                                        </div>
                                    )
                                })
                            }
                        </div>

                        <br />
                        <Button onClick={() => mutationUpdateConfig.mutate()} startIcon={<UploadFile />}>Cập nhật</Button>
                        <Button onClick={() => { props?.close() }} color="warning" endIcon={<CloseIcon />} style={{ marginLeft: "5px" }}>Huỷ bỏ</Button>
                    </Box>
                </Modal>
                : <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}