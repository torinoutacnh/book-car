import React, { useContext, useEffect, useState } from 'react';
import dayjs, { Dayjs } from 'dayjs';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import './index.scss'
import Grid from '@mui/material/Grid';
import { MonthPicker } from '@mui/x-date-pickers/MonthPicker';
import { YearPicker } from '@mui/x-date-pickers/YearPicker';
import { Calendar } from 'interface/calendar';
import { publicEndpoints } from 'routers/apiEndpoints';
import { useQuery } from 'react-query';
import AuthContext from 'context/AuthContext';
import { ViewCalendar } from './viewCalendar';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import PivotTableChartIcon from '@mui/icons-material/PivotTableChart';
import { IconButton } from '@mui/material';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';

const minDate = dayjs('2000-01-01T00:00:00.000');
const maxDate = dayjs('2050-01-01T00:00:00.000');

export const AdminCalendar = () => {

    const [date, setDate] = React.useState<Dayjs | null>(dayjs());
    const [dayOfMonth, setDayOfMonth] = useState<number[]>()
    const { auth } = useContext(AuthContext)
    const [listReport, setListReport] = useState<Calendar[]>()
    const [viewCalendar, setViewCalendar] = useState<Calendar>()
    const [isTable, setIsTable] = useState(false)
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    const [isShowCalendar, setIsShowCalendar] = useState(false);
    const onClickShowCalendar = (calendar: Calendar) => {
        setViewCalendar(calendar)
        setIsShowCalendar(true);
    }
    const onClickCloseCalendar = () => {
        setViewCalendar(null)
        setIsShowCalendar(false);
    }
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    const ArrDayOfWeek = ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"];

    const getReport = async (): Promise<Calendar[]> => {
        const res = await fetch(process.env.REACT_APP_API + publicEndpoints.getReportContract, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
        })

        const data = await res.json()
        if (!res.ok) { throw new Error() }
        return data.data
    }

    const { data, isLoading, isError } = useQuery({
        queryKey: "getReport",
        queryFn: () => getReport(),
    })


    useEffect(() => {
        const number = date.daysInMonth()
        const arr: number[] = []
        for (let tmp = 1; tmp <= number; tmp++) {
            arr.push(tmp)
        }
        setDayOfMonth(arr)

        if (!data) { return }

        const year = date.year()
        const month = (date.month() + 1) < 10 ? `0${date.month() + 1}` : `${date.month() + 1}`
        const fil = `${year}-${month}`

        const list: Calendar[] = []
        data.map((item, index) => {

            const rentFrom = item?.rentFrom?.slice(0, 7)
            const rentTo = item?.rentTo?.slice(0, 7)
            const dateReceive = item?.dateReceive?.slice(0, 7)
            const dateTransfer = item?.dateTransfer?.slice(0, 7)

            if (rentFrom === fil || rentTo === fil || dateReceive === fil || dateTransfer === fil) {
                list.push(item)
            }

        })
        // console.log(list)
        setListReport(list)

    }, [date, data])

    return (

        <Grid container>
            <Grid item xs={12} sm={12} md={4} lg={2.5} className="container_left p-0 overflow-hidden">
                <div className='box_time'>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <div className='item_time'>
                            <h6 className='titlePicker'>Chọn năm</h6>
                            <YearPicker
                                className='YearPicker'
                                date={date}
                                minDate={minDate}
                                maxDate={maxDate}
                                onChange={(newDate) => setDate(newDate)}
                            />
                        </div>
                    </LocalizationProvider>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <div className='item_time'>
                            <h6 className='titlePicker'>Chọn tháng</h6>
                            <MonthPicker
                                className='MonthPicker'
                                date={date}
                                minDate={minDate}
                                maxDate={maxDate}
                                onChange={(newDate) => setDate(newDate)}
                            />
                        </div>
                    </LocalizationProvider>
                </div>

                <div className='box_description'>
                    <h6 className='description'>Ngày thuê</h6>
                    <h6 className='description'>Ngày trả</h6>
                    <h6 className='description'>Ngày vận chuyển</h6>
                    <h6 className='description'>Ngày nhận xe</h6>
                </div>

            </Grid>

            <Grid item xs={12} sm={12} md={8} lg={9.5} className="container_right">
                <IconButton aria-label="Switch" color="primary" onClick={() => setIsTable(!isTable)}>
                    {isTable ? <CalendarMonthIcon /> : <PivotTableChartIcon />}
                </IconButton>

                {
                    !isTable
                        ?
                        <Grid container className='listDay'>
                            {
                                dayOfMonth?.map((day, index) => {
                                    const year = date.year()
                                    const month = (date.month() + 1) < 10 ? `0${date.month() + 1}` : `${date.month() + 1}`
                                    const d = day < 10 ? `0${day}` : `${day}`
                                    const new_date = new Date(`${year}-${month}-${d}`)
                                    const toISOS = `${year}-${month}-${d}`
                                    const dayofweek = ArrDayOfWeek[new_date.getDay()]
                                    return (
                                        <>
                                            <Grid item xs={4} sm={4} md={3} lg={2} className="itemDay" key={index}>
                                                <h6 className='item_dayofweek'>{dayofweek}</h6>
                                                <h6 className='item_day'>{d}</h6>
                                                <div className='list_note'>
                                                    {
                                                        listReport && listReport.map((item, index2) => {
                                                            const rentFrom: boolean = item?.rentFrom?.slice(0, 10) === toISOS
                                                            const rentTo: boolean = item?.rentTo?.slice(0, 10) === toISOS
                                                            const dateReceive: boolean = item?.dateReceive?.slice(0, 10) === toISOS
                                                            const dateTransfer: boolean = item?.dateTransfer?.slice(0, 10) === toISOS
                                                            return (
                                                                <>
                                                                    {
                                                                        rentFrom &&
                                                                        <div key={`${index2}${item.id}1`}
                                                                            onClick={() => { onClickShowCalendar(item) }}
                                                                            className="item_note bg-[#00ff25]">
                                                                            BKS: {item?.car?.descriptionInfo?.carNumber}
                                                                        </div>
                                                                    }

                                                                    {
                                                                        rentTo &&
                                                                        <div key={`${index2}${item.id}2`}
                                                                            onClick={() => { onClickShowCalendar(item) }}
                                                                            className="item_note bg-[#6291ff]">
                                                                            BKS: {item?.car?.descriptionInfo?.carNumber}
                                                                        </div>
                                                                    }

                                                                    {
                                                                        dateTransfer &&
                                                                        <div key={`${index2}${item.id}3`}
                                                                            onClick={() => { onClickShowCalendar(item) }}
                                                                            className="item_note bg-[#ff45dde0]">
                                                                            BKS: {item?.car?.descriptionInfo?.carNumber}
                                                                        </div>
                                                                    }

                                                                    {
                                                                        dateReceive &&
                                                                        <div key={`${index2}${item.id}4`}
                                                                            onClick={() => { onClickShowCalendar(item) }}
                                                                            className="item_note bg-[#ff0000bd]">
                                                                            BKS: {item?.car?.descriptionInfo?.carNumber}
                                                                        </div>
                                                                    }

                                                                </>
                                                            )
                                                        })
                                                    }

                                                </div>
                                            </Grid>
                                        </>
                                    )
                                })

                            }
                        </Grid>
                        :
                        <div className='boxTable'>
                            <TableContainer component={Paper} className="TableContainer">
                                <Table aria-label="customized table" className="tableReport">
                                    <TableHead>
                                        <TableRow>
                                            <StyledTableCell>Hãng xe / Biển kiểm soát</StyledTableCell>
                                            <StyledTableCell>Ngày thuê</StyledTableCell>
                                            <StyledTableCell>Ngày trả</StyledTableCell>
                                            <StyledTableCell>Ngày vận chuyển</StyledTableCell>
                                            <StyledTableCell>Ngày nhận xe</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody id='TableBody'>
                                        {listReport?.map((report) => (
                                            <StyledTableRow key={report.id}>
                                                <StyledTableCell component="td" scope="row">
                                                    {report?.car?.descriptionInfo?.carBrand} - &nbsp;
                                                    {report?.car?.descriptionInfo?.carNumber}
                                                </StyledTableCell>
                                                <StyledTableCell >{report?.rentFrom?.slice(0, 10)}</StyledTableCell>
                                                <StyledTableCell >{report?.rentTo?.slice(0, 10)}</StyledTableCell>
                                                <StyledTableCell >{report?.dateTransfer?.slice(0, 10)}</StyledTableCell>
                                                <StyledTableCell >{report?.dateReceive?.slice(0, 10)}</StyledTableCell>
                                            </StyledTableRow>
                                        ))}
                                        {listReport?.map((report) => (
                                            <StyledTableRow key={report.id}>
                                                <StyledTableCell component="td" scope="row">
                                                    {report?.car?.descriptionInfo?.carBrand} -
                                                    {report?.car?.descriptionInfo?.carNumber}
                                                </StyledTableCell>
                                                <StyledTableCell>{report?.rentFrom?.slice(0, 10)}</StyledTableCell>
                                                <StyledTableCell>{report?.rentTo?.slice(0, 10)}</StyledTableCell>
                                                <StyledTableCell>{report?.dateTransfer?.slice(0, 10)}</StyledTableCell>
                                                <StyledTableCell>{report?.dateReceive?.slice(0, 10)}</StyledTableCell>
                                            </StyledTableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                }
            </Grid>

            <ViewCalendar stateProps={isShowCalendar} close={onClickCloseCalendar} calendar={viewCalendar} />
        </Grid>



    );
}
