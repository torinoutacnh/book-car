import FullCalendar, { EventClickArg, } from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction'

export const DemoFullcalendar = () => {
    const getDate = (dayString: string) => {
        const today = new Date();
        const year = today.getFullYear().toString();
        let month = (today.getMonth() + 1).toString();

        if (month.length === 1) {
            month = "0" + month;
        }

        return dayString.replace("YEAR", year).replace("MONTH", month);
    }

    const handleEventClick = (clickInfo: EventClickArg) => {
        // if (window.confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
        //     clickInfo.event.remove()
        // }
        alert(clickInfo.event.title)
    }

    return (
        <div className='demo-app'>
            <div className='demo-app-main'>
                <FullCalendar
                    plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                    headerToolbar={{
                        left: 'prev,next today',
                        center: 'title',
                        right: 'dayGridMonth,timeGridWeek' //timeGridDay
                    }}
                    initialView='dayGridMonth'
                    editable={true}
                    selectable={true}
                    selectMirror={true}
                    dayMaxEvents={true}
                    eventClick={(eventClick) => handleEventClick(eventClick)}
                    events={[
                        { title: "All Day Event ", start: '2022-11-05' },
                        { title: "Long Event", start: '2022-11-15', end: '2022-11-25' },
                    ]}
                />
            </div>
        </div>
    )

}

