import { Box, Modal, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, } from "react";
import './index.scss'
import { Calendar } from "interface/calendar";
import { Link } from "react-router-dom";
import { adminEndpoints } from "routers/endpoints";

export function ViewCalendar(props: { stateProps: boolean, close: any, calendar: Calendar }) {

    const [isShow, setIsShow] = useState(false);

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    useEffect(() => {
        setIsShow(props.stateProps)
    }, [props.stateProps])


    return (
        <>
            {isShow ?
                <Modal
                    open={true}
                    onClose={() => { props.close(); }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center", fontWeight: "600", color: "#6291ff" }}>
                            Thông tin lịch trình
                        </Typography>

                        <div className="box_view_item">
                            <div className="view_color" style={{ background: "#00ff25" }}></div>
                            <span className="view_text"><b>Ngày thuê:</b> {props?.calendar?.rentFrom?.slice(0, 10)}</span>
                        </div>
                        <div className="box_view_item">
                            <div className="view_color" style={{ background: "#6291ff" }}></div>
                            <span className="view_text"><b>Ngày trả:</b> {props?.calendar?.rentTo?.slice(0, 10)}</span>
                        </div>
                        <div className="box_view_item">
                            <div className="view_color" style={{ background: "#ff45dde0" }}></div>
                            <span className="view_text"><b>Ngày vận chuyển:</b> {props?.calendar?.dateTransfer?.slice(0, 10)}</span>
                        </div>
                        <div className="box_view_item">
                            <div className="view_color" style={{ background: "#ff0000bd" }}></div>
                            <span className="view_text"><b>Ngày nhận xe:</b> {props?.calendar?.dateReceive?.slice(0, 10)}</span>
                        </div>

                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center", fontWeight: "600", color: "#6291ff" }}>
                            Thông tin xe
                        </Typography>

                        <img src={`${process.env.REACT_APP_API}${props?.calendar?.car?.carImage}`} style={{ maxHeight: "200px", margin: "0 auto" }} />

                        <div className="box_view_item">
                            <span className="view_text"><b>Hãng xe:</b> {props?.calendar?.car?.descriptionInfo?.carBrand}</span>
                        </div>

                        <div className="box_view_item">
                            <span className="view_text"><b>BKS:</b> {props?.calendar?.car?.descriptionInfo?.carNumber}</span>
                        </div>

                        <Link
                            to={`/${adminEndpoints.base}/${adminEndpoints.carInfo}?` + new URLSearchParams({ idCar: props?.calendar?.car?.id })}
                            target="_blank"
                            style={{ color: "blue" }}
                        >
                            {`Xem chi tiết xe >>>`}
                        </Link>

                    </Box>
                </Modal>
                :
                <></>
            }


        </>
    )
}