import { Alert, Button, Snackbar } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import EditIcon from "@mui/icons-material/Edit";
import { UpdateUser } from "components/admin/updateUser";
import AuthContext from "context/AuthContext";
import { User } from "interface/authenticate";
import { userEndpoints } from "routers/apiEndpoints";
import { useQuery } from "react-query";
import { Loading } from "components/loading/loading";

function UserPersonalInfo() {


    const [isShowUpdateUserInfo, setIsShowUpdateUserInfo] = useState(false);
    const onClickOpenUpdateUserInfo = () => setIsShowUpdateUserInfo(true);
    const onClickCloseUpdateUserInfo = () => setIsShowUpdateUserInfo(false);

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")
    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }

    const [reRender, setReRender] = useState(0)
    const reloadPage = () => {
        setReRender(reRender + 1)
    }

    const { auth } = useContext(AuthContext);

    const { data: user, isLoading, isError } = useQuery(
        "getUserInfo",
        async (): Promise<User> => {
            if (!auth) return;
            const res = await fetch(process.env.REACT_APP_API + userEndpoints.userInfo + auth?.userProfile?.id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + auth.accessToken,
                },
            })
            const data = await res.json();

            if (!res.ok) { throw new Error(JSON.stringify(data)) }

            return data.data
        },
        { cacheTime: 10000 }
    )

    if (isLoading) {
        return <Loading />
    }

    if (isError) { return <h1>ERROR...</h1> }

    return (
        <>

            <div className="grid grid-cols-1 md:grid-cols-3">

                <div className="m-auto mb-5">
                    <div className="w-[200px] h-[200px]">
                        <img
                            src="https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80"
                            alt=""
                            className="w-full h-full object-cover"
                        />
                    </div>
                </div>

                <div className="text__user--name">
                    <p>
                        <span>Họ tên </span>: {user?.name}
                    </p>
                    <p>
                        <span>Số điện thoại</span>: {user?.tel}
                    </p>
                    <p>
                        <span>Email</span>: {user?.email}
                    </p>
                    <p>
                        <span>Nghề nghiệp</span>: {user?.job}
                    </p>
                    <p>
                        <span>Địa chỉ hiện tại</span>: {user?.currentAddress}
                    </p>
                </div>

                <div className="text__user--info">
                    <p>
                        <span>CCCD/CMND</span>: {user?.citizenIdentificationInfo?.number}
                    </p>
                    <p>
                        <span>Nơi cấp</span>: {user?.citizenIdentificationInfo?.address}
                    </p>
                    <p>
                        <span>Hộ chiếu</span>: {user?.passportInfo?.number}
                    </p>
                    <p>
                        <span>Nơi cấp</span>: {user?.passportInfo?.address}
                    </p>
                    <p>
                        <span>Giấy phép lái xe</span>: {user?.drivingLicenseInfo?.number}
                    </p>
                </div>

            </div>

            <div className="md:text-right sx: text-center">
                <Button
                    variant="outlined"
                    startIcon={<EditIcon sx={{ fontSize: "18px !important" }} />}
                    onClick={() => { onClickOpenUpdateUserInfo() }}
                >
                    Chỉnh sửa thông tin
                </Button>
            </div>

            <UpdateUser
                close={onClickCloseUpdateUserInfo}
                stateProps={isShowUpdateUserInfo}
                reloadPage={reloadPage}
                user={user}
            />

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>
        </>
    );
}

export default UserPersonalInfo;