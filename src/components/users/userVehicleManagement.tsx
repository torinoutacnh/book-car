import React, { useContext } from "react";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import AuthContext from "context/AuthContext";
import { Contract, RentStatus } from "interface/contract";
import { contractGroupEndpoints, userEndpoints } from "routers/apiEndpoints";
import { Loading } from "components/loading/loading";
import { useQuery } from "react-query";
import { CarClassType } from "interface/car";
import { User } from "interface/authenticate";
import { Button, Divider } from "@mui/material";
import ReadMoreIcon from '@mui/icons-material/ReadMore';
import { Navigate, useNavigate } from "react-router-dom";
import { adminEndpoints, endpoints } from "routers/endpoints";

function UserVehicleManagement() {

    const navigate = useNavigate();
    const { auth } = useContext(AuthContext);

    const [expanded, setExpanded] = React.useState<string | false>(false);

    const handleChange = (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
        setExpanded(isExpanded ? panel : false);
    };

    const { data: listUser } = useQuery(
        "getUser",
        async (): Promise<User[]> => {
            if (!auth) return;
            const res = await fetch(process.env.REACT_APP_API + userEndpoints.base, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + auth.accessToken,
                },
            })
            const data = await res.json();

            if (!res.ok) { throw new Error(JSON.stringify(data)) }

            return data.data
        },
        { cacheTime: 10000 }
    )


    const { data: listContract, isLoading, isError } = useQuery(
        "getContract",
        async (): Promise<Contract[]> => {
            if (!auth) return;
            const res = await fetch(process.env.REACT_APP_API + contractGroupEndpoints.filterByUser + auth?.userProfile?.id, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + auth.accessToken,
                },
                body: JSON.stringify({ pageNumber: 1, pageSize: 100 })
            })
            const data = await res.json();

            if (!res.ok) { throw new Error(JSON.stringify(data)) }

            return data.data
        },
        { cacheTime: 10000 }
    )

    if (isLoading) {
        return <Loading />
    }

    if (isError) { return <h1>ERROR...</h1> }

    const onClickShowDetailContract = (id: string) => {
        navigate("/" + endpoints.contract_detail.slice(0, 18) + id);
    };

    return (
        <>
            {/* {console.log("list", data)} */}
            {listContract ? (
                <div>
                    {listContract?.map((item, index) => {
                        const User = listUser?.find(user => user.id === item.renterId)
                        return (
                            <div style={{ boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px" }} className="mb-3 mr-3">
                                <Accordion
                                    expanded={expanded === `panel${index + 1}`}
                                    onChange={handleChange(`panel${index + 1}`)}
                                    sx={{ overflowY: "auto", maxHeight: "80vh" }}

                                >
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls={`panel${index + 1}bh-content`}
                                        id={`panel${index + 1}bh-header`}
                                    >
                                        <Typography sx={{ width: "33%", flexShrink: 0 }}>
                                            Hãng xe: {item?.requireDescriptionInfo?.carBrand}
                                        </Typography>
                                        <Typography sx={{ width: "33%", color: "text.secondary" }}>
                                            Ngày thuê: {item?.rentFrom?.toString().slice(0, 10)}
                                        </Typography>
                                        <Typography sx={{ width: "33%", color: "text.secondary" }}>
                                            Ngày trả: {item?.rentTo?.toString().slice(0, 10)}
                                        </Typography>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Typography>
                                            <div className="flex justify-between items-start flex-wrap md:flex-nowrap gap-x-2 user__form">
                                                <div className="w-full md:w-[32%]">
                                                    <p>
                                                        Phân khúc xe: <span>{CarClassType[item?.carClass]}</span>
                                                    </p>
                                                    <p>
                                                        Màu: <span>{item?.requireDescriptionInfo?.carColor}</span>
                                                    </p>
                                                    <p>
                                                        Chỗ ngồi: <span>{item?.requireDescriptionInfo?.seatNumber} chỗ</span>
                                                    </p>
                                                    <p>
                                                        Mục đích thuê: <span>{item?.rentPurpose}</span>
                                                    </p>
                                                </div>
                                                <div className="w-full md:w-[32%]">
                                                    <p>
                                                        Tên bên thuê: <span>{User?.name}</span>
                                                    </p>
                                                    <p>
                                                        Địa chỉ: <span>{User?.currentAddress}</span>
                                                    </p>
                                                    <p>
                                                        Số điện thoại: <span>{User?.tel}</span>
                                                    </p>
                                                    <p>
                                                        Email: <span>{User?.email}</span>
                                                    </p>
                                                </div>
                                                <div className="w-full md:w-[36%]">
                                                    <p>
                                                        Trạng thái hợp đồng: <span>{RentStatus[item?.status]}</span>
                                                    </p>
                                                    <div className="text-right">
                                                        <Button
                                                            variant="text"
                                                            className=""
                                                            endIcon={<ReadMoreIcon />}
                                                            onClick={() => { onClickShowDetailContract(item?.id) }}
                                                        >
                                                            <p className="normal-case">Xem chi tiết</p>
                                                        </Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </Typography>
                                    </AccordionDetails>
                                </Accordion>
                                {/* <Divider /> */}

                            </div>
                        )
                    })}
                </div>)
                :
                (<></>)
            }
        </>
    );
}

export default UserVehicleManagement;