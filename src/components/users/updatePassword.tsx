import { Alert, Box, Button, Modal, Snackbar, Typography } from "@mui/material";
import { useState, useContext, useEffect } from "react";
import "../admin/styles/crud.scss";
import TextField from "@mui/material/TextField";
import CloseIcon from "@mui/icons-material/Close";
import AuthContext from "context/AuthContext";
import CheckIcon from "@mui/icons-material/Check";
import { userEndpoints } from "routers/apiEndpoints";

export function UpdatePassword(props: { id: string, stateProps: boolean, close: any }) {

    const style = {
        position: "absolute" as "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        bgcolor: "background.paper",
        border: "1px solid #000",
        boxShadow: 24,
        p: 4,
        borderRadius: "10px",
        textAlign: "center",
    };

    const [isShow, setIsShow] = useState(false);

    useEffect(() => {
        setIsShow(props.stateProps)
    }, [props.stateProps])

    const [typeNotifi, setTypeNotifi] = useState("success");
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("");

    const handleCloseNotify = (
        event?: React.SyntheticEvent | Event,
        reason?: string
    ) => {
        if (reason === "clickaway") {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type);
        setMessageNotify(message);
        setOpenNofity(true);
    };

    const [oldPassword, setOldPassword] = useState<string>()
    const [newPassword, setNewPassword] = useState<string>()
    const [newPasswordConfirm, setNewPasswordConfirm] = useState<string>()

    const { auth } = useContext(AuthContext);

    const handleSubmit = async () => {
        if (!auth) return;

        const dataCheck = {
            oldPassword: oldPassword,
            newPassword: newPassword,
            newPasswordConfirm: newPasswordConfirm
        }

        const checkNull = { a: false };

        Object.values(dataCheck).forEach((value, index) => {
            const tmp = value as string;
            if (!tmp || tmp.length === 0) {
                checkNull.a = true;
            }
        });

        if (checkNull.a) {
            handleOpenNotify("Vui lòng nhập đầy đủ thông tin!", "error");
            return;
        }

        if (newPassword !== newPasswordConfirm) {
            handleOpenNotify("Xác nhận mật khẩu không trùng nhau!", "error");
            return
        }

        if (((newPassword).length < 8) || ((newPassword).length > 20)) {
            handleOpenNotify('Mật khẩu từ 8 đến 20 ký tự!', 'error')
            return
        }

        const dataBody = {
            oldPassword: oldPassword,
            newPassword: newPassword
        }

        const res = await fetch(process.env.REACT_APP_API + userEndpoints.updatePassword + "?id=" + props.id,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + auth.accessToken,
                },
                body: JSON.stringify(dataBody)
            }
        );

        const data = await res.json();

        if (res.status > 200) {
            handleOpenNotify(data.message, "error");
            // console.log("Transfer-update-contract status ", res.status, data);
            return;
        }

        // console.log("create rent success => ", data)
        handleOpenNotify("Đổi mật khẩu thành công!", "success");
        props.close();
    };

    return (
        <>
            {isShow ? (
                <Modal
                    open={true}
                    onClose={() => { props.close() }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography
                            id="modal-modal-title"
                            variant="h6"
                            component="h2"
                            style={{ textAlign: "center" }}
                        >
                            Đổi mật khẩu
                        </Typography>
                        <form className="container_create max-h-[500px] overflow-y-auto form_scroll pr-3">
                            <div className="info w-full">

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Mật khẩu hiện tại"
                                    variant="outlined"
                                    size="small"
                                    required
                                    value={oldPassword}
                                    onChange={(e) => setOldPassword(e.target.value)}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Mật khẩu mới"
                                    variant="outlined"
                                    size="small"
                                    required
                                    value={newPassword}
                                    onChange={(e) => setNewPassword(e.target.value)}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Xác nhận mật khẩu"
                                    variant="outlined"
                                    size="small"
                                    required
                                    value={newPasswordConfirm}
                                    onChange={(e) => setNewPasswordConfirm(e.target.value)}
                                />

                            </div>
                        </form>

                        <div className="action">
                            <Button
                                size="small"
                                variant="contained"
                                startIcon={<CheckIcon />}
                                className="btnCreate"
                                type="submit"
                                onClick={handleSubmit}
                            >
                                Xác nhận
                            </Button>
                            <Button
                                size="small"
                                color="success"
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className="btnCancel"
                                onClick={() => {
                                    props.close()
                                }}
                            >
                                Hủy bỏ
                            </Button>
                        </div>

                    </Box>
                </Modal>
            ) : (
                <></>
            )}

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {typeNotifi === "success" ? (
                    <Alert
                        color={"info"}
                        onClose={handleCloseNotify}
                        severity={"success"}
                        sx={{ width: "100%" }}
                    >
                        {messageNotify}
                    </Alert>
                ) : (
                    <Alert
                        color={"error"}
                        onClose={handleCloseNotify}
                        severity={"error"}
                        sx={{ width: "100%" }}
                    >
                        {messageNotify}
                    </Alert>
                )}
            </Snackbar>
        </>
    );
}
