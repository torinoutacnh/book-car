import { Loading } from 'components/loading/loading';
import AuthContext from 'context/AuthContext';
import { Contract } from 'interface/contract';
import React, {useContext, useEffect, useState} from 'react';
import { useSearchParams } from 'react-router-dom';
import { contractGroupEndpoints } from 'routers/apiEndpoints';
import './infoContract.scss'

function InfoContract() {

    const { auth } = useContext(AuthContext);

    const [contract, setContract] = useState<Contract>()

    const [searchParams, setSearchParams] = useSearchParams();
    const id = searchParams.get("idContract");

    useEffect(()=>{
        
        if(!auth) {return}
        (async()=>{
            const res = await fetch(process.env.REACT_APP_API + contractGroupEndpoints.contractInfo + id, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + auth.accessToken,
                },
            })

            const data = await res.json()

            if(!res.ok){ 
                console.log("data info contract status ", res.status, ' => ' , contract);
                return
            }

            setContract(data.data)
            console.log("data info contract" , contract);
            
        })()
    },[])

    return (
        <>
            {
                contract 
                ?
                    <div className="container">
                        <>{Object.entries(contract).forEach((value)=>{console.log("contract info", value)})}

                        <div>
                            <p>{contract.id}</p>
                            <p>{contract.rentFrom.toString().slice(0,10)}</p>
                            <p>{contract.rentTo.toString().slice(0,10)}</p>
                            <p>{contract.carClass}</p>
                            <p>{contract.requireDescriptionInfo.carBrand}</p>
                            <p>{contract.rentPurpose}</p>
                            <p>{contract.expertiseInfo.isFirstTimeRent}</p>
                            <p>Thông tin mạng xã hội
                                { contract?.customerSocialInfo?.facebook && 
                                    <p> Facebook: 
                                        <a 
                                            className="text-blue-700" 
                                            target="_blank"
                                            href={contract.customerSocialInfo.facebook}
                                            rel="noreferrer"
                                        >
                                            Nhấn vào đây
                                        </a>
                                    </p>
                                }
                                { contract?.customerSocialInfo?.zalo && 
                                    <p> - Zalo: {contract?.customerSocialInfo?.zalo}</p>
                                }
                                { contract?.customerSocialInfo?.linkedin && 
                                    <p> - Linkedin: {contract?.customerSocialInfo?.linkedin}</p>}
                                { contract?.customerSocialInfo?.other && 
                                    <p> - Khác: {contract?.customerSocialInfo?.other}</p>}
                            </p>
                            {/* 
                            <p>{contract.companyInfo}</p>
                            <p>{contract.relativeTel}</p>
                            <p>{contract.addtionalInfo}</p>
                            <p>{contract.status}</p>
                            <p>{contract.car}</p>
                            <p>{contract.renter}</p>
                            <p>{contract.contractFile}</p>
                            <p>{contract.expertiseContract}</p>
                            <p>{contract.rentContract}</p>
                            <p>{contract.transferContract}</p>
                            <p>{contract.receiveContract}</p>
                            <p>{contract.expertiser}</p>
                            <p>{contract.representative}</p> */}
                        </div>
                        </>
                    </div>
                :
                <Loading/>
            }
        </>
        
    );
}

export default InfoContract;