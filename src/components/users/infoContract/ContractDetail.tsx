import React, { useState } from "react";
import AuthContext from "context/AuthContext";
import { Contract, RentStatus } from "interface/contract";
import { useContext } from "react";
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import "../../../pages/form/contract/index.scss";
import { contractGroupEndpoints } from "routers/apiEndpoints";
import { Box, Typography, Tabs, Tab, IconButton } from "@mui/material";
import { ExpertiseInfo, ExpertiseStatus } from "interface/request/contractGroupReq";
import PreviewIcon from "@mui/icons-material/Preview";
import { CarClassType } from "interface/car";
import { Loading } from "components/loading/loading";
import { WordContractPath } from "components/Word/WordContractPath";

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
}

export default function ContractDetail() {
    const { groupId } = useParams();
    const { auth } = useContext(AuthContext);
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    const [filePathWord, setFilePathWord] = useState("");
    const [isShowWordContractPath, setIsShowWordContractPath] = useState(false);
    const onClickShowWordContractPath = (path: string) => {
        setFilePathWord(path);
        setIsShowWordContractPath(true);
    };
    const onClickCloseShowWordContractPath = () => {
        setFilePathWord("");
        setIsShowWordContractPath(false);
    };
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    const AuthHeader = {
        "Content-Type": "application/json",
        Authorization: "Bearer " + auth?.accessToken,
    };

    const { data, error, isLoading, status } = useQuery(
        ["GetGroupDetail", groupId, AuthHeader],
        async (): Promise<Contract> => {
            const res = await fetch(process.env.REACT_APP_API + `${contractGroupEndpoints.contractInfo}${groupId}`, {
                method: "GET",
                headers: AuthHeader,
            });
            if (res.status === 401 || res.status === 403) throw { code: res.status };
            const data = await res.json();
            return data.data;
        },
        {
            useErrorBoundary: (error) => (error as any).response?.status >= 400,
            retry: false,
            refetchInterval: false,
            onError: console.log,
        }
    );

    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    if (data) {
        // console.log(data, status);
        return (
            <div className="bg__contract__detail">
                <div className="max-w-[90vw] px-3 m-auto">
                    <Box sx={{ width: "100%" }}>
                        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                            <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" variant="scrollable" scrollButtons>
                                <Tab label="THÔNG TIN THUÊ XE" {...a11yProps(0)} />
                                {data.expertiseContract &&
                                    <Tab label="THÔNG TIN THẨM ĐỊNH" {...a11yProps(1)} />
                                }
                                {data.rentContract &&
                                    <Tab label="HỢP ĐỒNG THUÊ" {...a11yProps(2)} />
                                }
                                {data.transferContract &&
                                    <Tab label="HỢP ĐỒNG GIAO XE" {...a11yProps(3)} />
                                }
                                {data.receiveContract &&
                                    <Tab label="HỢP ĐỒNG NHẬN XE" {...a11yProps(4)} />
                                }
                            </Tabs>
                        </Box>

                        <TabPanel value={value} index={0}>
                            {/* THÔNG TIN THUÊ XE */}
                            <div className="md:pt-transform">
                                <div className="px-5 bg-white bs">
                                    <h1 className="text-center text-[1.3em] text-main pt-3 font-bold">
                                        THÔNG TIN THUÊ XE
                                    </h1>
                                    <hr />
                                    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin yêu cầu thuê xe</h3>
                                            <p>
                                                <span>Ngày bắt đầu thuê</span> : {data?.rentFrom?.toString().slice(0, 10)}
                                            </p>
                                            <p>
                                                <span>Ngày kết thúc thuê</span> : {data?.rentTo?.toString().slice(0, 10)}
                                            </p>
                                            <p>
                                                <span>Mục đích thuê</span> : {data?.rentPurpose}
                                            </p>
                                            <p>
                                                <span>Hãng xe</span> : {data?.requireDescriptionInfo?.carBrand}
                                            </p>
                                            <p>
                                                <span>Đời xe</span> : {data?.requireDescriptionInfo?.yearCreate}
                                            </p>
                                            <p>
                                                <span>Phân khúc xe</span> : {CarClassType[data?.carClass]}
                                            </p>
                                            <p>
                                                <span>Số chỗ ngồi</span> : {data?.requireDescriptionInfo?.seatNumber} chỗ
                                            </p>
                                            <p>
                                                <span>Màu xe</span> : {data?.requireDescriptionInfo?.carColor}
                                            </p>
                                        </div>

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin liên quan</h3>
                                            <p>
                                                <span>Thuê xe</span> : {data?.expertiseInfo?.isFirstTimeRent ? "Lần đầu thuê xe" : ExpertiseInfo[data?.expertiseInfo?.status]}
                                            </p>
                                            <p>
                                                <span>Mạng xã hội</span> :
                                                {data?.customerSocialInfo?.facebook &&
                                                    <p className='text__p pl-2 whitespace-nowrap overflow-hidden text-ellipsis w-[300px]'> - Facebook: <a className="text-blue-700" target="_blank" href={data.customerSocialInfo.facebook}>{data?.customerSocialInfo?.facebook}</a></p>}
                                                {data?.customerSocialInfo?.zalo &&
                                                    <p className='text__p pl-2'> - Zalo: <span>{data?.customerSocialInfo?.zalo}</span></p>}
                                                {data?.customerSocialInfo?.linkedin &&
                                                    <p className='text__p pl-2'> - Linkedin: <span>{data?.customerSocialInfo?.linkedin}</span></p>}
                                                {data?.customerSocialInfo?.other &&
                                                    <p className='text__p pl-2'> - Khác: <span>{data?.customerSocialInfo?.other}</span></p>}
                                            </p>
                                            <p>
                                                <span>Thông tin công ty</span> : {data?.companyInfo}
                                            </p>
                                            <p>
                                                <span>Số điện thoại công ty</span> : {data?.relativeTel}
                                            </p>
                                            <p>
                                                <span>Thông tin bổ sung</span> : {data?.addtionalInfo}
                                            </p>
                                            <p style={{ whiteSpace: "unset" }}>
                                                <span>Trạng thái hợp đồng </span> : {RentStatus[data?.status]}
                                            </p>
                                        </div>

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin bên thuê</h3>
                                            <p>
                                                <span>Họ tên</span> : {data?.renter?.name}
                                            </p>
                                            <p>
                                                <span>Nghề nghiệp</span> : {data?.renter?.job}
                                            </p>
                                            <p>
                                                <span>Số điện thoại</span> : {data?.renter?.tel}
                                            </p>
                                            <p>
                                                <span>Địa chỉ</span> : {data?.renter?.currentAddress}
                                            </p>
                                            <p>
                                                <span>Email</span> : {data?.renter?.email}
                                            </p>
                                            {data?.renter?.citizenIdentificationInfo?.number &&
                                                <p>
                                                    <span>CCCD/CMND</span> : {data?.renter?.citizenIdentificationInfo?.number}
                                                </p>
                                            }
                                            {data?.renter?.passportInfo?.number &&
                                                <p>
                                                    <span>Số hộ chiếu</span> : {data?.renter?.passportInfo?.number}
                                                </p>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </TabPanel>

                        <TabPanel value={value} index={1}>
                            {/* THÔNG TIN THẨM ĐỊNH */}
                            <div className="md:pt-transform">
                                <div className="container bg-white bs">
                                    <h1 className="text-center text-[1.3em] text-main pt-3 font-bold">
                                        THÔNG TIN THẨM ĐỊNH
                                    </h1>
                                    <hr />
                                    <div className="grid grid-cols-1 md:grid-cols-2">

                                        <div className="contract--detail">
                                            <h3 className="text-center">Kết quả thẩm định</h3>
                                            <p>
                                                <span>Ngày thẩm định</span> : {data?.expertiseContract?.expertiseDate?.toString().slice(0, 10)}
                                            </p>
                                            <p>
                                                <span>Mô tả thẩm định</span> : {data?.expertiseContract?.description}
                                            </p>
                                            <p>
                                                <span>Kết quả</span> : {ExpertiseStatus[data?.expertiseContract?.result]}
                                            </p>
                                            <p>
                                                <span>Kết quả khác</span> : {data?.expertiseContract?.resultOther}
                                            </p>
                                            <p>
                                                <span>Độ tin cậy</span> : {data?.expertiseContract?.trustLevel}
                                            </p>
                                            <p>
                                                <span>Tài sản đặt cọc</span> : {data?.expertiseContract?.depositInfo?.asset}
                                            </p>
                                            <p>
                                                <span>Mô tả</span> : {data?.expertiseContract?.depositInfo?.description}
                                            </p>
                                            <p>
                                                <span>Cần thanh toán</span> : {data?.expertiseContract?.paymentAmount.toLocaleString()} VND
                                            </p>
                                        </div>

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin nhân viên thẩm định</h3>
                                            <p>
                                                <span>Họ tên </span> : {data?.expertiser?.name}
                                            </p>
                                            <p>
                                                <span>Số điện thoại</span> : {data?.expertiser?.tel}
                                            </p>
                                            <p>
                                                <span>Email</span> : {data?.expertiser?.email}
                                            </p>
                                            <p>
                                                <span>Trạng thái hợp đồng</span> : {data?.expertiseContract?.isExported ? "Đã duyệt" : "Chưa duyệt"}
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </TabPanel>

                        <TabPanel value={value} index={2}>
                            {/* HỢP ĐỒNG THUÊ */}
                            <div className="md:pt-transform">
                                <div className="container bg-white bs">
                                    <h1 className="text-center text-[1.3em] text-main pt-3 font-bold">
                                        HỢP ĐỒNG THUÊ
                                        <IconButton
                                            style={{ float: "right" }}
                                            aria-label="Preview"
                                            color="primary"
                                            onClick={(e) => {
                                                onClickShowWordContractPath(data?.rentContract?.filePath)
                                            }}
                                        >
                                            <PreviewIcon />
                                        </IconButton>
                                    </h1>

                                    <hr />
                                    <div className="lg:grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin xe</h3>
                                            <p>
                                                <span>Phân khúc</span> : {data?.car?.carClass}
                                            </p>
                                            <p>
                                                <span>Hãng xe</span> : {data?.rentContract?.carDescriptionInfoAtRent?.carBrand}
                                            </p>
                                            <p>
                                                <span>Tên xe</span> : {data?.rentContract?.carDescriptionInfoAtRent?.carName}
                                            </p>
                                            <p>
                                                <span>Phiên bản</span> : {data?.rentContract?.carDescriptionInfoAtRent?.carName}
                                            </p>
                                            <p>
                                                <span>Đời xe</span> : {data?.rentContract?.carDescriptionInfoAtRent?.yearCreate}
                                            </p>
                                            <p>
                                                <span>Truyền động</span> : {data?.rentContract?.carDescriptionInfoAtRent?.carTransmission}
                                            </p>
                                            <p>
                                                <span>Kiểu dáng</span> : {data?.rentContract?.carDescriptionInfoAtRent?.carStyle}
                                            </p>
                                            <p>
                                                <span>Số chỗ ngồi</span> : {data?.rentContract?.carDescriptionInfoAtRent?.seatNumber}
                                            </p>
                                            <p>
                                                <span>Màu xe</span> : {data?.rentContract?.carDescriptionInfoAtRent?.carColor}
                                            </p>
                                            <p>
                                                <span>Nhiên liệu sử dụng</span> : {data?.rentContract?.carDescriptionInfoAtRent?.carFuelUse}
                                            </p>
                                            <p>
                                                <span>Biển kiểm soát</span> : {data?.rentContract?.carDescriptionInfoAtRent?.carNumber}
                                            </p>
                                            <p style={{ whiteSpace: "unset" }}>
                                                <span>Mô tả </span> : {data?.rentContract?.carDescriptionInfoAtRent?.carDescription}
                                            </p>
                                        </div>

                                        <div className="contract--detail">
                                            <h3 className="text-center">Trạng thái xe</h3>
                                            <p>
                                                <span>Số trên đồng hồ</span> : {data?.car?.carState?.speedometerNumber}
                                            </p>
                                            <p>
                                                <span>Mức nhiên liệu</span> : {data?.car?.carState?.fuelPercent} %
                                            </p>
                                            <p>
                                                <span>Phí cao tốc ETC</span> : {data?.car?.carState?.currentEtcAmount.toLocaleString()} VND
                                            </p>
                                            <p>
                                                <span>Mô tả hiện trạng:</span> : {data?.car?.carState?.carStatusDescription}
                                            </p>
                                            <h3 className="text-center">Biểu phí thuê xe</h3>
                                            <p>
                                                <span>Ngày thường</span> : {data?.rentContract?.carGeneralInfoAtRent?.priceForNormalDay.toLocaleString()} VND
                                            </p>
                                            <p>
                                                <span>Cuối tuần</span> : {data?.rentContract?.carGeneralInfoAtRent?.priceForWeekendDay.toLocaleString()} VND
                                            </p>
                                            <p>
                                                <span>Ngày lễ</span> : {data?.rentContract?.carGeneralInfoAtRent?.priceForHoliday.toLocaleString()} VND
                                            </p>
                                            <p>
                                                <span>Tháng</span> : {data?.rentContract?.carGeneralInfoAtRent?.priceForMonth.toLocaleString()} VND
                                            </p>
                                            <p>
                                                <span>Quá giờ</span> : {data?.rentContract?.carGeneralInfoAtRent?.pricePerHourExceed.toLocaleString()} VND/Giờ
                                            </p>
                                            <p>
                                                <span>Quá km</span> : {data?.rentContract?.carGeneralInfoAtRent?.pricePerKmExceed.toLocaleString()} VND/Km
                                            </p>
                                            <p>
                                                <span>Giới hạn</span> : {data?.rentContract?.carGeneralInfoAtRent?.limitedKmForMonth.toLocaleString()} Km/Tháng
                                            </p>
                                        </div>

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin đặt cọc và thời gian thuê</h3>
                                            <p>
                                                <span>Ngày bắt đầu thuê</span> : {data?.rentContract?.rentFrom?.toString().slice(0, 10)}
                                            </p>
                                            <p>
                                                <span>Ngày kết thúc thuê</span> : {data?.rentContract?.rentTo?.toString().slice(0, 10)}
                                            </p>
                                            <p>
                                                <span>Tài sản đặt cọc</span> : {data?.rentContract?.depositItem?.asset}
                                            </p>
                                            <p>
                                                <span>Thông tin tài sản</span> : {data?.rentContract?.depositItem?.description}
                                            </p>
                                            <p>
                                                <span>Số tiền đặt cọc</span> : {data?.rentContract?.depositItem?.downPayment.toLocaleString()} VND
                                            </p>
                                            <p>
                                                <span>Số tiền thanh toán</span> : {data?.rentContract?.paymentAmount.toLocaleString()} VND
                                            </p>
                                            <h3 className="text-center">Thông tin nhân viên phụ trách</h3>
                                            <p>
                                                <span>Họ tên </span> : {data?.renter.name}
                                            </p>
                                            <p>
                                                <span>Số điện thoại</span> : {data?.renter.tel}
                                            </p>
                                            <p>
                                                <span>Email</span> : {data?.renter.email}
                                            </p>
                                            <p>
                                                <span>Trạng thái hợp đồng</span> : {data?.rentContract?.isExported ? "Đã duyệt" : "Chưa duyệt"}
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </TabPanel>

                        <TabPanel value={value} index={3}>
                            {/* HỢP ĐỒNG GIAO XE */}
                            <div className="md:pt-transform">
                                <div className="container bg-white bs">
                                    <h1 className="text-center text-[1.3em] text-main pt-3 font-bold">
                                        HỢP ĐỒNG GIAO XE
                                        <IconButton
                                            style={{ float: "right" }}
                                            aria-label="Preview"
                                            color="primary"
                                            onClick={(e) => {
                                                onClickShowWordContractPath(data?.transferContract?.filePath)
                                            }}
                                        >
                                            <PreviewIcon />
                                        </IconButton>
                                    </h1>
                                    <hr />
                                    <div className="lg:grid grid-cols-1 md:grid-cols-3">

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin vận chuyển</h3>
                                            <p>
                                                <span>Ngày vận chuyển</span> : {data?.transferContract?.dateTransfer?.toString().slice(0, 10)}
                                            </p>
                                            <p>
                                                <span>Số trên đồng hồ</span> : {data?.transferContract?.currentCarState?.speedometerNumber}
                                            </p>
                                            <p>
                                                <span>Mức nhiên liệu</span> : {data?.transferContract?.currentCarState?.fuelPercent} %
                                            </p>
                                            <p>
                                                <span>Phí cao tốc ETC</span> : {data?.transferContract?.currentCarState?.currentEtcAmount.toLocaleString()} VND
                                            </p>
                                            <p>
                                                <span>Mô tả hiện trạng</span> : {data?.transferContract?.currentCarState?.carStatusDescription}
                                            </p>
                                        </div>

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin tài sản giao xe</h3>
                                            <p>
                                                <span>Giấy tờ</span> : {data?.transferContract?.transferItem?.paper}
                                            </p>
                                            <p>
                                                <span>Thiết bị dự phòng</span> : {data?.transferContract?.transferItem?.standbyDevice}
                                            </p>
                                            <p>
                                                <span>Thông tin thiết bị</span> : {data?.transferContract?.transferItem?.standbyDeviceInfo}
                                            </p>
                                        </div>

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin nhân viên giao xe</h3>
                                            <p>
                                                <span>Họ tên </span> : {data?.transferer?.name}
                                            </p>
                                            <p>
                                                <span>Số điện thoại</span> : {data?.transferer?.tel}
                                            </p>
                                            <p>
                                                <span>Email</span> : {data?.transferer?.email}
                                            </p>
                                            <p>
                                                <span>Trạng thái hợp đồng</span> : {data?.transferContract?.isExported ? "Đã duyệt" : "Chưa duyệt"}
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </TabPanel>

                        <TabPanel value={value} index={4}>
                            {/* HỢP ĐỒNG NHẬN XE */}
                            <div className="md:pt-transform">
                                <div className="container bg-white bs">
                                    <h1 className="text-center text-[1.3em] text-main pt-3 font-bold">
                                        HỢP ĐỒNG NHẬN XE
                                        <IconButton
                                            style={{ float: "right" }}
                                            aria-label="Preview"
                                            color="primary"
                                            onClick={(e) => {
                                                onClickShowWordContractPath(data?.receiveContract?.filePath)
                                            }}
                                        >
                                            <PreviewIcon />
                                        </IconButton>
                                    </h1>
                                    <hr />
                                    <div className="lg:grid grid-cols-1 md:grid-cols-3">

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin nhận xe</h3>
                                            <p>
                                                <span>Ngày nhận xe</span> : {data?.receiveContract?.dateReceive?.toString().slice(0, 10)}
                                            </p>
                                            <p>
                                                <span>Số trên đồng hồ</span> : {data?.receiveContract?.currentCarState?.speedometerNumber}
                                            </p>
                                            <p>
                                                <span>Mức nhiên liệu</span> : {data?.receiveContract?.currentCarState?.fuelPercent} %
                                            </p>
                                            <p>
                                                <span>Phí cao tốc ETC</span> : {data?.receiveContract?.currentCarState?.currentEtcAmount.toLocaleString()} VND
                                            </p>
                                            <p>
                                                <span>Mô tả hiện trạng</span> : {data?.receiveContract?.currentCarState?.carStatusDescription}
                                            </p>
                                        </div>

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin tài sản nhận xe</h3>
                                            <p>
                                                <span>Giấy tờ</span> : {data?.receiveContract?.receiveItem?.paper}
                                            </p>
                                            <p>
                                                <span>Thiết bị dự phòng</span> : {data?.receiveContract?.receiveItem?.standbyDevice}
                                            </p>
                                            <p>
                                                <span>Thông tin thiết bị</span> : {data?.receiveContract?.receiveItem?.standbyDeviceInfo}
                                            </p>
                                        </div>

                                        <div className="contract--detail">
                                            <h3 className="text-center">Thông tin nhân viên nhận xe</h3>
                                            <p>
                                                <span>Họ tên </span> : {data?.receiver?.name}
                                            </p>
                                            <p>
                                                <span>Số điện thoại</span> : {data?.receiver?.tel}
                                            </p>
                                            <p>
                                                <span>Email</span> : {data?.receiver?.email}
                                            </p>
                                            <p>
                                                <span>Trạng thái hợp đồng</span> : {data?.receiveContract?.isExported ? "Đã duyệt" : "Chưa duyệt"}
                                            </p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </TabPanel>

                    </Box>
                </div>
                <WordContractPath
                    filePath={filePathWord}
                    stateProps={isShowWordContractPath}
                    close={onClickCloseShowWordContractPath}
                />
            </div >
        );
    }

    if (error) {
        return <>{(error as any).code}</>;
    }

    return (
        <>
            <Loading />
        </>
    );
}
