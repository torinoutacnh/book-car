import { Alert, Box, Button, Modal, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext } from "react";
import "./styles/crud.scss"
import TextField from '@mui/material/TextField';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { contractGroupEndpoints } from "routers/apiEndpoints";
import { WordContractPreview } from "components/Word/WordContractPreview";
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import { Contract } from "interface/contract";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';

export function UpdateReceiveContract(props: { id: string, stateProps: boolean, close: any, reloadPage: any, contract: Contract }) {

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const [isShow, setIsShow] = useState(false);
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    const [isPreviewWord, setIsPreviewWord] = useState(false);

    const onClickWordPreview = () => {
        setIsPreviewWord(true)

    }
    const onClickCloseWordPreview = () => {
        setIsPreviewWord(false)
    }

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }

    interface initValue {
        speedometerNumber: number;
        fuelPercent: number;
        currentEtcAmount: number;
        carStatusDescription: string;
        paper: string;
        standbyDevice: string;
        standbyDeviceInfo: string;
    }

    const [state, setState] = useState<initValue>();
    const [imageInside, setImageInside] = useState<FileList>(null)
    const [imageOutside, setImageOutside] = useState<FileList>(null)

    const [dateReceive, setDateReceive] = useState<Moment | null>(moment(props?.contract?.receiveContract?.dateReceive));
    const handleChangeDateReceive = (newValue: Moment | null) => {
        setDateReceive(newValue);
    }

    useEffect(() => {
        setIsShow(props.stateProps)
        if (!props?.contract?.receiveContract) { return }
        const resetvalue = {
            speedometerNumber: props?.contract?.receiveContract?.currentCarState?.speedometerNumber,
            fuelPercent: props?.contract?.receiveContract?.currentCarState?.fuelPercent,
            currentEtcAmount: props?.contract?.receiveContract?.currentCarState?.currentEtcAmount,
            carStatusDescription: props?.contract?.receiveContract?.currentCarState?.carStatusDescription,
            paper: props?.contract?.receiveContract?.receiveItem?.paper,
            standbyDevice: props?.contract?.receiveContract?.receiveItem?.standbyDevice,
            standbyDeviceInfo: props?.contract?.receiveContract?.receiveItem?.standbyDeviceInfo,
        };
        setDateReceive(moment(new Date(props?.contract?.receiveContract?.dateReceive.slice(0, 10))));
        setState(resetvalue);
    }, [props.stateProps, props.reloadPage])

    const { auth } = useContext(AuthContext);

    const handleSubmit = async () => {
        if (!auth) return;

        const dataCheck = {
            SpeedometerNumber: state.speedometerNumber,
            FuelPercent: state.fuelPercent,
            CurrentEtcAmount: state.currentEtcAmount,
            CarStatusDescription: state.carStatusDescription,
            Paper: state.paper,
            StandbyDevice: state.standbyDevice,
            StandbyDeviceInfo: state.standbyDeviceInfo,
        }

        const isNull = { a: false }

        Object.values(state).forEach((value, index) => {
            const tmp = value as string
            if (!tmp || tmp.length === 0) {
                isNull.a = true
            }
        })

        if (isNull.a) {
            handleOpenNotify('Vui lòng nhập đầy đủ thông tin!', 'error')
            return
        }

        const formData = new FormData()

        formData.append("DateReceive", dateReceive.format("YYYY-MM-DD").toString());
        formData.append("SpeedometerNumber", dataCheck.SpeedometerNumber.toString());
        formData.append("FuelPercent", dataCheck.FuelPercent.toString());
        formData.append("CurrentEtcAmount", dataCheck.CurrentEtcAmount.toString());
        formData.append("CarStatusDescription", dataCheck.CarStatusDescription);
        formData.append("Paper", dataCheck.Paper);
        formData.append("StandbyDevice", dataCheck.StandbyDevice);
        formData.append("StandbyDeviceInfo", dataCheck.StandbyDeviceInfo);

        if (imageOutside?.length > 0) {
            for (let i = 0; i < imageOutside.length; i++) {
                formData.append("OutsideCarImage", imageOutside[i]);
            }
        }
        if (imageInside?.length > 0) {
            for (let i = 0; i < imageInside.length; i++) {
                formData.append("InsideCarImage", imageInside[i]);
            }
        }

        const res = await fetch(process.env.REACT_APP_API + contractGroupEndpoints.updateReceive + props?.contract?.id, {
            method: "POST",
            headers: {
                // "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: formData,
        })

        const data = await res.json()

        if (res.status > 200) {
            handleOpenNotify(data.message, "error")
            // console.log("Receive-contract status ", res.status, data);
            return
        }

        // console.log("create receive success => ", data)
        handleOpenNotify("Cập nhật hợp đồng nhận xe thành công!", "success")
        props.reloadPage();
        props.close();
    }

    return (
        <>
            {isShow ?
                <Modal
                    open={true}
                    onClose={() => props.close()}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Cập nhật hợp đồng nhận xe
                        </Typography>

                        <form className="container_create max-h-[500px] overflow-y-auto form_scroll pr-3">

                            <div className="info w-full">

                                <DesktopDatePicker
                                    label="Ngày nhận xe"
                                    inputFormat="DD/MM/YYYY"
                                    value={dateReceive}
                                    onChange={handleChangeDateReceive}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ my: 1 }}
                                        />}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Số trên đồng hồ khi nhận"
                                    variant="outlined"
                                    size="small"
                                    required
                                    value={String(state.speedometerNumber)}
                                    type='number'
                                    onChange={(e) => setState({
                                        ...state,
                                        speedometerNumber: Number(e.target.value)
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    value={String(state.fuelPercent)}
                                    id="outlined-basic"
                                    label="Mức nhiên liệu khi nhận (%)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    type='number'
                                    onChange={(e) => setState({
                                        ...state,
                                        fuelPercent: Number(e.target.value)
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Phí cao tốc ETC khi nhận (VND)"
                                    variant="outlined"
                                    value={String(state.currentEtcAmount)}
                                    size="small"
                                    required
                                    type='number'
                                    onChange={(e) => setState({
                                        ...state,
                                        currentEtcAmount: Number(e.target.value)
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-multiline-flexible"
                                    multiline
                                    maxRows={3}
                                    label="Mô tả hiện trạng"
                                    value={String(state.carStatusDescription)}
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        carStatusDescription: e.target.value
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Tình trạng giấy tờ"
                                    variant="outlined"
                                    size="small"
                                    required
                                    value={state.paper}
                                    onChange={(e) => {
                                        setState({
                                            ...state,
                                            paper: e.target.value.trim()
                                        });
                                    }}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Đồ dự phòng"
                                    variant="outlined"
                                    size="small"
                                    required={true}
                                    value={state.standbyDevice}
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            standbyDevice: e.target.value.trim()
                                        })
                                    }
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Thông tin đồ dự phòng"
                                    variant="outlined"
                                    size="small"
                                    required
                                    value={state.standbyDeviceInfo}
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            standbyDeviceInfo: e.target.value.trim()
                                        })
                                    }
                                />
                                <label htmlFor="imageInside" style={{ color: "#158d88", cursor: "pointer" }}>Nội thất xe <AddPhotoAlternateIcon /> </label>
                                <input type="file" id="imageInside" multiple hidden
                                    onChange={(e) => {
                                        const files = e.target.files
                                        setImageInside(files)
                                    }}
                                />
                                <div style={{ minHeight: `${imageInside?.length > 0 ? "200px" : "0px"}`, maxHeight: "300px", overflowY: "auto" }}>
                                    {imageInside &&
                                        Array.from(imageInside).map((file, index) => {
                                            return <>
                                                <img key={index} src={URL.createObjectURL(file)} style={{ width: "100%", maxHeight: "200px", margin: "3px auto", borderRadius: "3px" }} alt="inside" />
                                            </>
                                        })
                                    }
                                </div>


                                <label htmlFor="imageOtside" style={{ color: "#158d88", cursor: "pointer" }}>Ngoại thất xe <AddPhotoAlternateIcon /> </label>
                                <input type="file" id="imageOtside" multiple hidden
                                    onChange={(e) => {
                                        const files = e.target.files
                                        setImageOutside(files)
                                    }}
                                />
                                <div style={{ minHeight: `${imageOutside?.length > 0 ? "200px" : "0px"}`, maxHeight: "300px", overflowY: "auto" }}>
                                    {imageOutside &&
                                        Array.from(imageOutside).map((file, index) => {
                                            return <>
                                                <img key={index} src={URL.createObjectURL(file)} style={{ width: "100%", maxHeight: "200px", margin: "3px auto", borderRadius: "3px" }} alt="outside" />
                                            </>
                                        })
                                    }
                                </div>

                            </div>

                        </form>

                        <div className="action">

                            <Button
                                size='small'
                                variant="contained"
                                startIcon={<BorderColorIcon />}
                                className="btnCreate"
                                type="submit"
                                onClick={handleSubmit}
                            >
                                Cập nhật
                            </Button>

                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className='btnCancel'
                                onClick={() => { props.close() }}
                            >
                                Hủy bỏ
                            </Button>

                        </div>

                        <div className="action">
                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<RemoveRedEyeIcon />}
                                className='btnCancel'
                                onClick={() => { onClickWordPreview() }}
                                style={{ whiteSpace: "nowrap" }}
                            >
                                Xem trước
                            </Button>

                        </div>

                    </Box>
                </Modal>
                :
                <></>
            }

            {
                props?.contract?.receiveContract && !props?.contract?.receiveContract?.isExported &&
                <WordContractPreview
                    stateProps={isPreviewWord}
                    pathApi={contractGroupEndpoints.previewReceive}
                    idGroup={props?.contract?.id}
                    close={onClickCloseWordPreview}
                    data={
                        {
                            dateReceive: dateReceive.format("YYYY-MM-DD"),
                            currentCarState: {
                                speedometerNumber: state?.speedometerNumber,
                                fuelPercent: state?.fuelPercent,
                                currentEtcAmount: state?.currentEtcAmount,
                                carStatusDescription: state?.carStatusDescription
                            },
                            depositItem: {
                                paper: state?.paper,
                                standbyDevice: state?.standbyDevice,
                                standbyDeviceInfo: state?.standbyDeviceInfo,
                            },
                        }
                    }
                />
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}