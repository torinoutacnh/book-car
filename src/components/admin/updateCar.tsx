import { Alert, Box, Button, FormControl, InputLabel, MenuItem, Modal, Select, Snackbar, TextareaAutosize, Typography } from "@mui/material"
import { useState, useEffect, useContext, ChangeEvent } from "react";
import "./styles/crud.scss"
import "./styles/file.scss"
import TextField from '@mui/material/TextField';
import CloseIcon from '@mui/icons-material/Close';
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { useForm } from "react-hook-form";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import { useMutation, useQueryClient } from "react-query";
import { Car, CarClassType } from "interface/car";
import { carEndpoints } from "routers/apiEndpoints";
import { ConfigContext } from "provider";

export function UpdateCar(props: { stateProps: boolean, close: any, reloadPage?: any, car: Car }) {

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const { register, handleSubmit, watch, formState: { errors }, reset } = useForm<Car>();
    const [isShow, setIsShow] = useState(false);


    useEffect(() => {
        setIsShow(props.stateProps)
    }, [props.stateProps])

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            },
        },
    };

    const { configCar } = useContext(ConfigContext)
    const { auth } = useContext(AuthContext);

    const resetFields = () => {
        reset(formValues => ({
            ...formValues,
            descriptionInfo: {
                carBrand: props?.car?.descriptionInfo?.carBrand,
                seatNumber: props?.car?.descriptionInfo?.seatNumber,
                yearCreate: props?.car?.descriptionInfo?.yearCreate,
                carColor: props?.car?.descriptionInfo?.carColor,
                carNumber: props?.car?.descriptionInfo?.carNumber,
                carDescription: props?.car?.descriptionInfo?.carDescription,
                carFuelUse: props?.car?.descriptionInfo?.carFuelUse,
                carName: props?.car?.descriptionInfo?.carName,
                carStyle: props?.car?.descriptionInfo?.carStyle,
                carTransmission: props?.car?.descriptionInfo?.carTransmission,
                carVersion: props?.car?.descriptionInfo?.carVersion,
            },
            carParkingLot: props?.car?.carParkingLot,
            generalInfo: {
                priceForNormalDay: props?.car?.generalInfo?.priceForNormalDay,
                priceForWeekendDay: props?.car?.generalInfo?.priceForWeekendDay,
                priceForHoliday: props?.car?.generalInfo?.priceForHoliday,
                pricePerHourExceed: props?.car?.generalInfo?.pricePerHourExceed,
                pricePerKmExceed: props?.car?.generalInfo?.pricePerKmExceed,
                limitedKmForMonth: props?.car?.generalInfo?.limitedKmForMonth,
                priceForMonth: props?.car?.generalInfo?.priceForMonth,
            },
            carState: {
                carStatusDescription: props?.car?.carState?.carStatusDescription,
                currentEtcAmount: props?.car?.carState?.currentEtcAmount,
                fuelPercent: props?.car?.carState?.fuelPercent,
                speedometerNumber: props?.car?.carState?.speedometerNumber
            },
            carImage: "",
            carClass: CarClassType[props?.car?.carClass as number] as string

        }))
    }
    const submitForm = async (data: Car) => {
        if (!auth) return;

        const list = data.carImage as FileList
        data.carImage = list[0]


        const carUpdate = {
            CarClass: data.carClass,
            CarParkingLot: data.carParkingLot,
            CarBrand: data.descriptionInfo.carBrand,
            CarName: data.descriptionInfo.carName,
            CarVersion: data.descriptionInfo.carVersion,
            CarStyle: data.descriptionInfo.carStyle,
            CarTransmission: data.descriptionInfo.carTransmission,
            CarFuelUse: data.descriptionInfo.carFuelUse,
            SeatNumber: data.descriptionInfo.seatNumber,
            YearCreate: data.descriptionInfo.yearCreate,
            CarColor: data.descriptionInfo.carColor,
            CarNumber: data.descriptionInfo.carNumber,
            CarDescription: data.descriptionInfo.carDescription,
            PriceForNormalDay: data.generalInfo.priceForNormalDay,
            PriceForWeekendDay: data.generalInfo.priceForWeekendDay,
            PriceForHoliday: data.generalInfo.priceForHoliday,
            PriceForMonth: data.generalInfo.priceForMonth,
            LimitedKmForMonth: data.generalInfo.limitedKmForMonth,
            PricePerKmExceed: data.generalInfo.pricePerKmExceed,
            PricePerHourExceed: data.generalInfo.pricePerHourExceed,
            SpeedometerNumber: data.carState.speedometerNumber,
            FuelPercent: data.carState.fuelPercent,
            CurrentEtcAmount: data.carState.currentEtcAmount,
            CarStatusDescription: data.carState.carStatusDescription,
        }

        const formData = new FormData()
        Object.entries(carUpdate).forEach(value => {
            formData.append(value[0], String(value[1]));

        })
        formData.append("CarImage", data.carImage);



        return fetch(process.env.REACT_APP_API + carEndpoints.updateCar + props?.car?.id, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth.accessToken,
            },
            body: formData,
        })

    }
    const queryclient = useQueryClient()
    const mutationUpdateCar = useMutation(
        (car: Car) => submitForm(car),
        {
            onSuccess: async (res, variable, context) => {
                const data = await res.json()
                if (res.status > 200) {
                    handleOpenNotify(data.message, "error")
                    return
                }
                queryclient.invalidateQueries(["carInfo"])
                handleOpenNotify("Cập nhật thông tin xe thành công!", "success")
                props.close()
            },
            onError: (error, variable, context) => {
            }
        })

    const years: number[] = Array.from(
        { length: (2000 - moment().year()) / -1 + 1 },
        (_, i) => moment().year() + i * -1
    );

    const preViewImage = (event: ChangeEvent<HTMLInputElement>) => {

        const file = event.target.files[0]
        const output = document.getElementById("imgCarImage") as HTMLImageElement;
        output.src = URL.createObjectURL(file);
        output.classList.add("active")

    }

    return (
        <>

            {isShow ?
                <Modal
                    open={isShow}
                    onClose={() => {
                        props.close();
                        resetFields();
                    }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Cập nhật thông tin xe
                        </Typography>

                        <form id="formCreateCar" className="container_create" onSubmit={handleSubmit(data => mutationUpdateCar.mutate(data))}>

                            <div className="info">

                                <TextField
                                    id="outlined-basic"
                                    label="Biển kiểm soát"
                                    sx={{ m: 1, width: '30ch' }}
                                    variant="outlined"
                                    size="small"
                                    required
                                    defaultValue={props?.car?.descriptionInfo?.carNumber}
                                    {...register("descriptionInfo.carNumber")}
                                />

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Màu xe</InputLabel>
                                    <Select
                                        size="small"
                                        MenuProps={MenuProps}
                                        defaultValue={props?.car?.descriptionInfo?.carColor}
                                        label={"Màu xe"}
                                        required
                                        {...register("descriptionInfo.carColor")}
                                    >
                                        {
                                            configCar?.carColor?.sort().map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>Màu {item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Kiểu dáng</InputLabel>
                                    <Select
                                        size="small"
                                        MenuProps={MenuProps}
                                        defaultValue={props?.car?.descriptionInfo?.carStyle}
                                        label={"Kiểu dáng"}
                                        required
                                        {...register("descriptionInfo.carStyle")}
                                    >
                                        {
                                            configCar?.carStyle?.sort().map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Truyền động</InputLabel>
                                    <Select
                                        defaultValue={props?.car?.descriptionInfo?.carTransmission}
                                        size="small"
                                        MenuProps={MenuProps}
                                        label={"Truyền động"}
                                        required
                                        {...register("descriptionInfo.carTransmission")}
                                    >
                                        {
                                            configCar?.carTransmission?.map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <TextField
                                    id="outlined-multiline-flexible"
                                    multiline
                                    maxRows={4}
                                    label="Mô tả"
                                    required
                                    sx={{ m: 1, width: '30ch' }}
                                    size='small'
                                    defaultValue={props?.car?.descriptionInfo?.carDescription}
                                    placeholder="Mô tả"
                                    {...register("descriptionInfo.carDescription")}
                                />

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Nhiên liệu sử dụng</InputLabel>
                                    <Select
                                        size="small"
                                        MenuProps={MenuProps}
                                        label={"Nhiên liệu sử dụng"}
                                        defaultValue={props?.car?.descriptionInfo?.carFuelUse}
                                        required
                                        {...register("descriptionInfo.carFuelUse")}
                                    >
                                        {
                                            configCar?.carFuelUse?.map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Trạng thái xe"
                                    variant="outlined"
                                    defaultValue={props?.car?.carState?.carStatusDescription}
                                    size="small"
                                    required
                                    {...register("carState.carStatusDescription")}
                                />
                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Bãi xe</InputLabel>
                                    <Select
                                        size="small"
                                        MenuProps={MenuProps}
                                        defaultValue={props?.car?.carParkingLot}
                                        label={"Bãi xe"}
                                        required
                                        {...register("carParkingLot")}
                                    >
                                        {
                                            configCar?.carParkingSlot?.sort().map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                                        <div className="select">
                                            <label htmlFor="InputcarImage" style={{ fontSize: "15px", color: "#1976d2", cursor: "pointer" }}> <AddPhotoAlternateIcon /> Hình ảnh*</label>
                                            <input
                                                style={{ position: "absolute", right: "2000%" }}
                                                id="InputcarImage"
                                                {...register("carImage")}
                                                type="file"
                                                accept="image/*"
                                                onChange={event => preViewImage(event)}
                                            />
                                        </div>
                                        <img id="imgCarImage" style={{ borderRadius: "4px", width: "100%", maxHeight: "300px" }}
                                            src={`${process.env.REACT_APP_API}${props?.car?.carImage}`}
                                        />
                                    </div>
                                </FormControl>

                            </div>

                            <div className="info">
                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Hãng xe</InputLabel>
                                    <Select
                                        size="small"
                                        MenuProps={MenuProps}
                                        defaultValue={props?.car?.descriptionInfo?.carBrand}
                                        label={"Hãng xe"}
                                        required
                                        {...register("descriptionInfo.carBrand")}
                                    >
                                        {
                                            configCar?.carBrand?.sort().map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Tên xe"
                                    defaultValue={props?.car?.descriptionInfo?.carName}
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("descriptionInfo.carName")}
                                />



                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    defaultValue={props?.car?.descriptionInfo?.carVersion}
                                    label="Phiên bản"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("descriptionInfo.carVersion")}
                                />
                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Số chỗ</InputLabel>
                                    <Select
                                        size="small"
                                        MenuProps={MenuProps}
                                        defaultValue={props?.car?.descriptionInfo?.seatNumber}
                                        label={"Số chỗ"}
                                        required
                                        {...register("descriptionInfo.seatNumber")}
                                    >
                                        {
                                            configCar?.carSeat?.map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item} chỗ</MenuItem>
                                                )
                                            })
                                        }

                                    </Select>
                                </FormControl>
                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Năm sản suất</InputLabel>
                                    <Select
                                        size="small"
                                        MenuProps={MenuProps}
                                        defaultValue={props?.car?.descriptionInfo?.yearCreate}
                                        label={"Năm sản suất"}
                                        required
                                        {...register("descriptionInfo.yearCreate")}
                                    >
                                        {
                                            years.map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>
                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Phân khúc xe</InputLabel>
                                    <Select
                                        size="small"
                                        displayEmpty
                                        MenuProps={MenuProps}
                                        defaultValue={CarClassType[props?.car?.carClass as number]}
                                        label={"Phân khúc xe"}
                                        required
                                        {...register("carClass")}
                                    >
                                        <MenuItem disabled></MenuItem>
                                        {Object.keys(CarClassType)
                                            .filter((value) => isNaN(value as any))
                                            .map((value) => (
                                                <MenuItem
                                                    key={value}
                                                    value={CarClassType[value as any]}
                                                >
                                                    {value}
                                                </MenuItem>
                                            ))}
                                    </Select>
                                </FormControl>

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Giá thuê ngày thường (VND/Ngày)"
                                    defaultValue={props?.car?.generalInfo?.priceForNormalDay}
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.priceForNormalDay")}
                                />
                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    defaultValue={props?.car?.generalInfo?.priceForWeekendDay}
                                    label="Giá thuê cuối tuần (VND/Ngày)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.priceForWeekendDay")}
                                />
                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    defaultValue={props?.car?.generalInfo?.priceForHoliday}
                                    label="Giá thuê ngày lễ (VND/Ngày)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.priceForHoliday")}
                                />
                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Giá thuê tháng"
                                    variant="outlined"
                                    defaultValue={props?.car?.generalInfo?.priceForMonth}
                                    size="small"
                                    required
                                    {...register("generalInfo.priceForMonth")}
                                />
                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    defaultValue={props?.car?.generalInfo?.pricePerKmExceed}
                                    id="outlined-basic"
                                    label="Giá thuê quá km (VND/Km)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.pricePerKmExceed")}
                                />
                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    defaultValue={props?.car?.generalInfo?.pricePerHourExceed}
                                    label="Giá thuê quá giờ (VND/Giờ)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.pricePerHourExceed")}
                                />
                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Giới hạn số km/tháng"
                                    defaultValue={props?.car?.generalInfo?.limitedKmForMonth}
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.limitedKmForMonth")}
                                />
                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Số km trên đồng hồ hiện tại"
                                    variant="outlined"
                                    defaultValue={props?.car?.carState?.speedometerNumber}
                                    size="small"
                                    required
                                    {...register("carState.speedometerNumber")}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Phần trăm nhiên liệu hiện tại (%)"
                                    defaultValue={props?.car?.carState?.fuelPercent}
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("carState.fuelPercent")}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Số tiền ETC hiện tại (VND)"
                                    variant="outlined"
                                    defaultValue={props?.car?.carState?.currentEtcAmount}
                                    size="small"
                                    required
                                    {...register("carState.currentEtcAmount")}
                                />

                            </div>

                        </form>

                        <div className="action">

                            <Button
                                size='small'
                                variant="contained"
                                startIcon={<BorderColorIcon />}
                                className="btnCreate"
                                type="submit"
                                form="formCreateCar"
                            >
                                Cập nhật
                            </Button>
                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className='btnCancel'
                                onClick={() => { props.close(); resetFields(); }}
                            >
                                Hủy bỏ
                            </Button>
                        </div>

                    </Box>
                </Modal>
                : <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}