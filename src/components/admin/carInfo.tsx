import { Button } from '@mui/material';
import { useContext, useState } from 'react';
import './styles/userInfo.scss';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import { useSearchParams } from 'react-router-dom';
import AuthContext from "context/AuthContext";
import { Car } from 'interface/car';
import { Loading } from 'components/loading/loading';
import { UpdateCar } from './updateCar';
import { useQuery } from 'react-query';
import { carEndpoints } from 'routers/apiEndpoints';
import { CarSchedule } from 'components/calendar/car/carSchedule';

export const CarInfo = () => {

    const [searchParams, setSearchParams] = useSearchParams()
    const id = searchParams.get("idCar")

    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    const [isShowUpdateCarInfo, setIsShowUpdateCarInfo] = useState(false);
    const onClickOpenUpdateCarInfo = () => setIsShowUpdateCarInfo(true);
    const onClickCloseUpdateCarInfo = () => setIsShowUpdateCarInfo(false);
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////

    const { auth } = useContext(AuthContext);

    const getCarInfo = async (idCar: string): Promise<Car> => {
        if (!auth) return;
        const res = await fetch(process.env.REACT_APP_API + carEndpoints.carInfo + idCar, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
        })
        const data = await res.json()
        if (!res.ok) {
            throw new Error(JSON.stringify(data))
        }
        return data.data
    }

    const { data, isLoading, isError } = useQuery({
        queryKey: ["carInfo", id],
        queryFn: ({ queryKey }) => getCarInfo(queryKey[1])
    })

    if (isLoading) {
        return <Loading />
    }

    if (isError) {
        return <h1>ERROR...</h1>
    }

    return (
        <>
            <div className="bg-contract__info">
                <div className="container">

                    <div className="flex justify-between items-start gap-x-5 pb-5 flex-wrap md:flex-nowrap">
                        <div className='w-full lg:w-[40%]'>
                            <div>
                                <img src={`${process.env.REACT_APP_API}${data.carImage}`} alt="" className="w-full object-contain max-h-[45vh]" />
                            </div>

                        </div>

                        <div className='w-full lg:w-[60%]'>
                            <div className='grid grid-cols-1 lg:grid-cols-2'>
                                <div>
                                    <h3 className='text-[1.3rem] font-bold uppercase mt-3 lg:mt-0 text-center'>Thông tin xe</h3>
                                    <p className='text__p'>Hãng xe: <span>{data?.descriptionInfo?.carBrand}</span> </p>
                                    <p className='text__p'>Tên xe: <span>{data?.descriptionInfo?.carName}</span> </p>
                                    <p className='text__p'>Phiên bản: <span>{data?.descriptionInfo?.carVersion}</span> </p>
                                    <p className='text__p'>Kiểu dáng: <span>{data?.descriptionInfo?.carStyle}</span> </p>
                                    <p className='text__p'>Màu xe: <span>{data?.descriptionInfo?.carColor}</span> </p>
                                    <p className='text__p'>Phân khúc: <span>{data?.carClass}</span> </p>
                                    <p className='text__p'>Số chỗ ngồi: <span>{data?.descriptionInfo?.seatNumber}</span> </p>
                                    <p className='text__p'>Đời xe: <span>{data?.descriptionInfo?.yearCreate}</span> </p>
                                    <p className='text__p'>Truyền động: <span>{data?.descriptionInfo?.carTransmission}</span> </p>
                                    <p className='text__p'>Nhiên liệu: <span>{data?.descriptionInfo?.carFuelUse}</span> </p>
                                    <p className='text__p'>Biển kiểm soát: <span>{data?.descriptionInfo?.carNumber}</span> </p>
                                    <p className='text__p'>Mô tả: <span>{data?.descriptionInfo?.carDescription}</span> </p>
                                    <h3 className='text-[1.3rem] font-bold uppercase mt-3 lg:mt-0 text-center'>Hiện trạng xe</h3>
                                    <p className="text__p">Số trên đồng hồ hiện tại: <span>{data?.carState?.speedometerNumber} km</span> </p>
                                    <p className="text__p">Mức nhiên liệu hiện tại: <span>{data?.carState?.fuelPercent} %</span> </p>
                                    <p className="text__p">Phí cao tốc ETC hiện tại: <span>{data?.carState?.currentEtcAmount.toLocaleString()} VND</span> </p>
                                    <p className="text__p">Mô tả trạng thái: <span>{data?.carState?.carStatusDescription}</span> </p>

                                </div>

                                <div>
                                    <h3 className='text-[1.3rem] font-bold uppercase mt-3 lg:mt-0 text-center'>Thông chủ xe</h3>
                                    <p className="text__p">Tên: <span>{data?.loaner?.name}</span> </p>
                                    <p className="text__p">Số điện thoại: <span>{data?.loaner?.tel}</span> </p>
                                    <p className="text__p">Công việc: <span>{data?.loaner?.job}</span> </p>
                                    <p className="text__p">Địa chỉ hiện tại: <span>{data?.loaner?.currentAddress}</span> </p>
                                    <p className="text__p">Email: <span>{data?.loaner?.email}</span> </p>
                                    <h3 className='text-[1.3rem] font-bold uppercase mt-3 lg:mt-0 text-center'>Biểu phí thuê xe</h3>
                                    <p className='text__p whitespace-nowrap'> Ngày thường: <span>{data?.generalInfo?.priceForNormalDay.toLocaleString()} VND</span> </p>
                                    <p className='text__p whitespace-nowrap'> Cuối tuần: <span>{data?.generalInfo?.priceForWeekendDay.toLocaleString()} VND</span> </p>
                                    <p className='text__p whitespace-nowrap'> Ngày lễ: <span>{data?.generalInfo?.priceForHoliday.toLocaleString()} VND</span> </p>
                                    <p className='text__p whitespace-nowrap'> Tháng: <span>{data?.generalInfo?.priceForMonth.toLocaleString()} VND</span> </p>
                                    <p className='text__p whitespace-nowrap'> Quá km: <span>{data?.generalInfo?.pricePerKmExceed.toLocaleString()} VND/Km</span> </p>
                                    <p className='text__p whitespace-nowrap'> Quá giờ: <span>{data?.generalInfo?.pricePerHourExceed.toLocaleString()} VND/Giờ</span> </p>
                                    <p className='text__p whitespace-nowrap'> Giới hạn: <span>{data?.generalInfo?.limitedKmForMonth.toLocaleString()} Km/Tháng</span> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='text-center lg:text-right mr-0 lg:mr-[170px]'>
                        <Button
                            variant="outlined"
                            startIcon={<BorderColorIcon />}
                            onClick={() => { onClickOpenUpdateCarInfo() }}
                        >
                            Cập nhật thông tin xe
                        </Button>
                    </div>
                </div>
                <CarSchedule
                    id={data?.id}
                />

                <UpdateCar
                    car={data}
                    stateProps={isShowUpdateCarInfo}
                    close={onClickCloseUpdateCarInfo}
                />

            </div>
        </>
    )
}