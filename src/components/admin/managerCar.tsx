import { DesktopDatePicker } from "@mui/x-date-pickers";
import { FormControl, Button, Alert, Snackbar, MenuItem, Select, Box, InputLabel, Pagination, TextField } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import "./styles/manager.scss";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import { useNavigate } from "react-router-dom";
import AuthContext from "context/AuthContext";
import { Car, CarClassType, SeatNumber, XPagination } from "interface/car";
import { CreateCar } from "./createCar";
import { adminEndpoints } from "routers/endpoints";
import { Loading } from "components/loading/loading";
import { useQuery, useQueryClient } from "react-query";
import { Confirm } from "components/layout/confirm";
import moment, { Moment } from "moment";
import { carEndpoints } from "routers/apiEndpoints";
import { FilterNotFound } from "components/loading/contractNotFound";
import { ConfigContext } from "provider";

interface BodyGetCar {
    carClass: number,
    registerDescription: {
        carBrand: string,
        seatNumber: number,
        yearCreate: number,
        carColor: string
    },
    pageModel: {
        pageNumber: number,
        pageSize: number
    },
    timeRequirement: {
        from: Moment | null,
        to: Moment | null
    }
}

export const ManagerCar = () => {

    const [typeNotifi, setTypeNotifi] = useState("success");
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("");
    const [listCar, setListCar] = useState<Car[]>();

    const navigate = useNavigate();
    const { configCar } = useContext(ConfigContext);

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === "clickaway") {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type);
        setMessageNotify(message);
        setOpenNofity(true);
    };

    const [isShowModalCreateCar, setIsShowModalCreateCar] = useState(false);
    const onClickShowModalCreateCar = () => setIsShowModalCreateCar(true);
    const onClickCloseModalCreateCar = () => setIsShowModalCreateCar(false);
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    const [idCarDelete, setIdCarDelete] = useState<string>()
    const [isShowModalDeleteCar, setIsShowModalDeleteCar] = useState(false);
    const onClickShowModalDeleteCar = (id: string, event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        event.stopPropagation()
        setIdCarDelete(id)
        setIsShowModalDeleteCar(true)
    };
    const onClickCloseModalDeleteCar = () => {
        setIdCarDelete(null)
        setIsShowModalDeleteCar(false);
    }
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    const { auth } = useContext(AuthContext);
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    const initBody: BodyGetCar = {
        carClass: undefined,
        registerDescription: {
            carBrand: undefined,
            carColor: undefined,
            seatNumber: undefined,
            yearCreate: undefined
        },
        pageModel: {
            pageNumber: 1,
            pageSize: 20
        },
        timeRequirement: {
            from: null,
            to: null,
        }
    }
    const [bodyGetCar, setBodyGetCar] = useState<BodyGetCar>(initBody)
    const [count, setCount] = useState(0)
    const [xpagination, setXpagination] = useState<XPagination>()
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    const getAllCar = async (): Promise<Car[]> => {

        const res = await fetch(process.env.REACT_APP_API + carEndpoints.base, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(bodyGetCar)
        })
        const data = await res.json();
        if (!res.ok) {
            // console.log(`get car status ${res.status} => `, data)
            throw new Error(JSON.stringify(data))
        }
        const tmp: XPagination = JSON.parse(res.headers.get("x-pagination"))
        setXpagination(tmp)
        return data.data

    }

    const queryClient = useQueryClient()
    const { data, isLoading, isError, refetch } = useQuery({
        queryKey: ["getAllCar"],
        queryFn: () => getAllCar()
    })
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    useEffect(() => {
        if (!data || !auth) { return }
        setListCar(data)
    }, [data])
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    useEffect(() => {
        if (count === 0) { return }
        refetch()
    }, [count])
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    const onClickDeleteCar = async () => {

        const res = await fetch(process.env.REACT_APP_API + carEndpoints.deleteCar + idCarDelete, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
        })
        const data = await res.json()

        // console.log(`delete car status ${res.status} => `, data)

        if (res.status > 200) {
            handleOpenNotify(data.message, "error")
            return
        }

        handleOpenNotify("Xóa xe thành công", "success");
        queryClient.invalidateQueries(["getAllCar"])
        refetch()
    };
    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////

    const onClickCarInfo = (id: string) => {
        navigate("/" + adminEndpoints.base + "/" + adminEndpoints.carInfo + "?" + new URLSearchParams({ idCar: id }));
    };

    const years: number[] = Array.from(
        { length: (2000 - moment().year()) / -1 + 1 },
        (_, i) => moment().year() + i * -1
    );

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            },
        },
    };


    if (isLoading) {
        return <Loading />
    }

    if (isError) {
        return <h1>ERROR</h1>
    }

    return (
        <>
            {
                listCar ? (
                    <>
                        {/* <>{console.log("data ", data)}</> */}
                        <div className="nav mb-3">
                            <div className="w-full">
                                <Box>
                                    {/* <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-6 gap-4"> */}
                                    <div className="flex justify-start items-center flex-wrap lg:flex-nowrap gap-y-3 lg:gap-y-0 gap-x-3">
                                        <FormControl className="w-[48%] lg:w-[12vw]">
                                            <InputLabel id="demo-simple-select-label" size="small">
                                                Hãng xe
                                            </InputLabel>
                                            <Select
                                                MenuProps={MenuProps}
                                                size="small"
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={bodyGetCar.registerDescription.carBrand}
                                                label="Hãng xe"
                                                defaultValue={"null"}
                                                onChange={(event) => {
                                                    setBodyGetCar({
                                                        ...bodyGetCar,
                                                        registerDescription: {
                                                            ...bodyGetCar.registerDescription,
                                                            carBrand: event.target.value === "null" ? null : event.target.value
                                                        }
                                                    })
                                                    setCount(count + 1)
                                                }}

                                            >
                                                <MenuItem key={9} value={"null"}>
                                                    Tất cả
                                                </MenuItem>
                                                {configCar?.carBrand?.map((item, index) => {
                                                    return (
                                                        <MenuItem key={index} value={item}>{item}</MenuItem>
                                                    )
                                                })}
                                            </Select>
                                        </FormControl>

                                        <FormControl className="w-[48%] lg:w-[10vw]">
                                            <InputLabel id="demo-simple-select-label" size="small">
                                                Đời xe
                                            </InputLabel>
                                            <Select
                                                sx={{ minWidth: 100 }}
                                                MenuProps={MenuProps}
                                                size="small"
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={bodyGetCar?.registerDescription?.yearCreate?.toString()}
                                                label="Đời xe"
                                                defaultValue={"null"}
                                                onChange={(event) => {
                                                    setBodyGetCar({
                                                        ...bodyGetCar,
                                                        registerDescription: {
                                                            ...bodyGetCar.registerDescription,
                                                            yearCreate: event.target.value === "null" ? null : Number(event.target.value)
                                                        }
                                                    })
                                                    setCount(count + 1)
                                                }}

                                            >
                                                <MenuItem key={9} value={"null"}>
                                                    Tất cả
                                                </MenuItem>
                                                {
                                                    years.map((value, index) => {
                                                        return (
                                                            <MenuItem key={index} value={value}>
                                                                {value}
                                                            </MenuItem>
                                                        )
                                                    })
                                                }
                                            </Select>
                                        </FormControl>

                                        <FormControl className="w-[48%] lg:w-[10vw]">
                                            <InputLabel id="demo-simple-select-label" size="small">
                                                Phân khúc xe
                                            </InputLabel>
                                            <Select
                                                MenuProps={MenuProps}
                                                sx={{ minWidth: 100 }}
                                                size="small"
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={CarClassType[bodyGetCar.carClass as number]}
                                                label="Phân khúc xe"
                                                defaultValue={"null"}
                                                onChange={(event) => {
                                                    setBodyGetCar({
                                                        ...bodyGetCar,
                                                        carClass: event.target.value === "null" ? null : CarClassType[event.target.value as keyof typeof CarClassType]
                                                    })
                                                    setCount(count + 1)
                                                }}
                                            >
                                                <MenuItem key={9} value={"null"}>
                                                    Tất cả
                                                </MenuItem>
                                                {
                                                    Object.keys(CarClassType)
                                                        .filter((value) => isNaN(value as any))
                                                        .map((value, index) => {
                                                            return (
                                                                <MenuItem key={index} value={value}>
                                                                    {value}
                                                                </MenuItem>
                                                            )
                                                        })
                                                }
                                            </Select>
                                        </FormControl>

                                        <FormControl className="w-[48%] lg:w-[10vw]">
                                            <InputLabel id="demo-simple-select-label" size="small">
                                                Số chỗ
                                            </InputLabel>
                                            <Select
                                                className="w-full"
                                                MenuProps={MenuProps}
                                                size="small"
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={SeatNumber[bodyGetCar?.registerDescription?.seatNumber as number]}
                                                label="Số chỗ"
                                                defaultValue={"null"}
                                                onChange={(event) => {
                                                    setBodyGetCar({
                                                        ...bodyGetCar,
                                                        registerDescription: {
                                                            ...bodyGetCar.registerDescription,
                                                            seatNumber: event.target.value === "null" ? null : Number(event.target.value)
                                                        }
                                                    })
                                                    setCount(count + 1)
                                                }}

                                            >
                                                <MenuItem key={9} value={"null"}>
                                                    Tất cả
                                                </MenuItem>
                                                {
                                                    configCar?.carSeat?.map((item, index) => {
                                                        return (
                                                            <MenuItem key={index} value={item}>{item} chỗ</MenuItem>
                                                        )
                                                    })
                                                }
                                            </Select>
                                        </FormControl>

                                        <DesktopDatePicker
                                            className="w-[48%] lg:w-[10vw]"
                                            label="Ngày bắt đầu"
                                            inputFormat="DD/MM/YYYY"
                                            value={bodyGetCar?.timeRequirement?.from}
                                            onChange={(newValue) => {
                                                setBodyGetCar({
                                                    ...bodyGetCar,
                                                    timeRequirement: {
                                                        ...bodyGetCar.timeRequirement,
                                                        from: newValue
                                                    }
                                                })
                                                setCount(count + 1)
                                            }}
                                            renderInput={(params) =>
                                                <TextField
                                                    {...params}
                                                    size='small'
                                                />}
                                        />
                                        <DesktopDatePicker
                                            className="w-[48%] lg:w-[10vw]"
                                            label="Ngày kết thúc"
                                            inputFormat="DD/MM/YYYY"
                                            value={bodyGetCar?.timeRequirement?.to}
                                            onChange={(newValue) => {
                                                setBodyGetCar({
                                                    ...bodyGetCar,
                                                    timeRequirement: {
                                                        ...bodyGetCar.timeRequirement,
                                                        to: newValue
                                                    }
                                                })
                                                setCount(count + 1)
                                            }}
                                            renderInput={(params) =>
                                                <TextField
                                                    {...params}
                                                    size='small'
                                                />}
                                        />


                                    </div>
                                </Box>


                            </div>

                            <Button
                                className="addDesktop"
                                variant="outlined"
                                startIcon={<AddIcon />}
                                onClick={() => { onClickShowModalCreateCar() }}
                                style={{ whiteSpace: "nowrap", minWidth: "100px", marginLeft: "10px" }}
                            >
                                Thêm mới
                            </Button>

                            <button className="addMobile" onClick={() => onClickShowModalCreateCar()}>
                                +
                            </button>
                        </div>

                        {listCar.length > 0 ?
                            <div className="overflow-y-auto grid grid-cols-1 md:grid-cols-2 gap-x-3 list_contract">
                                {listCar?.map((item, index) => {
                                    return (
                                        <>
                                            <div className="car mb-3">
                                                <div
                                                    className="manager-car flex justify-between items-center flex-wrap lg:flex-nowrap gap-4 border-2 p-3 rounded-md cursor-pointer custom__bs"
                                                    key={index}
                                                    onClick={() => { onClickCarInfo(item.id) }}
                                                >
                                                    <div className="w-full lg:w-[40%]">
                                                        <div style={{
                                                            paddingTop: `${item.carImage ? "150px" : "0px"}`,
                                                            backgroundImage: `url(${process.env.REACT_APP_API}${item.carImage})`,
                                                            backgroundSize: "contain", backgroundRepeat: "no-repeat",
                                                            backgroundPosition: "50% 50%"
                                                        }}
                                                        ></div>
                                                    </div>

                                                    <div className="manager-car__info w-full lg:w-[40%]">
                                                        <div className="grid grid-cols-2 md:grid-cols-1 mt-2">
                                                            <div>
                                                                <p><span>Hãng xe</span>: {item.descriptionInfo.carBrand}</p>
                                                                <p><span>Đời xe</span>: {item.descriptionInfo.yearCreate}</p>
                                                                <p><span>Phân khúc</span>: {item.carClass}</p>
                                                                <p><span>Số chỗ</span>: {item.descriptionInfo.seatNumber}</p>
                                                                <p><span>Màu xe</span>: {item.descriptionInfo.carColor}</p>
                                                                <p className="whitespace-nowrap"><span>Biển kiểm soát</span>: {item.descriptionInfo.carNumber}</p>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div className="action_info w-full lg:w-[20%] text-center">
                                                        {/* if role admin => show button */}

                                                        <Button
                                                            className="btn-delete-car"
                                                            sx={{ mt: 1, mb: 1 }}
                                                            variant="outlined"
                                                            color="warning"
                                                            startIcon={<DeleteIcon />}
                                                            onClick={(event) => { onClickShowModalDeleteCar(item.id, event) }}
                                                        >
                                                            Xóa
                                                        </Button>
                                                    </div>

                                                </div>
                                            </div>
                                        </>
                                    );
                                })}
                            </div>
                            : <FilterNotFound title="Không tìm thấy xe phù hợp!" />
                        }

                        {xpagination?.TotalPages > 0 &&
                            <div className="flex justify-center items-center">
                                <Pagination
                                    size="small"
                                    count={xpagination?.TotalPages} variant="outlined" color="secondary"
                                    onChange={(event, page) => {
                                        setBodyGetCar({
                                            ...bodyGetCar,
                                            pageModel: {
                                                ...bodyGetCar.pageModel,
                                                pageNumber: page
                                            }
                                        })
                                        setCount(count + 1)
                                    }}
                                />
                            </div>}

                        <CreateCar
                            stateProps={isShowModalCreateCar}
                            close={onClickCloseModalCreateCar}
                            refetch={refetch}
                        />

                        <Confirm
                            close={onClickCloseModalDeleteCar}
                            stateOpen={isShowModalDeleteCar}
                            title={"Xác nhận xóa xe"}
                            function={onClickDeleteCar}
                        />

                        <Snackbar
                            anchorOrigin={{ vertical: "top", horizontal: "right" }}
                            key={"top right"}
                            open={openNotify}
                            autoHideDuration={3000}
                            onClose={handleCloseNotify}
                        >
                            {typeNotifi === "success" ? (
                                <Alert
                                    color={"info"}
                                    onClose={handleCloseNotify}
                                    severity={"success"}
                                    sx={{ width: "100%" }}
                                >
                                    {messageNotify}
                                </Alert>
                            ) : (
                                <Alert
                                    color={"error"}
                                    onClose={handleCloseNotify}
                                    severity={"error"}
                                    sx={{ width: "100%" }}
                                >
                                    {messageNotify}
                                </Alert>
                            )}
                        </Snackbar>
                    </>
                ) : (
                    <Loading />
                )}
        </>
    );
};
