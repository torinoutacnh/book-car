import React from "react";
import AuthContext from "context/AuthContext";
import { useContext } from "react";
import "./styles/admin.scss";
import TimeToLeaveIcon from "@mui/icons-material/TimeToLeave";
import ContentPasteIcon from "@mui/icons-material/ContentPaste";
import PersonIcon from "@mui/icons-material/Person";
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { Link, Outlet, useLocation, useNavigate } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Login from "components/login/Login";
import { RoleUser } from "interface/authenticate";
import { adminEndpoints } from "routers/endpoints";
import Particle from "react-particles-js";
import ConstructionIcon from '@mui/icons-material/Construction';
import BarChartIcon from '@mui/icons-material/BarChart';

const drawerWidth = 240;

interface Props {
    window?: () => Window;
}

export const AdminIndex = (props: Props) => {
    interface IMenu { icon: any, title: string, path: string }
    const listMenu: IMenu[] = [
        {
            icon: <TimeToLeaveIcon />,
            title: "Quản lý xe",
            path: "/" + adminEndpoints.base + "/" + adminEndpoints.car,
        },
        {
            icon: <ContentPasteIcon />,
            title: "Hợp đồng",
            path: "/" + adminEndpoints.base + "/" + adminEndpoints.contract,
        },
        {
            icon: <PersonIcon />,
            title: "Người dùng",
            path: "/" + adminEndpoints.base + "/" + adminEndpoints.user,
        },
        {
            icon: <CalendarMonthIcon />,
            title: "Lịch thuê xe",
            path: "/" + adminEndpoints.base + "/" + adminEndpoints.calendar,
        },
        {
            icon: <ConstructionIcon />,
            title: "Bảo dưỡng",
            path: "/" + adminEndpoints.base + "/" + adminEndpoints.maintenance,
        },
        {
            icon: <BarChartIcon />,
            title: "Thống kê",
            path: "/" + adminEndpoints.base + "/" + adminEndpoints.statistical,
        },
    ];

    const navigate = useNavigate()
    const { auth } = useContext(AuthContext);

    const onClickItemMenu = (data: IMenu) => {
        let items = document.querySelectorAll(".menu_items");
        items.forEach((item) => {
            item.addEventListener("click", function () {
                items.forEach((items) => items.classList.remove("active"));
                item.classList.add("active");
            });
        });
        navigate(data.path);
    };


    const CheckRole = () => {
        if (Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["Customer"])) {
            navigate("/")
        }
    }
    CheckRole()

    const location = useLocation()

    // sidebar
    const { window } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <div className="container_admin">
            <div className="my-[21px]">
                <Link to={"/"}>
                    <img
                        src="https://apivshare.amazingtech.vn/static/logoVshare.jpg"
                        alt=""
                        className="m-auto"
                    />
                </Link>
            </div>
            <Divider />
            <List>
                <div className="menu">
                    {listMenu.map((item, index) => {
                        return (
                            <ul id="menu">
                                <li
                                    className={`menu_items ${location.pathname.toUpperCase().indexOf(item.path.toUpperCase()) > -1 ? "active"
                                        :
                                        location.pathname.toUpperCase() === ("/" + adminEndpoints.base.toUpperCase()) && index === 0 ? "active" : ""
                                        }`}
                                    onClick={() => {
                                        onClickItemMenu(item);
                                    }}
                                >
                                    {item.icon} {item.title}
                                </li>
                            </ul>
                        );
                    })}

                </div>
            </List>
        </div>
    );

    const container = window !== undefined ? () => window().document.body : undefined;
    //sidebar

    return (
        <>
            {CheckRole()}
            {
                auth && Number(RoleUser[auth.userProfile.role]) !== Number(RoleUser["Customer"]) &&

                <div className="admin__vip__pro">
                    <Box sx={{ display: "flex" }}>
                        <CssBaseline />
                        <AppBar
                            position="fixed"
                            sx={{
                                width: { sm: `calc(100% - ${drawerWidth}px)` },
                                ml: { sm: `${drawerWidth}px` },
                            }}
                        >
                            <Toolbar>
                                <IconButton
                                    color="inherit"
                                    aria-label="open drawer"
                                    edge="start"
                                    onClick={handleDrawerToggle}
                                    sx={{ mr: 2, display: { sm: "none" } }}
                                >
                                    <MenuIcon />
                                </IconButton>
                                <Typography variant="h6" noWrap component="div" sx={{ height: "80px", marginLeft: "auto" }}>
                                    <div className="flex justify-end leading-[90px]">
                                        <div className="block text-right mt-[18px] mr-5 text-white-login-admin">
                                            <Login />
                                        </div>
                                        <div className="cursor-pointer" title="Cài đặt">
                                            <Link to={"/" + adminEndpoints.base + "/" + adminEndpoints.config}>
                                                <i className="fa-solid fa-gear"></i>
                                            </Link>
                                        </div>
                                    </div>
                                </Typography>
                            </Toolbar>
                            <Particle className="App-particles__container" />
                        </AppBar>
                        <Box
                            component="nav"
                            sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
                            aria-label="mailbox folders"
                        >
                            {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                            <Drawer
                                container={container}
                                variant="temporary"
                                open={mobileOpen}
                                onClose={handleDrawerToggle}
                                ModalProps={{
                                    keepMounted: true, // Better open performance on mobile.
                                }}
                                sx={{
                                    display: { xs: "block", sm: "none" },
                                    "& .MuiDrawer-paper": { boxSizing: "border-box", width: drawerWidth },
                                }}
                            >
                                {drawer}
                            </Drawer>
                            <Drawer
                                variant="permanent"
                                sx={{
                                    display: { xs: "none", sm: "block" },
                                    "& .MuiDrawer-paper": { boxSizing: "border-box", width: drawerWidth },
                                }}
                                open
                            >
                                {drawer}
                            </Drawer>
                        </Box>
                        <Box
                            component="main"
                            sx={{ flexGrow: 1, p: 3, width: { sm: `calc(100% - ${drawerWidth}px)` } }}
                        >
                            {/* <Toolbar /> */}
                            <Typography paragraph>
                                <div className="wrapper">
                                    {/* {showComponent} */}
                                    <Outlet />
                                </div>
                            </Typography>
                        </Box>
                    </Box>
                </div>
            }
        </>
    );
};
