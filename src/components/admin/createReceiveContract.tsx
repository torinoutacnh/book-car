import { Alert, Box, Button, Modal, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext } from "react";
import "./styles/crud.scss"
import TextField from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { User } from "interface/authenticate";
import { contractGroupEndpoints, userEndpoints } from "routers/apiEndpoints";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';

export function CreateReceiveContract(props: { id: string, stateProps: boolean, close: any, reloadPage: any }) {

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const [isShow, setIsShow] = useState(false);

    useEffect(() => {
        setIsShow(props.stateProps)
    }, [props.stateProps, props.reloadPage])

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }

    const [dateReceive, setDateReceive] = useState<Moment | null>(moment());
    const handleChangeDateReceive = (newValue: Moment | null) => {
        setDateReceive(newValue);
    }

    // const [listUser, setListUser] = useState<User[]>();

    // useEffect(() => {
    //     if (!auth) return;
    //     fetch(process.env.REACT_APP_API + userEndpoints.base, {
    //         method: "GET",
    //         headers: {
    //             "Content-Type": "application/json",
    //             Authorization: "Bearer " + auth.accessToken,
    //         },
    //     })
    //         .then((res) => {
    //             if (res.status >= 500) throw new Error("Lỗi hệ thống!");
    //             if (res.status >= 400) {
    //                 // console.log(res.status);
    //             }
    //             return res.json();
    //         })
    //         .then((data) => setListUser(data.data))
    //         .catch((err) => {

    //             // console.log(err)
    //         }
    //         );
    // }, []);

    const initValue = {
        contractGroupId: props.id,
        speedometerNumber: 0,
        fuelPercent: 0,
        currentEtcAmount: 0,
        carStatusDescription: '',
        paper: '',
        standbyDevice: '',
        standbyDeviceInfo: ''
    }

    const [imageInside, setImageInside] = useState<FileList>(null)
    const [imageOutside, setImageOutside] = useState<FileList>(null)

    const [state, setState] = useState(initValue)

    const { auth } = useContext(AuthContext);

    const handleSubmit = async () => {
        if (!auth) return;

        const checkNumber = {
            SpeedometerNumber: state.speedometerNumber,
            FuelPercent: state.fuelPercent,
            CurrentEtcAmount: state.currentEtcAmount
        }

        const checkString = {
            CarStatusDescription: state.carStatusDescription,
            Paper: state.paper,
            StandbyDecvice: state.standbyDevice,
            StandbyDecviceInfo: state.standbyDeviceInfo
        }

        const checkNull = { a: false }

        Object.values(checkNumber).forEach((value, index) => {
            const tmp = value as number
            if (tmp === 0) {
                checkNull.a = true
            }
        })

        Object.values(checkString).forEach((value, index) => {
            const tmp = value as string
            if (tmp.length === 0) {
                checkNull.a = true
            }
        })

        if (!imageInside || !imageOutside) {
            checkNull.a = true
        }

        if (checkNull.a) {
            handleOpenNotify('Vui lòng nhập đầy đủ thông tin!', 'error')
            return
        }

        const ReceiveItem = {
            Paper: state.paper,
            StandbyDevice: state.standbyDevice,
            StandbyDeviceInfo: state.standbyDeviceInfo
        }

        const CurrentCarState = {
            SpeedometerNumber: state.speedometerNumber,
            FuelPercent: state.fuelPercent,
            CurrentEtcAmount: state.currentEtcAmount,
            CarStatusDescription: state.carStatusDescription
        }

        const formData = new FormData()
        formData.append("CreateReceiveContractData.ContractGroupId", state.contractGroupId);
        formData.append("CreateReceiveContractData.DateReceive", dateReceive.format("YYYY-MM-DD").toString());
        formData.append("CreateReceiveContractData.SpeedometerNumber", CurrentCarState.SpeedometerNumber.toString());
        formData.append("CreateReceiveContractData.FuelPercent", CurrentCarState.FuelPercent.toString());
        formData.append("CreateReceiveContractData.CurrentEtcAmount", CurrentCarState.CurrentEtcAmount.toString());
        formData.append("CreateReceiveContractData.CarStatusDescription", CurrentCarState.CarStatusDescription);
        formData.append("CreateReceiveContractData.Paper", ReceiveItem.Paper);
        formData.append("CreateReceiveContractData.StandbyDevice", ReceiveItem.StandbyDevice);
        formData.append("CreateReceiveContractData.StandbyDeviceInfo", ReceiveItem.StandbyDeviceInfo);

        for (let i = 0; i < imageOutside.length; i++) {
            formData.append("CreateReceiveContractData.OutsideCarImage", imageOutside[i]);
        }

        for (let i = 0; i < imageInside.length; i++) {
            formData.append("CreateReceiveContractData.InsideCarImage", imageInside[i]);

        }

        const res = await fetch(process.env.REACT_APP_API + contractGroupEndpoints.createReceive, {
            method: "POST",
            headers: {
                // "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: formData,
        })

        const data = await res.json()

        if (res.status > 200) {
            handleOpenNotify(data.message, "error")
            // console.log("Receive-contract status ", res.status, data);
            return
        }

        // console.log("create receive success => ", data)
        handleOpenNotify("Tạo hợp đồng nhận xe thành công!", "success")
        setState(initValue);
        props.reloadPage();
        props.close();
    }

    return (
        <>
            {isShow ?
                <Modal
                    open={true}
                    onClose={() => { props.close(); setImageInside(null); setImageOutside(null); setDateReceive(moment()) }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Tạo thông tin hợp đồng nhận xe
                        </Typography>

                        <form className="container_create max-h-[500px] overflow-y-auto form_scroll pr-3">

                            <div className="info w-full">

                                <DesktopDatePicker
                                    label="Ngày nhận xe"
                                    inputFormat="DD/MM/YYYY"
                                    value={dateReceive}
                                    onChange={handleChangeDateReceive}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ my: 1 }}
                                        />}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Số trên đồng hồ khi nhận"
                                    variant="outlined"
                                    size="small"
                                    required
                                    type='number'
                                    onChange={(e) => setState({
                                        ...state,
                                        speedometerNumber: Number(e.target.value)
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Mức nhiên liệu khi nhận (%)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    type='number'
                                    onChange={(e) => setState({
                                        ...state,
                                        fuelPercent: Number(e.target.value)
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Phí cao tốc ETC khi nhận (VND)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    type='number'
                                    onChange={(e) => setState({
                                        ...state,
                                        currentEtcAmount: Number(e.target.value)
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-multiline-flexible"
                                    multiline
                                    maxRows={3}
                                    label="Mô tả hiện trạng"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        carStatusDescription: e.target.value
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Tình trạng giấy tờ"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        paper: e.target.value.trim()
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Đồ dự phòng"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        standbyDevice: e.target.value
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Thông tin đồ dự phòng"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        standbyDeviceInfo: e.target.value
                                    })}
                                />

                                <label htmlFor="imageInside" style={{ color: "#158d88", cursor: "pointer" }}>Nội thất xe <AddPhotoAlternateIcon /> </label>
                                <input type="file" id="imageInside" multiple hidden
                                    onChange={(e) => {
                                        const files = e.target.files
                                        setImageInside(files)
                                    }}
                                />
                                <div style={{ minHeight: `${imageInside?.length > 0 ? "200px" : "0px"}`, maxHeight: "300px", overflowY: "auto" }}>
                                    {imageInside &&
                                        Array.from(imageInside).map((file, index) => {
                                            return <>
                                                <img key={index} src={URL.createObjectURL(file)} style={{ width: "100%", maxHeight: "200px", margin: "3px auto", borderRadius: "3px" }} alt="inside" />
                                            </>
                                        })
                                    }
                                </div>


                                <label htmlFor="imageOtside" style={{ color: "#158d88", cursor: "pointer" }}>Ngoại thất xe <AddPhotoAlternateIcon /> </label>
                                <input type="file" id="imageOtside" multiple hidden
                                    onChange={(e) => {
                                        const files = e.target.files
                                        setImageOutside(files)
                                    }}
                                />
                                <div style={{ minHeight: `${imageOutside?.length > 0 ? "200px" : "0px"}`, maxHeight: "300px", overflowY: "auto" }}>
                                    {imageOutside &&
                                        Array.from(imageOutside).map((file, index) => {
                                            return <>
                                                <img key={index} src={URL.createObjectURL(file)} style={{ width: "100%", maxHeight: "200px", margin: "3px auto", borderRadius: "3px" }} alt="outside" />
                                            </>
                                        })
                                    }
                                </div>


                            </div>

                        </form>

                        <div className="action">

                            <Button
                                size='small'
                                variant="contained"
                                startIcon={<AddIcon />}
                                className="btnCreate"
                                type="submit"
                                onClick={handleSubmit}
                            >
                                Thêm mới
                            </Button>

                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className='btnCancel'
                                onClick={() => { props.close() }}
                            >
                                Hủy bỏ
                            </Button>

                        </div>

                    </Box>
                </Modal>
                :
                <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}