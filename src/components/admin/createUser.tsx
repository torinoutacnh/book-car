import { Alert, Box, Button, FormControlLabel, Modal, Radio, RadioGroup, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext } from "react";
import "./styles/crud.scss"
import TextField from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { userEndpoints } from "routers/apiEndpoints";

export function CreateUser(props: { stateProps: boolean, close: any, reloadPage: any }) {

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const [isShow, setIsShow] = useState(false);

    useEffect(() => {
        setIsShow(props.stateProps)
    }, [props.stateProps, props.reloadPage])

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }

    const [dateReceiveCCCD, setDateReceiveCCCD] = useState<Moment | null>(moment());
    const [dateReceivePassPort, setDateReceivePassPort] = useState<Moment | null>(moment());
    const [dateReceiveGPLX, setDateReceiveGPLX] = useState<Moment | null>(moment());

    const handleChangeDateCCCD = (newValue: Moment | null) => {
        setDateReceiveCCCD(newValue);
    }

    const handleChangeDatePassPort = (newValue: Moment | null) => {
        setDateReceivePassPort(newValue);
    }

    const handleChangeDateGPLX = (newValue: Moment | null) => {
        setDateReceiveGPLX(newValue);
    }

    const initValue = {
        typeInfo: 'citizenIdentificationInfo',
        name: '',
        tel: '',
        job: '',
        currentAddress: '',
        email: '',
        passWord: '',
        numberCCCD: '',
        addressCCCD: '',
        numberPassPort: '',
        addressPassPort: '',
        numberGPLX: '',
    }

    const [state, setState] = useState(initValue)

    const { auth } = useContext(AuthContext);

    const handleSubmit = () => {
        if (!auth) return;

        const isNull = { a: false }

        const tmp1 = [state.name, state.tel, state.job, state.currentAddress, state.email, state.numberGPLX]
        tmp1.map((item) => {
            if (item.length === 0) {
                isNull.a = true;
                return;
            }
        })

        const tmp2 = state.typeInfo === "passportInfo" ? [state.numberPassPort, state.addressPassPort] : [state.numberCCCD, state.addressCCCD]
        tmp2.map((item) => {
            if (item.length === 0) {
                isNull.a = true;
                return;
            }
        })

        if (isNull.a) {
            handleOpenNotify('Vui lòng nhập đầy đủ thông tin!', 'error')
            return
        }

        if (((state.passWord).length < 8) || ((state.passWord).length > 20)) {
            handleOpenNotify('Mật khẩu từ 8 đến 20 ký tự!', 'error')
            return
        }

        const dataBody = {
            name: state.name,
            tel: state.tel,
            job: state.job,
            currentAddress: state.currentAddress,
            email: state.email,
            passWord: state.passWord,
            citizenIdentificationInfo: {
                number: state.numberCCCD,
                address: state.addressCCCD,
                dateReceive: dateReceiveCCCD,
            },
            passportInfo: {
                number: state.numberPassPort,
                address: state.addressPassPort,
                dateReceive: dateReceivePassPort,
            },
            drivingLicenseInfo: {
                number: state.numberGPLX,
                dateReceive: dateReceiveGPLX,
            }
        }

        fetch(process.env.REACT_APP_API + userEndpoints.createUser, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(dataBody),
        })
            .then((res) => {
                if (res.status >= 400) {
                    handleOpenNotify("Vui lòng kiểm tra lại thông tin!", "error");
                }
                return res.json();
            })
            .then((data) => {
                // console.log("create user success => ", data)
                handleOpenNotify("Tạo người dùng thành công!", "success")
                setState(initValue);
                props.reloadPage();
            })
            .catch((error) => {
                // console.log("error >>>>>>", error);
            })
        props.close();
    }

    return (
        <>

            {isShow ?
                <Modal
                    open={isShow}
                    onClose={() => { props.close(); setState(initValue) }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Tạo hồ sơ người dùng
                        </Typography>

                        <form className="container_create">

                            <div className="info">

                                <TextField
                                    id="outlined-basic"
                                    label="Tên"
                                    sx={{ m: 1, width: '30ch' }}
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        name: e.target.value
                                    })}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Số điện thoại"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        tel: e.target.value
                                    })}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Công việc"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        job: e.target.value
                                    })}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Địa chỉ hiện tại"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        currentAddress: e.target.value
                                    })}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    type="email"
                                    id="outlined-basic"
                                    label="Email"
                                    variant="outlined"
                                    // inputProps={{ pattern: "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" }}
                                    size="small"
                                    required
                                    // onChange={(e) => {
                                    //     setState({ ...state, email: e.target.value });
                                    //     const reg = new RegExp("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$");
                                    //     setValid(reg.test(e.target.value));
                                    // }}
                                    onChange={(e) => setState({
                                        ...state,
                                        email: e.target.value
                                    })}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Mật khẩu"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        passWord: e.target.value
                                    })}
                                />

                            </div>

                            <div className="another_info">

                                <RadioGroup
                                    sx={{ m: 1, width: '30ch' }}
                                    row
                                    aria-labelledby="demo-radio-buttons-group-label"
                                    defaultValue={state.typeInfo}
                                    name="radio-buttons-group"
                                    onChange={(e) => { setState({ ...state, typeInfo: e.target.value }) }}
                                >
                                    <FormControlLabel value="citizenIdentificationInfo" control={<Radio />} label="CCCD" />
                                    <FormControlLabel value="passportInfo" control={<Radio />} label="Passport" />
                                </RadioGroup>

                                <TextField
                                    type='text'
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label={`Số ${state.typeInfo === "passportInfo" ? "Passport" : "CCCD"}`}
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => {
                                        state.typeInfo === "passportInfo" ?
                                            setState({
                                                ...state,
                                                numberPassPort: e.target.value
                                            })
                                            :
                                            setState({
                                                ...state,
                                                numberCCCD: e.target.value
                                            })
                                    }}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label='Nơi cấp'
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => {
                                        state.typeInfo === "passportInfo" ?
                                            setState({
                                                ...state,
                                                addressPassPort: e.target.value
                                            })
                                            :
                                            setState({
                                                ...state,
                                                addressCCCD: e.target.value
                                            })
                                    }}
                                />

                                <DesktopDatePicker
                                    label={`Ngày cấp ${state.typeInfo === "passportInfo" ? "Passport" : "CCCD"}`}
                                    inputFormat="DD/MM/YYYY"
                                    value={state.typeInfo === "passportInfo" ? dateReceivePassPort : dateReceiveCCCD}
                                    onChange={state.typeInfo === "passportInfo" ? handleChangeDatePassPort : handleChangeDateCCCD}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ m: 1, width: '30ch' }}

                                        />}
                                />

                                <TextField
                                    type='text'
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label='Số GPLX'
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        numberGPLX: e.target.value
                                    })}
                                />

                                <DesktopDatePicker
                                    label="Ngày cấp GPLX"
                                    inputFormat="DD/MM/YYYY"
                                    value={dateReceiveGPLX}
                                    onChange={handleChangeDateGPLX}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ m: 1, width: '30ch' }}
                                        />}
                                />

                            </div>

                        </form>

                        <div className="action">

                            <Button
                                size='small'
                                variant="contained"
                                startIcon={<AddIcon />}
                                className="btnCreate"
                                type="submit"
                                onClick={handleSubmit}
                            >
                                Tạo
                            </Button>
                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className='btnCancel'
                                onClick={() => { props.close(); setState(initValue) }}
                            >
                                Hủy bỏ
                            </Button>
                        </div>

                    </Box>
                </Modal>
                : <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}