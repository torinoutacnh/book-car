import { Alert, Box, Button, Modal, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext } from "react";
import "./styles/crud.scss"
import TextField from '@mui/material/TextField';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { User } from "interface/authenticate";
import BorderColorIcon from '@mui/icons-material/BorderColor';
import { userEndpoints } from "routers/apiEndpoints";
import { useQueryClient } from "react-query";


export function UpdateUser(props: { stateProps: boolean, close: any, reloadPage: any, user: User }) {

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const [isShow, setIsShow] = useState(false);

    interface initValue {
        name: string,
        tel: string,
        job: string,
        currentAddress: string,
        email: string,
        numberCCCD: string,
        addressCCCD: string,
        numberPassPort: string,
        addressPassPort: string,
        numberGPLX: string,
    }

    const [state, setState] = useState<initValue>()

    useEffect(() => {
        setIsShow(props.stateProps)

        const defaultValue = {
            name: props?.user?.name,
            tel: props?.user?.tel,
            job: props?.user?.job,
            currentAddress: props?.user?.currentAddress,
            email: props?.user?.email,
            numberCCCD: props?.user?.citizenIdentificationInfo?.number,
            addressCCCD: props?.user?.citizenIdentificationInfo?.address,
            numberPassPort: props?.user?.passportInfo?.number,
            addressPassPort: props?.user?.passportInfo?.address,
            numberGPLX: props?.user?.drivingLicenseInfo?.number,
        }
        setState(defaultValue)
    }, [props.stateProps, props.reloadPage])

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }

    const [dateReceiveCCCD, setDateReceiveCCCD] = useState<Moment | null>(moment(props?.user?.citizenIdentificationInfo?.dateReceive));
    const [dateReceivePassPort, setDateReceivePassPort] = useState<Moment | null>(moment(props?.user?.passportInfo?.dateReceive));
    const [dateReceiveGPLX, setDateReceiveGPLX] = useState<Moment | null>(moment(props?.user?.drivingLicenseInfo?.dateReceive));

    const handleChangeDateCCCD = (newValue: Moment | null) => {
        setDateReceiveCCCD(newValue);
    }

    const handleChangeDatePassPort = (newValue: Moment | null) => {
        setDateReceivePassPort(newValue);
    }

    const handleChangeDateGPLX = (newValue: Moment | null) => {
        setDateReceiveGPLX(newValue);
    }



    const { auth } = useContext(AuthContext);

    const queryClient = useQueryClient()

    const handleSubmit = async () => {
        if (!auth) return;

        const isNull = { a: false, b: true }

        const tmp1 = [state.name, state.tel, state.job, state.currentAddress, state.email, state.numberGPLX]
        tmp1.map((item) => {
            if (item.length === 0) {
                isNull.a = true;
                return;
            }
        })

        if ((state.numberPassPort && state.addressPassPort) || (state.numberCCCD && state.addressCCCD)) {
            isNull.b = false;
        }

        if (isNull.a || isNull.b) {
            handleOpenNotify('Vui lòng nhập đầy đủ thông tin!', 'error')
            return
        }

        const body_res = {
            name: state.name,
            tel: state.tel,
            job: state.job,
            currentAddress: state.currentAddress,
            email: state.email,
            citizenIdentificationInfo: {
                number: state.numberCCCD,
                address: state.addressCCCD,
                dateReceive: dateReceiveCCCD,
            },
            passportInfo: {
                number: state.numberPassPort,
                address: state.addressPassPort,
                dateReceive: dateReceivePassPort,
            },
            drivingLicenseInfo: {
                number: state.numberGPLX,
                dateReceive: dateReceiveGPLX,
            },
        }

        const res = await fetch(process.env.REACT_APP_API + userEndpoints.updateUser + props.user?.id, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(body_res),
        })
        const data = await res.json()

        if (res.status > 200) {
            handleOpenNotify(data.message, "error")
            // console.log(`update user info status ${res.status} => `, data)
            return
        }

        // console.log(`update user info status ${res.status} => `, data)
        handleOpenNotify("Cập nhật hồ sơ người dùng thành công!", "success")
        queryClient.invalidateQueries("getUserInfo")
        props.reloadPage();
        props.close();

    }

    return (
        <>

            {isShow ?
                <Modal
                    open={isShow}
                    onClose={() => props.close()}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Cập nhật hồ sơ người dùng
                        </Typography>

                        <form className="container_create">

                            <div className="info">

                                <TextField
                                    id="outlined-basic"
                                    label="Tên"
                                    sx={{ m: 1, width: '30ch' }}
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        name: e.target.value.trim()
                                    })}
                                    defaultValue={state.name}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Số điện thoại"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        tel: e.target.value.trim()
                                    })}
                                    defaultValue={state.tel}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Công việc"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        job: e.target.value.trim()
                                    })}
                                    defaultValue={state.job}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Địa chỉ hiện tại"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        currentAddress: e.target.value.trim()
                                    })}
                                    defaultValue={state.currentAddress}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    type={"email"}
                                    id="outlined-basic"
                                    label="Email"
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        email: e.target.value.trim()
                                    })}
                                    defaultValue={state.email}

                                />
                                <TextField
                                    type='text'
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label='Số GPLX'
                                    variant="outlined"
                                    size="small"
                                    required
                                    onChange={(e) => setState({
                                        ...state,
                                        numberGPLX: e.target.value.trim()
                                    })}
                                    defaultValue={state.numberGPLX}

                                />

                                <DesktopDatePicker
                                    label="Ngày cấp GPLX"
                                    inputFormat="DD/MM/YYYY"
                                    value={dateReceiveGPLX}
                                    onChange={handleChangeDateGPLX}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ m: 1, width: '30ch' }}
                                        />}
                                />
                            </div>

                            <div className="another_info">

                                <TextField
                                    type='text'
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label={`Hộ chiếu số`}
                                    variant="outlined"
                                    size="small"
                                    required
                                    defaultValue={state.numberPassPort}
                                    onChange={(e) => {
                                        setState({
                                            ...state,
                                            numberPassPort: e.target.value.trim()
                                        })
                                    }}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label='Nơi cấp hộ chiếu'
                                    variant="outlined"
                                    size="small"
                                    required
                                    defaultValue={state.addressPassPort}
                                    onChange={(e) => {
                                        setState({
                                            ...state,
                                            addressPassPort: e.target.value.trim()
                                        })
                                    }}
                                />

                                <DesktopDatePicker
                                    label={`Ngày cấp hộ chiếu`}
                                    inputFormat="DD/MM/YYYY"
                                    value={dateReceivePassPort}
                                    onChange={handleChangeDatePassPort}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ m: 1, width: '30ch' }}

                                        />}
                                />
                                <TextField
                                    type='text'
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label={`CCCD/CMND số`}
                                    variant="outlined"
                                    size="small"
                                    required
                                    defaultValue={state.numberCCCD}
                                    onChange={(e) => {
                                        setState({
                                            ...state,
                                            numberCCCD: e.target.value.trim()
                                        })
                                    }}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label='Nơi cấp CCCD/CMND'
                                    variant="outlined"
                                    size="small"
                                    required
                                    defaultValue={state.addressCCCD}
                                    onChange={(e) => {
                                        setState({
                                            ...state,
                                            addressCCCD: e.target.value.trim()
                                        })
                                    }}
                                />

                                <DesktopDatePicker
                                    label={`Ngày cấp CCCD/CMND`}
                                    inputFormat="DD/MM/YYYY"
                                    value={dateReceiveCCCD}
                                    onChange={handleChangeDateCCCD}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ m: 1, width: '30ch' }}
                                        />}
                                />

                            </div>

                        </form>

                        <div className="action">

                            <Button
                                size='small'
                                variant="contained"
                                startIcon={<BorderColorIcon />}
                                className="btnCreate"
                                type="submit"
                                onClick={handleSubmit}
                            >
                                Cập nhật
                            </Button>
                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className='btnCancel'
                                onClick={() => { props.close() }}
                            >
                                Hủy bỏ
                            </Button>
                        </div>

                    </Box>
                </Modal>
                : <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}