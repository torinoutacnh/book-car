import {
	Button,
	Alert,
	Snackbar,
	Grid,
	FormControlLabel,
	Radio,
	RadioGroup,
} from "@mui/material";
import { useContext, useEffect, useState } from "react";
import "./styles/userInfo.scss";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import { RoleUser, User, VieRole } from "../../interface/authenticate";
import { useSearchParams } from "react-router-dom";
import PersonPinIcon from "@mui/icons-material/PersonPin";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import PublicIcon from "@mui/icons-material/Public";
import SaveIcon from "@mui/icons-material/Save";
import AuthContext from "context/AuthContext";
import { UpdateUser } from "./updateUser";
import { Loading } from "components/loading/loading";
import { userEndpoints } from "routers/apiEndpoints";

export const UserInfo = () => {
	const [user, setUser] = useState<User>();
	const [isShowUpdateRole, setIsShowUpdateRole] = useState(false);
	const [roleChange, setRoleChange] = useState<number>();
	//get params
	const [searchParams, setSearchParams] = useSearchParams();
	const id = searchParams.get("idUser");
	//

	const [typeNotifi, setTypeNotifi] = useState("success");
	const [openNotify, setOpenNofity] = useState(false);
	const [messageNotify, setMessageNotify] = useState("");
	const handleCloseNotify = (
		event?: React.SyntheticEvent | Event,
		reason?: string
	) => {
		if (reason === "clickaway") {
			return;
		}
		setOpenNofity(false);
	};

	const handleOpenNotify = (message: string, type: string) => {
		setTypeNotifi(type);
		setMessageNotify(message);
		setOpenNofity(true);
	};

	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isShowUpdateUserInfo, setIsShowUpdateUserInfo] = useState(false);
	const onClickOpenUpdateUserInfo = () => setIsShowUpdateUserInfo(true);
	const onClickCloseUpdateUserInfo = () => setIsShowUpdateUserInfo(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////

	const [reRender, setReRender] = useState(0);
	const reloadPage = () => {
		setReRender(reRender + 1);
	};

	const { auth } = useContext(AuthContext);
	useEffect(() => {
		if (!auth) return;
		fetch(process.env.REACT_APP_API + userEndpoints.userInfo + id, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + auth.accessToken,
			},
		})
			.then((res) => {
				if (res.status >= 400) throw new Error();
				return res.json();
			})
			.then((data) => {
				const user: User = data.data;
				// console.log("useEffect user info => ", user)
				setUser(user);
			})
			.catch((error) => {
				// console.log(error)
			});
	}, [auth?.userProfile?.id, reRender]);

	const onChangeRole = (event: any) => {
		const data: string = event.target.value;

		Object.keys(RoleUser)
			.filter((value) => isNaN(value as any))
			.map((value, index) => {
				if (value === data) {
					setRoleChange(index);
					setRole(index);
					return;
				}
			});
	};

	const [role, setRole] = useState<number>();

	const handleUpdateRole = async (id: string) => {
		if (!auth) return;
		const res = await fetch(
			process.env.REACT_APP_API + userEndpoints.updateRole + id,
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + auth.accessToken,
				},
				body: JSON.stringify({ role: role }),
			}
		);
		const data = await res.json();

		if (res.status > 200) {
			handleOpenNotify(data.message, "error");
			// console.log("update role status ", res.status, " => ", data)
			return;
		}

		handleOpenNotify("Cập nhật thành công", "success");
		reloadPage();
	};

	return (
		<>
			{user ? (
				<>
					<div className="container_userInfo">
						<Grid container className="grid">
							<Grid item xs={12} sm={9} md={5} lg={5} className="left">
								<div className="infomation">
									<PersonPinIcon className="iconPerson" />

									<div className="box_infomation">
										<span className="box_infomation_title">Họ tên: </span>
										<span className="box_infomation_content"> {user.name}</span>
									</div>
									<div className="box_infomation">
										<span className="box_infomation_title">Nghề nghiệp: </span>
										<span className="box_infomation_content">{user.job}</span>
									</div>

									<div className="box_infomation">
										<span className="box_infomation_title">Chức vụ: </span>
										<span className="box_infomation_content">
											{VieRole[Number(RoleUser[user.role])]}
										</span>
									</div>
								</div>

								<div className="contact">
									<div className="box_infomation">
										<span className="box_infomation_title">
											<LocalPhoneIcon style={{ color: "blue" }} /> &nbsp; Số
											điện thoại:
										</span>
										<span className="box_infomation_content"> {user.tel}</span>
									</div>
									<div className="box_infomation">
										<span className="box_infomation_title">
											<PublicIcon style={{ color: "black" }} /> &nbsp; Địa chỉ:
										</span>
										<span className="box_infomation_content">
											{user.currentAddress}
										</span>
									</div>
									<div className="box_infomation">
										<span className="box_infomation_title">
											<MailOutlineIcon style={{ color: "red" }} /> &nbsp; Email:
										</span>
										<span className="box_infomation_content">{user.email}</span>
									</div>
								</div>

								{Number(RoleUser[auth?.userProfile?.role]) ===
									Number(RoleUser["Admin"]) && (
										<Button
											variant="outlined"
											size="small"
											style={{ marginTop: "15px" }}
											startIcon={<BorderColorIcon />}
											onClick={() => {
												setIsShowUpdateRole(!isShowUpdateRole);
												setRoleChange(-1);
											}}>
											Cập nhật chức vụ
										</Button>
									)}

								{isShowUpdateRole && (
									<div className="box_update_role_left">
										<RadioGroup
											style={{ display: "flex", flexDirection: "column" }}
											row
											aria-labelledby="demo-radio-buttons-group-label"
											defaultValue={user.role}
											name="radio-buttons-group"
											onChange={(event) => {
												onChangeRole(event);
											}}>
											<FormControlLabel
												value={RoleUser[2]}
												control={<Radio />}
												label={"Quản lí"}
											/>
											<FormControlLabel
												value={RoleUser[1]}
												control={<Radio />}
												label={"NV thẩm định"}
											/>
											<FormControlLabel
												value={RoleUser[3]}
												control={<Radio />}
												label={"NV điều hành"}
											/>
											<FormControlLabel
												value={RoleUser[4]}
												control={<Radio />}
												label={"NV sale"}
											/>
											<FormControlLabel
												value={RoleUser[0]}
												control={<Radio />}
												label={"Khách hàng"}
											/>
										</RadioGroup>

										{roleChange >= 0 && (
											<Button
												aria-label="save"
												size="small"
												style={{ color: "black" }}
												onClick={() => {
													handleUpdateRole(user.id);
												}}>
												<SaveIcon fontSize="medium" color="info" /> Lưu
											</Button>
										)}
									</div>
								)}

								<Button
									variant="outlined"
									size="small"
									style={{ marginTop: "15px" }}
									startIcon={<BorderColorIcon />}
									onClick={() => {
										onClickOpenUpdateUserInfo();
									}}>
									Cập nhật thông tin
								</Button>
							</Grid>

							<Grid item xs={12} sm={9} md={6.9} lg={6.9} className="right">
								<h3 className="title_right">GIẤY TỜ TÙY THÂN</h3>

								<div className="identification">
									<div className="box_identification">
										<span className="box_identification_title">CMND/CCCD số: </span>
										<span className="box_identification_content">
											{user.citizenIdentificationInfo?.number}
										</span>
									</div>
									<div className="box_identification">
										<span className="box_identification_title">Nơi cấp: </span>
										<span className="box_identification_content">
											{user.citizenIdentificationInfo?.address}
										</span>
									</div>
									<div className="box_identification">
										<span className="box_identification_title">Ngày cấp: </span>
										<span className="box_identification_content">
											{user.citizenIdentificationInfo?.number === "" ? "" : user.citizenIdentificationInfo?.dateReceive.toString().slice(0, 10)}
										</span>
									</div>
								</div>
								<div className="identification">
									<div className="box_identification">
										<span className="box_identification_title">Hộ chiếu số:	</span>
										<span className="box_identification_content">
											{user.passportInfo?.number}
										</span>
									</div>
									<div className="box_identification">
										<span className="box_identification_title">Nơi cấp: </span>
										<span className="box_identification_content">
											{user.passportInfo?.address}
										</span>
									</div>
									<div className="box_identification">
										<span className="box_identification_title">Ngày cấp: </span>
										<span className="box_identification_content">
											{user.passportInfo?.number === "" ? "" : user.passportInfo?.dateReceive.toString().slice(0, 10)}
										</span>
									</div>
								</div>
								<div className="identification">
									<div className="box_identification">
										<span className="box_identification_title">GPLX số: </span>
										<span className="box_identification_content">
											{user.drivingLicenseInfo?.number}
										</span>
									</div>

									<div className="box_identification">
										<span className="box_identification_title">Ngày cấp: </span>
										<span className="box_identification_content">
											{user.drivingLicenseInfo?.dateReceive.toString().slice(0, 10)}
										</span>
									</div>
								</div>
							</Grid>
						</Grid>
					</div>

					<UpdateUser
						close={onClickCloseUpdateUserInfo}
						stateProps={isShowUpdateUserInfo}
						reloadPage={reloadPage}
						user={user}
					/>

					<Snackbar
						anchorOrigin={{ vertical: "top", horizontal: "right" }}
						key={"top right"}
						open={openNotify}
						autoHideDuration={3000}
						onClose={handleCloseNotify}>
						{typeNotifi === "success" ? (
							<Alert
								color={"info"}
								onClose={handleCloseNotify}
								severity={"success"}
								sx={{ width: "100%" }}>
								{messageNotify}
							</Alert>
						) : (
							<Alert
								color={"error"}
								onClose={handleCloseNotify}
								severity={"error"}
								sx={{ width: "100%" }}>
								{messageNotify}
							</Alert>
						)}
					</Snackbar>
				</>
			) : (
				<Loading />
			)}
		</>
	);
};
