import { Alert, Button, IconButton, Snackbar, Typography } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import "./styles/userInfo.scss";
import { useNavigate, useSearchParams } from "react-router-dom";
import AuthContext from "context/AuthContext";
import { Contract, RentStatus } from "interface/contract";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { styled } from "@mui/material/styles";
import MuiAccordion, { AccordionProps } from "@mui/material/Accordion";
import MuiAccordionSummary, { AccordionSummaryProps } from "@mui/material/AccordionSummary";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import "./styles/contractInfo.scss";
import AddIcon from "@mui/icons-material/Add";
import { CreateExpertiseContract } from "./createExpertiseContract";
// import { CreateRentContract } from "./createRentContract";
import { CreateTransferContract } from "./createTransferContract";
import { CreateReceiveContract } from "./createReceiveContract";
import { RoleUser } from "interface/authenticate";
import CheckIcon from "@mui/icons-material/Check";
import DeleteIcon from "@mui/icons-material/Delete";
import UploadFileIcon from "@mui/icons-material/UploadFile";
import { CreateFile } from "./createFile";
import { ExpertiseInfo, ExpertiseStatus } from "interface/request/contractGroupReq";
import { UpdateFile } from "./updateFile";
import { adminEndpoints } from "routers/endpoints";
import { Loading } from "components/loading/loading";
import { UpdateContract } from "./updateContract";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import { Confirm } from "components/layout/confirm";
import { CarClassType } from "interface/car";
import { UpdateExpertiseInfo } from "./updateExpertiseContract";
import EditIcon from "@mui/icons-material/Edit";
import PreviewIcon from "@mui/icons-material/Preview";
import { UpdateTransferContract } from "./updateTransferContract";
import { UpdateRentContract } from "./updateRentContract";
import { contractGroupEndpoints } from "routers/apiEndpoints";
import { UpdateReceiveContract } from "./updateReceiveContract";
import { WordContractPath } from "components/Word/WordContractPath";
import { SignContractComponent } from "components/Word/signContract";

export const ContractInfo = () => {
	const Accordion = styled((props: AccordionProps) => (
		<MuiAccordion disableGutters elevation={0} square {...props} />
	))(({ theme }) => ({
		border: `1px solid ${theme.palette.divider}`,
		"&:not(:last-child)": { marginBottom: "10px" },
		"&:before": { display: "none" },
	}));

	const AccordionSummary = styled((props: AccordionSummaryProps) => (
		<MuiAccordionSummary
			expandIcon={<ExpandMoreIcon sx={{ fontSize: "0.9rem" }} />}
			{...props}
		/>
	))(({ theme }) => ({
		backgroundColor: theme.palette.mode === "dark" ? "rgba(255, 255, 255, .05)" : "rgba(0, 0, 0, .03)",
		flexDirection: "row-reverse",
		"& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": { transform: "rotate(90deg)" },
		"& .MuiAccordionSummary-content": { marginLeft: theme.spacing(1) },
	}));

	const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
		padding: theme.spacing(2),
		borderTop: "1px solid rgba(0, 0, 0, .125)",
	}));

	const [contract, setContract] = useState<Contract>();
	//get params
	const [searchParams, setSearchParams] = useSearchParams();
	const id = searchParams.get("idContract");
	//
	interface ExpandedContract {
		ThongTinThue: boolean;
		ThongTinThamDinh: boolean;
		HopDongThue: boolean;
		HopDongGiaoXe: boolean;
		HopDongNhanXe: boolean;
	}

	const initExpaned: ExpandedContract = {
		ThongTinThue: false,
		ThongTinThamDinh: false,
		HopDongThue: false,
		HopDongGiaoXe: false,
		HopDongNhanXe: false,
	};

	const [defaultExpandedContract, setDefaultExpandedCar] = useState<ExpandedContract>(initExpaned);

	const navigate = useNavigate();

	const [typeNotifi, setTypeNotifi] = useState("success");
	const [openNotify, setOpenNofity] = useState(false);
	const [messageNotify, setMessageNotify] = useState("");
	const handleCloseNotify = (
		event?: React.SyntheticEvent | Event,
		reason?: string
	) => {
		if (reason === "clickaway") {
			return;
		}
		setOpenNofity(false);
	};

	const handleOpenNotify = (message: string, type: string) => {
		setTypeNotifi(type);
		setMessageNotify(message);
		setOpenNofity(true);
	};

	const [reRender, setReRender] = useState(0);
	const reloadPage = () => {
		setReRender(reRender + 1);
	};

	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isCreateExpertise, setIsCreateExpertise] = useState(false);
	const onClickCreateExpertise = () => setIsCreateExpertise(true);
	const onClickCloseExpertise = () => setIsCreateExpertise(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isCreateRent, setIsCreateRent] = useState(false);
	const onClickCreateRent = () => setIsCreateRent(true);
	const onClickCloseRent = () => setIsCreateRent(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isCreateTransfer, setIsCreateTransfer] = useState(false);
	const onClickCreateTransfer = () => setIsCreateTransfer(true);
	const onClickCloseTransfer = () => setIsCreateTransfer(false);
	const [isUpdateTransfer, setIsUpdateTransfer] = useState(false);
	const onClickUpdateTransfer = () => setIsUpdateTransfer(true);
	const onClickCloseUpdateTransfer = () => setIsUpdateTransfer(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isCreateReceive, setIsCreateReceive] = useState(false);
	const onClickCreateReceive = () => setIsCreateReceive(true);
	const onClickCloseReceive = () => setIsCreateReceive(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isCreateFile, setIsCreateFile] = useState(false);
	const onClickCreateFile = () => setIsCreateFile(true);
	const onClickCloseFile = () => setIsCreateFile(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isUpdateFile, setIsUpdateFile] = useState(false);
	const onClickUpdateFile = () => setIsUpdateFile(true);
	const onClickCloseUpdateFile = () => setIsUpdateFile(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isUpdateContract, setIsUpdateContract] = useState(false);
	const onClickUpdateContract = () => setIsUpdateContract(true);
	const onClickCloseUpdateContract = () => setIsUpdateContract(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isUpdateExpertiseInfo, setIsUpdateExpertiseInfo] = useState(false);
	const onClickUpdateExpertiseInfo = () => setIsUpdateExpertiseInfo(true);
	const onClickCloseUpdateExpertiseInfo = () => setIsUpdateExpertiseInfo(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isUpdateRentContract, setIsUpdateRentContract] = useState(false);
	const onClickUpdateRentContract = () => setIsUpdateRentContract(true);
	const onClickCloseUpdateRentContract = () => setIsUpdateRentContract(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isUpdateTranferContract, setIsUpdateTranferContract] = useState(false);
	const onClickUpdateTranferContract = () => setIsUpdateTranferContract(true);
	const onClickCloseUpdateTranferContract = () =>
		setIsUpdateTranferContract(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [isUpdateReceiveContract, setIsUpdateReceiveContract] = useState(false);
	const onClickUpdateReceiveContract = () => setIsUpdateReceiveContract(true);
	const onClickCloseUpdateReceiveContract = () =>
		setIsUpdateReceiveContract(false);
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	const [filePathWord, setFilePathWord] = useState("");
	const [isShowWordContractPath, setIsShowWordContractPath] = useState(false);
	const onClickShowWordContractPath = (path: string) => {
		setFilePathWord(path);
		setIsShowWordContractPath(true);
	};
	const onClickCloseShowWordContractPath = () => {
		setFilePathWord("");
		setIsShowWordContractPath(false);
	};
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////

	const [isShowModalDeleteContract, setIsShowModalDeleteContract] = useState(false);
	const onClickShowModalDeleteContract = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		event.stopPropagation();
		setIsShowModalDeleteContract(true);
	};
	const onClickCloseModalDeleteContract = () => {
		setIsShowModalDeleteContract(false);
	};

	const { auth } = useContext(AuthContext);

	useEffect(() => {
		if (!auth) return;
		fetch(
			process.env.REACT_APP_API + contractGroupEndpoints.contractInfo + id,
			{
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + auth.accessToken,
				},
			}
		)
			.then((res) => {
				if (res.status >= 400) throw new Error();
				return res.json();
			})
			.then((data) => setContract(data.data))
			.catch((error) => {
				// console.log(error)
			});
	}, [auth?.userProfile?.id, reRender]);

	const onClickFinishContract = async (path: string) => {
		if (!auth) {
			return;
		}

		const res = await fetch(process.env.REACT_APP_API + path + contract?.id, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + auth.accessToken,
			},
		});
		const data = await res.json();

		if (res.status > 200) {
			// console.log("finish status ", res.status, " => ", data);
			handleOpenNotify(data.message, "error");
			return;
		}

		// console.log("finish status ", res.status, " => ", data);
		handleOpenNotify("Duyệt hợp đồng thành công", "success");
		reloadPage();
	};

	const handleDeleteContract = async () => {
		const res = await fetch(
			process.env.REACT_APP_API +
			contractGroupEndpoints.deleteContract +
			contract?.id,
			{
				method: "GET",
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + auth.accessToken,
				},
			}
		);
		const data = await res.json();

		if (res.status > 200) {
			handleOpenNotify(data.message, "error");
			// console.log("finish status ", res.status, " => ", data);
			return;
		}
		handleOpenNotify("Xóa hợp đồng thành công", "success");
		setTimeout(() => { navigate("/" + adminEndpoints.base + "/" + adminEndpoints.contract) }, 500);
	};

	return (
		<>
			{/* <>{console.log("contract", contract)}</> */}
			<div className="bg-contract__info">
				{contract ? (
					<>
						<div className="container-contract-info min-h-[500px] ">
							<h3
								className="title text-center pt-[20px]"
								style={{ position: "relative" }}>
								Thông tin hợp đồng
							</h3>
							<div
								style={{
									display: "flex",
									justifyContent: "center",
									marginBottom: "20px",
								}}>
								{!contract?.contractFile ? (
									<Button
										variant="outlined"
										startIcon={<UploadFileIcon />}
										onClick={() => {
											onClickCreateFile();
										}}>
										Upload file
									</Button>
								) : (
									<Button
										variant="outlined"
										startIcon={<UploadFileIcon />}
										onClick={() => {
											onClickUpdateFile();
										}}>
										Update file
									</Button>
								)}

								{Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["Admin"]) &&
									<Button
										variant="outlined"
										style={{ marginLeft: "20px" }}
										color="error"
										startIcon={<DeleteIcon />}
										onClick={(e) => {
											onClickShowModalDeleteContract(e);
										}}>
										Xóa
									</Button>
								}
							</div>

							<div className="flex justify-between items-start gap-4 pb-5 flex-wrap md:flex-nowrap">
								<div className="w-[100%]">
									{/* Thông tin thuê */}
									<Accordion
										defaultExpanded={defaultExpandedContract.ThongTinThue}>
										<AccordionSummary
											expandIcon={<ExpandMoreIcon />}
											aria-controls="panel1a-content"
											id="panel1a-header"
											onClick={() => {
												setDefaultExpandedCar({
													...defaultExpandedContract, ThongTinThue: !defaultExpandedContract.ThongTinThue
												});
											}}>
											<Typography className="title">
												Thông tin thuê xe
											</Typography>
										</AccordionSummary>
										<AccordionDetails>
											<Typography className="flex w-[100%] flex-wrap md:flex-nowrap">
												<div className="w-[100%] md:w-[33.33%]">
													<h3 className="font-bold uppercase text-center">
														Thông tin yêu cầu
													</h3>
													<p className="text__p">
														Thuê từ ngày:
														<span> {contract?.rentFrom?.toString().slice(0, 10)} </span>
													</p>
													<p className="text__p">
														Thuê đến ngày:
														<span> {contract?.rentTo?.toString().slice(0, 10)} </span>
													</p>
													<p className="text__p">
														Hãng xe:
														<span> {contract?.requireDescriptionInfo?.carBrand} </span>
													</p>
													<p className="text__p">
														Đời xe:
														<span> {contract?.requireDescriptionInfo?.yearCreate} </span>
													</p>
													<p className="text__p">
														Phân khúc xe:
														<span> {CarClassType[contract?.carClass]} </span>
													</p>
													<p className="text__p">
														Số chỗ ngồi:
														<span> {contract?.requireDescriptionInfo?.seatNumber} chỗ</span>
													</p>
													<p className="text__p">
														Màu xe:
														<span> {contract?.requireDescriptionInfo?.carColor} </span>
													</p>
												</div>

												<div className="w-[100%] md:w-[33.33%]">
													<h3 className="font-bold uppercase text-center">
														Thông tin liên quan
													</h3>
													<p className="text__p">
														Thuê xe:
														<span> {contract?.expertiseInfo?.isFirstTimeRent ? "Lần đầu thuê xe" : ExpertiseInfo[contract?.expertiseInfo?.status]} </span>
													</p>
													<p className="text__p">Thông tin MXH:</p>
													{contract?.customerSocialInfo?.facebook && (
														<p className="text__p pl-2 whitespace-nowrap overflow-hidden text-ellipsis w-[300px]">
															- Facebook: <a
																className="text-blue-700"
																target="_blank"
																href={contract.customerSocialInfo.facebook}>
																{contract?.customerSocialInfo?.facebook}
															</a>
														</p>
													)}
													{contract?.customerSocialInfo?.zalo && (
														<p className="text__p pl-2">
															- Zalo:
															<span> {contract?.customerSocialInfo?.zalo} </span>
														</p>
													)}
													{contract?.customerSocialInfo?.linkedin && (
														<p className="text__p pl-2">
															- Linkedin:
															<span> {contract?.customerSocialInfo?.linkedin} </span>
														</p>
													)}
													{contract?.customerSocialInfo?.other && (
														<p className="text__p pl-2">
															- Khác:
															<span> {contract?.customerSocialInfo?.other} </span>
														</p>
													)}
													<p className="text__p">
														Thông tin công ty:
														<span> {contract?.companyInfo} </span>
													</p>
													<p className="text__p">
														Số điện thoại công ty:
														<span> {contract?.relativeTel} </span>
													</p>
													<p className="text__p">
														Thông tin bổ sung:
														<span> {contract?.addtionalInfo} </span>
													</p>
												</div>

												<div className="w-[100%] md:w-[33.33%]">
													<h3 className="font-bold uppercase text-center">
														Thông tin người thuê
													</h3>
													<p className="text__p">
														Họ tên:
														<span> {contract?.renter?.name} </span>
													</p>
													<p className="text__p">
														Số điện thoại:
														<span> {contract?.renter?.tel} </span>
													</p>
													<p className="text__p">
														Công việc:
														<span> {contract?.renter?.job} </span>
													</p>
													<p className="text__p">
														Email:
														<span> {contract?.renter?.email} </span>
													</p>
													<p className="text__p">
														Mục đích thuê: <span> {contract?.rentPurpose} </span>
													</p>
													<p className="text__p">
														Trạng thái hợp đồng:
														<span> {RentStatus[contract?.status]} </span>
													</p>
												</div>
											</Typography>
										</AccordionDetails>

										{(Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["Admin"]) ||
											Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["ExpertiseStaff"])) &&
											contract?.status <= 2 && !contract?.expertiseContract?.isExported && (
												<div className="text-center mb-[16px]">
													<Button
														variant="outlined"
														size="small"
														startIcon={<BorderColorIcon />}
														onClick={() => {
															onClickUpdateContract();
														}}
														style={{
															whiteSpace: "nowrap",
															minWidth: "100px",
															marginLeft: "10px",
															textTransform: "none",
														}}>
														Cập nhật thông tin thuê xe
													</Button>
												</div>
											)}
									</Accordion>

									{/*Thông tin thẩm định*/}
									<Accordion
										defaultExpanded={defaultExpandedContract.ThongTinThamDinh}>
										<AccordionSummary
											expandIcon={<ExpandMoreIcon />}
											aria-controls="panel1a-content"
											onClick={() => {
												setDefaultExpandedCar({
													...defaultExpandedContract, ThongTinThamDinh: !defaultExpandedContract.ThongTinThamDinh
												});
											}}
											id="panel1a-header">
											<Typography className="title">
												Thông tin thẩm định
											</Typography>
										</AccordionSummary>
										<AccordionDetails>
											<Typography className="flex w-[100%] flex-wrap md:flex-nowrap">
												<div className="w-[100%] md:w-[33.33%]">
													<h3 className="font-bold uppercase text-center">
														Kết quả thẩm định
													</h3>
													<p className="text__p">
														Ngày thẩm định:
														<span> {contract?.expertiseContract?.expertiseDate.toString().slice(0, 10)} </span>
													</p>
													<p className="text__p">
														Mô tả thẩm định:
														<span> {contract?.expertiseContract?.description} </span>
													</p>
													<p className="text__p">
														Kết quả:
														<span> {ExpertiseStatus[contract?.expertiseContract?.result]} </span>
													</p>
													<p className="text__p">
														Kết quả khác:
														<span> {contract?.expertiseContract?.resultOther} </span>
													</p>
													<p className="text__p">
														Độ tin cậy:
														<span> {contract?.expertiseContract?.trustLevel} </span>
													</p>
												</div>

												<div className="w-[100%] md:w-[33.33%]">
													<h3 className="font-bold uppercase text-center">
														Thông tin đặt cọc
													</h3>
													<p className="text__p">
														Tài sản đặt cọc:
														<span> {contract?.expertiseContract?.depositInfo?.asset} </span>
													</p>
													<p className="text__p">
														Mô tả:
														<span> {contract?.expertiseContract?.depositInfo?.description} </span>
													</p>
													<p className="text__p">
														Số tiền đặt cọc:
														<span> {
															!contract?.expertiseContract?.depositInfo?.downPayment
																? ""
																: contract?.expertiseContract?.depositInfo?.downPayment?.toLocaleString() + " VND"
														}
														</span>
													</p>
													<p className="text__p">
														Số tiền cần thanh toán:
														<span> {!contract?.expertiseContract ? "" : contract?.expertiseContract?.paymentAmount?.toLocaleString() + " VND"}</span>
													</p>
												</div>

												<div className="w-[100%] md:w-[33.33%]">
													<h3 className="font-bold uppercase text-center">
														Thông tin nhân viên thẩm định
													</h3>
													<p className="text__p">
														Họ tên:
														<span> {contract?.expertiser?.name} </span>
													</p>
													<p className="text__p">
														Số điện thoại:
														<span> {contract?.expertiser?.tel} </span>
													</p>
													<p className="text__p">
														Email:
														<span> {contract?.expertiser?.email} </span>
													</p>
													<p className="text__p">
														Trạng thái hợp đồng:
														<span> {contract?.expertiseContract ? contract?.expertiseContract?.isExported ? "Đã duyệt" : "Chưa duyệt" : ""} </span>
													</p>
												</div>
											</Typography>
										</AccordionDetails>

										{(Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["Admin"]) ||
											Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["ExpertiseStaff"])) && (
												<div className="text-center mb-[16px]">
													{!contract?.expertiseContract && (
														<Button
															className="addDesktop"
															variant="outlined"
															size="small"
															startIcon={<AddIcon />}
															onClick={() => {
																onClickCreateExpertise();
															}}
															style={{
																whiteSpace: "nowrap",
																minWidth: "100px",
																marginLeft: "10px",
																textTransform: "none",
															}}>
															Tạo thẩm định
														</Button>
													)}

													{contract?.expertiseContract?.isExported === false && contract?.expertiseContract?.result === 0 && (
														<Button
															className="addDesktop"
															variant="outlined"
															size="small"
															startIcon={<CheckIcon />}
															onClick={() => {
																onClickFinishContract(contractGroupEndpoints.exportExpertise)
															}}
															style={{
																whiteSpace: "nowrap",
																minWidth: "100px",
																marginLeft: "10px",
																textTransform: "none",
															}}>
															Thẩm định hợp đồng
														</Button>
													)}
													{!contract?.expertiseContract?.isExported && contract?.expertiseContract && (
														<Button
															className="addDesktop"
															variant="outlined"
															size="small"
															startIcon={<BorderColorIcon />}
															onClick={() => {
																onClickUpdateExpertiseInfo()
															}}
															style={{
																whiteSpace: "nowrap",
																minWidth: "100px",
																marginLeft: "10px",
																textTransform: "none",
															}}>
															Cập nhật thông tin
														</Button>
													)}
												</div>
											)}
									</Accordion>

									{/*Hợp đồng thuê*/}
									{contract?.expertiseContract?.isExported && (
										<Accordion
											defaultExpanded={defaultExpandedContract.HopDongThue}>
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												onClick={() => {
													setDefaultExpandedCar({
														...defaultExpandedContract, HopDongThue: !defaultExpandedContract.HopDongThue
													});
												}}
												aria-controls="panel1a-content"
												id="panel1a-header">
												<Typography
													style={{ display: "flex", alignItems: "center" }}
													className="title">
													Hợp đồng thuê
												</Typography>

												{Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) && contract?.rentContract?.isExported && (
													<div className="">
														<IconButton
															aria-label="Preview"
															color="primary"
															onClick={() => {
																onClickShowWordContractPath(contract?.rentContract?.filePath)
															}}>
															<PreviewIcon />
														</IconButton>
													</div>
												)}
											</AccordionSummary>
											<AccordionDetails>
												<Typography className="flex w-[100%] flex-wrap md:flex-nowrap">

													<div className="w-[100%] md:w-[33.33%]">
														<h3 className="font-bold uppercase text-center">
															Thông tin xe
														</h3>
														<p className="text__p">
															Phân khúc:
															<span> {contract?.car?.carClass} </span>
														</p>
														<p className="text__p">
															Hãng xe:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.carBrand} </span>
														</p>
														<p className="text__p">
															Tên xe:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.carName} </span>
														</p>
														<p className="text__p">
															Phiên bản:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.carVersion} </span>
														</p>
														<p className="text__p">
															Đời xe:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.yearCreate} </span>
														</p>
														<p className="text__p">
															Truyền động:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.carTransmission} </span>
														</p>
														<p className="text__p">
															Kiểu dáng:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.carStyle} </span>
														</p>
														<p className="text__p">
															Số chỗ ngồi:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.seatNumber} chỗ</span>
														</p>
														<p className="text__p">
															Màu xe:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.carColor} </span>
														</p>
														<p className="text__p">
															Nhiên liệu sử dụng:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.carFuelUse} </span>
														</p>
														<p className="text__p">
															Biển kiểm soát:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.carNumber} </span>
														</p>
														<p className="text__p">
															Mô tả:
															<span> {contract?.rentContract?.carDescriptionInfoAtRent?.carDescription} </span>
														</p>
													</div>

													<div className="w-[100%] md:w-[33.33%]">
														<h3 className="font-bold uppercase text-center">
															Hiện trạng xe
														</h3>
														<p className="text__p">
															Số trên đồng hồ hiện tại:
															<span> {contract?.car?.carState?.speedometerNumber} </span>
														</p>
														<p className="text__p">
															Mức nhiên liệu hiện tại:
															<span> {contract?.car?.carState?.fuelPercent} %</span>
														</p>
														<p className="text__p">
															Phí cao tốc ETC hiện tại:
															<span> {contract?.car?.carState?.currentEtcAmount.toLocaleString()} VND</span>
														</p>
														<p className="text__p">
															Mô tả hiện trạng:
															<span> {contract?.car?.carState?.carStatusDescription} </span>
														</p>
														<h3 className="font-bold uppercase text-center">
															Biểu phí thuê
														</h3>
														<p className="text__p">
															Ngày thường:
															<span> {contract?.rentContract?.carGeneralInfoAtRent?.priceForNormalDay.toLocaleString()} VND</span>
														</p>
														<p className="text__p">
															Ngày cuối tuần:
															<span> {contract?.rentContract?.carGeneralInfoAtRent?.priceForWeekendDay.toLocaleString()} VND</span>
														</p>
														<p className="text__p">
															Ngày lễ:
															<span> {contract?.rentContract?.carGeneralInfoAtRent?.priceForHoliday.toLocaleString()} VND</span>
														</p>
														<p className="text__p">
															Tháng:
															<span> {contract?.rentContract?.carGeneralInfoAtRent?.priceForMonth.toLocaleString()} VND</span>
														</p>
														<p className="text__p">
															Quá giờ:
															<span> {contract?.rentContract?.carGeneralInfoAtRent?.pricePerHourExceed.toLocaleString()} VND/Giờ</span>
														</p>
														<p className="text__p">
															Quá km:
															<span> {contract?.rentContract?.carGeneralInfoAtRent?.pricePerKmExceed.toLocaleString()} VND/Km</span>
														</p>
														<p className="text__p">
															Giới hạn:
															<span> {contract?.rentContract?.carGeneralInfoAtRent?.limitedKmForMonth.toLocaleString()} Km/Tháng</span>
														</p>
													</div>

													<div className="w-[100%] md:w-[33.33%]">
														<h3 className="font-bold uppercase text-center">
															Thông tin đặt cọc và thời gian thuê
														</h3>
														<p className="text__p">
															Thuê từ ngày:
															<span> {contract?.rentContract?.rentFrom.toString().slice(0, 10)} </span>
														</p>
														<p className="text__p">
															Thuê đến ngày:
															<span> {contract?.rentContract?.rentTo.toString().slice(0, 10)} </span>
														</p>
														<p className="text__p">
															Tài sản đặt cọc:
															<span> {contract?.rentContract?.depositItem?.asset}</span>
														</p>
														<p className="text__p">
															Thông tin tài sản:
															<span> {contract?.rentContract?.depositItem?.description}</span>
														</p>
														<p className="text__p">
															Số tiền đã đặt cọc:
															<span> {contract?.rentContract?.depositItem?.downPayment?.toLocaleString()} VND </span>
														</p>
														<p className="text__p">
															Số tiền cần thanh toán:
															<span> {contract?.rentContract?.paymentAmount?.toLocaleString()} VND </span>
														</p>
														<h3 className="font-bold uppercase text-center">
															Thông tin nhân viên phụ trách
														</h3>
														<p className="text__p">
															Họ tên:
															<span> {contract?.representative?.name} </span>
														</p>
														<p className="text__p">
															Số điện thoại:
															<span> {contract?.representative?.tel} </span>
														</p>
														<p className="text__p">
															Email:
															<span> {contract?.representative?.email} </span>
														</p>
														<p className="text__p">
															Trạng thái hợp đồng:
															<span> {contract?.rentContract?.isExported ? "Đã duyệt" : "Chưa duyệt"} </span>
														</p>
														<p className="text__p">
															Chữ kí điện tử: &nbsp;
															<span>
																{contract?.rentContract?.customerSignature && "Khách hàng đã kí"}
																{contract?.rentContract?.customerSignature && contract?.rentContract?.staffSignature && ", "}
																{contract?.rentContract?.staffSignature && "nhân viên đã kí"}
															</span>
														</p>
													</div>
												</Typography>
											</AccordionDetails>

											{(Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["Admin"]) ||
												Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["OperatorStaff"])) && (
													<div className="text-center mb-[16px]">
														{contract?.rentContract?.isExported === false && (
															<Button
																className="addDesktop"
																variant="outlined"
																size="small"
																startIcon={<CheckIcon />}
																onClick={() => {
																	onClickFinishContract(contractGroupEndpoints.exportRent)
																}}
																style={{
																	whiteSpace: "nowrap",
																	minWidth: "100px",
																	marginLeft: "10px",
																	textTransform: "none",
																}}>
																Duyệt hợp đồng
															</Button>
														)}

														{!contract?.rentContract?.isExported && (
															<Button
																className="addDesktop"
																variant="outlined"
																size="small"
																startIcon={<BorderColorIcon />}
																onClick={() => {
																	onClickUpdateRentContract()
																}}
																style={{
																	whiteSpace: "nowrap",
																	minWidth: "100px",
																	marginLeft: "10px",
																	textTransform: "none",
																}}>
																Cập nhật thông tin
															</Button>
														)}

														{!contract?.transferContract &&

															<div
																style={{
																	display: "flex",
																	width: "100%",
																	justifyContent: "space-evenly",
																}}>
																{Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) &&
																	contract?.rentContract && contract?.rentContract?.isExported &&
																	!contract?.rentContract?.customerSignature && (
																		<SignContractComponent
																			Id={contract?.id}
																			userId={auth?.userProfile?.id}
																			reloadPage={reloadPage}
																			handleOpenNotify={handleOpenNotify}
																			role={0}
																		/>
																	)}

																{Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) &&
																	contract?.rentContract && contract?.rentContract?.isExported &&
																	!contract?.rentContract?.staffSignature && (
																		<SignContractComponent
																			Id={contract?.id}
																			userId={auth?.userProfile?.id}
																			reloadPage={reloadPage}
																			handleOpenNotify={handleOpenNotify}
																			role={1}
																		/>
																	)}
															</div>

														}
													</div>
												)}
										</Accordion>
									)}

									{/*Hợp đồng giao xe*/}
									{contract?.rentContract?.isExported && (
										<Accordion
											defaultExpanded={defaultExpandedContract.HopDongGiaoXe}>
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												onClick={() => {
													setDefaultExpandedCar({
														...defaultExpandedContract, HopDongGiaoXe: !defaultExpandedContract.HopDongGiaoXe
													});
												}}
												id="panel1a-header">
												<Typography
													style={{ display: "flex", alignItems: "center" }}
													className="title">
													Hợp đồng giao xe
												</Typography>

												{Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) && contract?.transferContract && (
													<IconButton
														aria-label="Preview"
														color="primary"
														onClick={(e) => {
															onClickShowWordContractPath(contract?.transferContract?.filePath)
														}}>
														<PreviewIcon />
													</IconButton>
												)}
											</AccordionSummary>
											<AccordionDetails>
												<Typography className="flex w-[100%] flex-wrap">

													<div className="flex w-[100%] flex-wrap md:flex-nowrap">

														<div className="w-[100%] md:w-[33.33%]">
															<h3 className="font-bold uppercase text-center">
																Thông tin vận chuyển
															</h3>
															<p className="text__p">
																Ngày vận chuyển:
																<span> {contract?.transferContract?.dateTransfer?.toString().slice(0, 10)} </span>
															</p>
															<p className="text__p">
																Số trên đồng hồ khi giao:
																<span> {contract?.transferContract?.currentCarState?.speedometerNumber} </span>
															</p>
															<p className="text__p">
																Mức nhiên liệu khi giao:
																<span> {!contract?.transferContract ? "" : contract?.transferContract?.currentCarState?.fuelPercent + " %"}</span>
															</p>
															<p className="text__p">
																Phí cao tốc ETC khi giao:
																<span> {!contract?.transferContract ? "" : contract?.transferContract?.currentCarState?.currentEtcAmount.toLocaleString() + " VND"}</span>
															</p>
															<p className="text__p">
																Mô tả hiện trạng:
																<span> {contract?.transferContract?.currentCarState?.carStatusDescription} </span>
															</p>
														</div>

														<div className="w-[100%] md:w-[33.33%]">
															<h3 className="font-bold uppercase text-center">
																Thông tin tài sản giao xe
															</h3>
															<p className="text__p">
																Tình trạng giấy tờ:
																<span> {contract?.transferContract?.transferItem?.paper} </span>
															</p>
															<p className="text__p">
																Thiết bị dự phòng:
																<span> {contract?.transferContract?.transferItem?.standbyDevice} </span>
															</p>
															<p className="text__p">
																Thông tin thiết bị:
																<span> {contract?.transferContract?.transferItem?.standbyDeviceInfo} </span>
															</p>
														</div>

														<div className="w-[100%] md:w-[33.33%]">
															<h3 className="font-bold uppercase text-center">
																Thông tin nhân viên giao xe
															</h3>
															<p className="text__p">
																Họ tên:
																<span> {contract?.transferer?.name} </span>
															</p>
															<p className="text__p">
																Số điện thoại:
																<span> {contract?.transferer?.tel} </span>
															</p>
															<p className="text__p">
																Email:
																<span> {contract?.transferer?.email} </span>
															</p>
															<p className="text__p">
																Trạng thái hợp đồng:
																<span> {contract?.transferContract ? contract?.transferContract?.isExported ? "Đã duyệt" : "Chưa duyệt" : ""} </span>
															</p>
															<p className="text__p">
																Chữ kí điện tử: &nbsp;
																<span>
																	{contract?.transferContract?.customerSignature && "Khách hàng đã kí"}
																	{contract?.transferContract?.customerSignature && contract?.transferContract?.staffSignature && ", "}
																	{contract?.transferContract?.staffSignature && "nhân viên đã kí"}
																</span>
															</p>
														</div>

													</div>

													<div className="flex w-[100%] flex-wrap md:flex-nowrap gap-x-3 mt-3">
														<div className="w-full lg:w-[50%]">
															<h3 className="font-bold uppercase text-center">
																Hình ảnh nội thất
															</h3>
															<div style={{ height: "200px", overflowX: "auto", display: "flex", gap: "5px" }}>
																{
																	contract?.transferContract?.insideCarImage.map((image, index) => {
																		return <>
																			{<img key={index} src={`${process.env.REACT_APP_API}/Uploads/${contract?.id?.replaceAll("-", "")}/${image}`} style={{ width: "100%" }} alt="inside" />}
																		</>
																	})
																}
															</div>
														</div>
														<div className="w-full lg:w-[50%]">
															<h3 className="font-bold uppercase text-center">
																Hình ảnh ngoại thất
															</h3>
															<div style={{ height: "200px", overflowX: "auto", display: "flex", gap: "5px" }}>
																{
																	contract?.transferContract?.outsideCarImage.map((image, index) => {
																		return <>
																			{<img key={index} src={`${process.env.REACT_APP_API}/Uploads/${contract?.id?.replaceAll("-", "")}/${image}`} style={{ width: "100%" }} alt="outside" />}
																		</>
																	})
																}
															</div>
														</div>
													</div>

												</Typography>
											</AccordionDetails>

											{(Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["Admin"]) ||
												Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["OperatorStaff"])) && (
													<div className="text-center mb-[16px]">
														{!contract?.transferContract && (
															<Button
																className="addDesktop"
																variant="outlined"
																size="small"
																startIcon={<AddIcon />}
																onClick={() => {
																	onClickCreateTransfer();
																}}
																style={{
																	whiteSpace: "nowrap",
																	minWidth: "100px",
																	marginLeft: "10px",
																	textTransform: "none",
																}}>
																Tạo hợp đồng
															</Button>
														)}

														{contract?.transferContract?.isExported === false && (
															<Button
																className="addDesktop"
																variant="outlined"
																size="small"
																startIcon={<CheckIcon />}
																onClick={() => {
																	onClickFinishContract(
																		contractGroupEndpoints.exportTransfer
																	);
																}}
																style={{
																	whiteSpace: "nowrap",
																	minWidth: "100px",
																	marginLeft: "10px",
																	textTransform: "none",
																}}>
																Duyệt hợp đồng
															</Button>
														)}
														{contract?.transferContract?.isExported === false && (
															<Button
																className="addDesktop"
																variant="outlined"
																size="small"
																startIcon={<EditIcon />}
																onClick={() => {
																	onClickUpdateTranferContract();
																}}
																style={{
																	whiteSpace: "nowrap",
																	minWidth: "100px",
																	marginLeft: "10px",
																	textTransform: "none",
																}}>
																Cập nhật hợp đồng
															</Button>
														)}

														{!contract.receiveContract &&

															<div
																style={{
																	display: "flex",
																	width: "100%",
																	justifyContent: "space-evenly",
																}}>
																{Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) &&
																	contract?.transferContract && contract?.transferContract?.isExported &&
																	!contract?.transferContract?.customerSignature && (
																		<SignContractComponent
																			Id={contract?.id}
																			userId={auth?.userProfile?.id}
																			reloadPage={reloadPage}
																			handleOpenNotify={handleOpenNotify}
																			role={0}
																		/>
																	)}
																{Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) &&
																	contract?.transferContract && contract?.transferContract?.isExported &&
																	!contract?.transferContract?.staffSignature && (
																		<SignContractComponent
																			Id={contract?.id}
																			userId={auth?.userProfile?.id}
																			reloadPage={reloadPage}
																			handleOpenNotify={handleOpenNotify}
																			role={1}
																		/>
																	)}
															</div>

														}
													</div>
												)}
										</Accordion>
									)}

									{/*Hợp đồng nhận xe*/}
									{contract?.transferContract?.isExported && (
										<Accordion
											defaultExpanded={defaultExpandedContract.HopDongNhanXe}>
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												onClick={() => {
													setDefaultExpandedCar({
														...defaultExpandedContract, HopDongNhanXe: !defaultExpandedContract.HopDongNhanXe
													});
												}}
												id="panel1a-header">
												<Typography
													style={{ display: "flex", alignItems: "center" }}
													className="title">
													Hợp đồng nhận xe
												</Typography>

												{Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) && contract?.receiveContract && (
													<IconButton
														aria-label="Preview"
														color="primary"
														onClick={(e) => {
															onClickShowWordContractPath(
																contract?.receiveContract?.filePath
															);
														}}>
														<PreviewIcon />
													</IconButton>
												)}
											</AccordionSummary>
											<AccordionDetails>
												<Typography className="flex w-[100%] flex-wrap">

													<div className="flex w-[100%] flex-wrap md:flex-nowrap">

														<div className="w-[100%] md:w-[33.33%]">
															<h3 className="font-bold uppercase text-center">
																Thông tin nhận xe
															</h3>
															<p className="text__p">
																Ngày nhận xe:
																<span> {contract?.receiveContract?.dateReceive?.toString().slice(0, 10)} </span>
															</p>
															<p className="text__p">
																Số trên đồng hồ khi nhận:
																<span> {contract?.receiveContract?.currentCarState?.speedometerNumber} </span>
															</p>
															<p className="text__p">
																Mức nhiên liệu khi nhận:
																<span> {!contract?.receiveContract ? "" : contract?.receiveContract?.currentCarState?.fuelPercent + " %"} </span>
															</p>
															<p className="text__p">
																Phí cao tốc ETC khi nhận:
																<span> {!contract?.receiveContract ? "" : contract?.receiveContract?.currentCarState?.currentEtcAmount.toLocaleString() + " VND"} </span>
															</p>
															<p className="text__p">
																Mô tả hiện trạng:
																<span> {contract?.receiveContract?.currentCarState?.carStatusDescription} </span>
															</p>
														</div>

														<div className="w-[100%] md:w-[33.33%]">
															<h3 className="font-bold uppercase text-center">
																Thông tin tài sản nhận xe
															</h3>
															<p className="text__p">
																Tình trạng giấy tờ:
																<span> {contract?.receiveContract?.receiveItem?.paper} </span>
															</p>
															<p className="text__p">
																Thiết bị dự phòng:
																<span> {contract?.receiveContract?.receiveItem?.standbyDevice} </span>
															</p>
															<p className="text__p">
																Thông tin thiết bị:
																<span> {contract?.receiveContract?.receiveItem?.standbyDeviceInfo} </span>
															</p>
														</div>

														<div className="w-[100%] md:w-[33.33%]">
															<h3 className="font-bold uppercase text-center">
																Thông tin nhân viên nhận xe
															</h3>
															<p className="text__p">
																Họ tên:
																<span> {contract?.receiver?.name}</span>
															</p>
															<p className="text__p">
																Số điện thoại:
																<span> {contract?.receiver?.tel} </span>
															</p>
															<p className="text__p">
																Email:
																<span> {contract?.receiver?.email} </span>
															</p>
															<p className="text__p">
																Trạng thái hợp đồng:
																<span> {contract?.receiveContract ? contract?.receiveContract?.isExported ? "Đã duyệt" : "Chưa duyệt" : ""} </span>
															</p>
															<p className="text__p">
																Chữ kí điện tử:
																<span>
																	{contract?.receiveContract?.customerSignature && "Khách hàng đã kí"}
																	{contract?.receiveContract?.customerSignature && contract?.receiveContract?.staffSignature && ", "}
																	{contract?.receiveContract?.staffSignature && "nhân viên đã kí"}
																</span>
															</p>
														</div>

													</div>

													<div className="flex w-[100%] flex-wrap md:flex-nowrap gap-x-3 mt-3">
														<div className="w-full lg:w-[50%]">
															<h3 className="font-bold uppercase text-center">
																Hình ảnh nội thất
															</h3>
															<div style={{ height: "200px", overflowX: "auto", display: "flex", gap: "5px" }}>
																{
																	contract?.receiveContract?.insideCarImage.map((image, index) => {
																		return <>
																			{<img key={index} src={`${process.env.REACT_APP_API}/Uploads/${contract?.id?.replaceAll("-", "")}/${image}`} style={{ width: "100%" }} alt="inside" />}
																		</>
																	})
																}
															</div>
														</div>
														<div className="w-full lg:w-[50%]">
															<h3 className="font-bold uppercase text-center">
																Hình ảnh ngoại thất
															</h3>
															<div style={{ height: "200px", overflowX: "auto", display: "flex", gap: "5px" }}>
																{
																	contract?.receiveContract?.outSideCarImage.map((image, index) => {
																		return <>
																			{<img key={index} src={`${process.env.REACT_APP_API}/Uploads/${contract?.id?.replaceAll("-", "")}/${image}`} style={{ width: "100%" }} alt="outside" />}
																		</>
																	})
																}
															</div>
														</div>
													</div>

												</Typography>
											</AccordionDetails>

											{(Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["Admin"]) ||
												Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["OperatorStaff"])) && (
													<div className="text-center mb-[16px]">
														{!contract?.receiveContract && (
															<Button
																className="addDesktop"
																variant="outlined"
																size="small"
																startIcon={<AddIcon />}
																onClick={() => {
																	onClickCreateReceive();
																}}
																style={{
																	whiteSpace: "nowrap",
																	minWidth: "100px",
																	marginLeft: "10px",
																	textTransform: "none",
																}}>
																Tạo hợp đồng
															</Button>
														)}

														{contract?.receiveContract?.isExported === false && (
															<Button
																className="addDesktop"
																variant="outlined"
																size="small"
																startIcon={<CheckIcon />}
																onClick={() => {
																	onClickFinishContract(
																		contractGroupEndpoints.exportReceive
																	);
																}}
																style={{
																	whiteSpace: "nowrap",
																	minWidth: "100px",
																	marginLeft: "10px",
																	textTransform: "none",
																}}>
																Duyệt hợp đồng
															</Button>
														)}

														{contract?.receiveContract?.isExported === false && (
															<Button
																className="addDesktop"
																variant="outlined"
																size="small"
																startIcon={<BorderColorIcon />}
																onClick={() => {
																	onClickUpdateReceiveContract();
																}}
																style={{
																	whiteSpace: "nowrap",
																	minWidth: "100px",
																	marginLeft: "10px",
																	textTransform: "none",
																}}>
																Cập nhật hợp đồng
															</Button>
														)}

														<div
															style={{
																display: "flex",
																width: "100%",
																justifyContent: "space-evenly",
															}}>
															{Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) &&
																contract?.receiveContract && contract?.receiveContract?.isExported &&
																!contract?.receiveContract?.customerSignature && (
																	<SignContractComponent
																		Id={contract?.id}
																		userId={auth?.userProfile?.id}
																		reloadPage={reloadPage}
																		handleOpenNotify={handleOpenNotify}
																		role={0}
																	/>
																)}
															{Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) &&
																contract?.receiveContract && contract?.receiveContract?.isExported &&
																!contract?.receiveContract?.staffSignature && (
																	<SignContractComponent
																		Id={contract?.id}
																		userId={auth?.userProfile?.id}
																		reloadPage={reloadPage}
																		handleOpenNotify={handleOpenNotify}
																		role={1}
																	/>
																)}
														</div>
													</div>
												)}
											{/* <Button
												className=""
												onClick={() => {}}
												variant="contained">
												Hoàn tất
											</Button> */}
										</Accordion>
									)}


								</div>
							</div>
						</div>

						<WordContractPath
							filePath={filePathWord}
							stateProps={isShowWordContractPath}
							close={onClickCloseShowWordContractPath}
						/>

						<UpdateTransferContract
							id={id}
							stateProps={isUpdateTranferContract}
							close={onClickCloseUpdateTranferContract}
							reloadPage={reloadPage}
							contract={contract}
						/>

						<UpdateReceiveContract
							id={id}
							stateProps={isUpdateReceiveContract}
							close={onClickCloseUpdateReceiveContract}
							reloadPage={reloadPage}
							contract={contract}
						/>

						<UpdateRentContract
							id={id}
							stateProps={isUpdateRentContract}
							close={onClickCloseUpdateRentContract}
							reloadPage={reloadPage}
							contract={contract}
						/>

						<UpdateExpertiseInfo
							id={id}
							stateProps={isUpdateExpertiseInfo}
							close={onClickCloseUpdateExpertiseInfo}
							reloadPage={reloadPage}
							contract={contract}
						/>

						<CreateExpertiseContract
							id={id}
							stateProps={isCreateExpertise}
							close={onClickCloseExpertise}
							reloadPage={reloadPage}
							contract={contract}
						/>

						{/* <CreateRentContract
							id={id}
							contract={contract}
							stateProps={isCreateRent}
							close={onClickCloseRent}
							reloadPage={reloadPage}
						/> */}

						<CreateTransferContract
							id={id}
							stateProps={isCreateTransfer}
							close={onClickCloseTransfer}
							reloadPage={reloadPage}
						/>

						<UpdateTransferContract
							id={contract?.id}
							stateProps={isUpdateTransfer}
							close={onClickCloseUpdateTransfer}
							reloadPage={reloadPage}
							contract={contract}
						/>

						<CreateReceiveContract
							id={id}
							stateProps={isCreateReceive}
							close={onClickCloseReceive}
							reloadPage={reloadPage}
						/>

						<CreateFile
							stateProps={isCreateFile}
							close={onClickCloseFile}
							reloadPage={reloadPage}
							idGroup={contract?.id}
						/>

						<UpdateFile
							stateProps={isUpdateFile}
							close={onClickCloseUpdateFile}
							reloadPage={reloadPage}
							contract={contract}
						/>

						<UpdateContract
							stateProps={isUpdateContract}
							close={onClickCloseUpdateContract}
							reloadPage={reloadPage}
							contract={contract}
						/>

						<Confirm
							close={onClickCloseModalDeleteContract}
							stateOpen={isShowModalDeleteContract}
							title={"Xác nhận xóa hợp đồng"}
							function={handleDeleteContract}
						/>

						<Snackbar
							anchorOrigin={{ vertical: "top", horizontal: "right" }}
							key={"top right"}
							open={openNotify}
							autoHideDuration={3000}
							onClose={handleCloseNotify}>
							{typeNotifi === "success" ? (
								<Alert
									color={"info"}
									onClose={handleCloseNotify}
									severity={"success"}
									sx={{ width: "100%" }}>
									{messageNotify}
								</Alert>
							) : (
								<Alert
									color={"error"}
									onClose={handleCloseNotify}
									severity={"error"}
									sx={{ width: "100%" }}>
									{messageNotify}
								</Alert>
							)}
						</Snackbar>
					</>
				) : (
					<Loading />
				)}
			</div>
		</>
	);
};