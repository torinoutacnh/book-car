import { Alert, Box, Button, FormControl, FormControlLabel, FormLabel, InputLabel, MenuItem, Modal, Radio, RadioGroup, Select, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext } from "react";
import "./styles/crud.scss"
import TextField from '@mui/material/TextField';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { CarClassType } from "interface/car";
import { ContractGroupReq, ExpertiseInfo } from "interface/request/contractGroupReq";
import { useSearchParams } from "react-router-dom";
import { Contract } from "interface/contract";
import { useForm } from "react-hook-form";
import BorderColorIcon from '@mui/icons-material/BorderColor';
import { contractGroupEndpoints } from "routers/apiEndpoints";
import { useQueryClient } from "react-query";
import { ConfigContext } from "provider";

export function UpdateContract(props: { stateProps: boolean, close: any, reloadPage: any, contract: Contract }) {

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            },
        },
    };

    const [isShow, setIsShow] = useState(false);
    const { configCar } = useContext(ConfigContext);

    useEffect(() => {
        setIsShow(props.stateProps)
    }, [props.stateProps, props.reloadPage])

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }

    const [rentFrom, setRentFrom] = useState<Moment | null>(moment(props?.contract?.rentFrom));
    const [rentTo, setRentTo] = useState<Moment | null>(moment(props?.contract?.rentTo));

    const handleChangeRentFrom = (newValue: Moment | null) => {
        setRentFrom(newValue);
    }

    const handleChangeRentTo = (newValue: Moment | null) => {
        setRentTo(newValue);
    }

    const [searchParams, setSearchParams] = useSearchParams();
    const id = searchParams.get("idContract");

    const [enableExpertiseInfo, setEnableExpertiseInfo] = useState<boolean>();

    const { register, handleSubmit, formState: { errors }, reset } = useForm<ContractGroupReq>();

    const years: number[] = Array.from(
        { length: (2000 - moment().year()) / -1 + 1 },
        (_, i) => moment().year() + i * -1
    );

    const { auth } = useContext(AuthContext);

    const resetFields = () => {
        reset(formValues => ({
            ...formValues,
            rentFrom: props?.contract?.rentFrom.toString(),
            rentTo: props?.contract?.rentTo.toString(),
            carClass: props?.contract?.carClass,
            requireDescriptionInfo: {
                carBrand: props?.contract?.requireDescriptionInfo?.carBrand,
                seatNumber: props?.contract?.requireDescriptionInfo?.seatNumber,
                yearCreate: props?.contract?.requireDescriptionInfo?.yearCreate,
                carColor: props?.contract?.requireDescriptionInfo?.carColor
            },
            rentPurpose: props?.contract?.rentPurpose,
            expertiseInfo: {
                isFirstTimeRent: props?.contract?.expertiseInfo?.isFirstTimeRent,
                status: props?.contract?.expertiseInfo?.status
            },
            customerSocialInfo: {
                facebook: props?.contract?.customerSocialInfo?.facebook,
                zalo: props?.contract?.customerSocialInfo?.zalo,
                linkedin: props?.contract?.customerSocialInfo?.linkedin,
                other: props?.contract?.customerSocialInfo?.other
            },
            companyInfo: props?.contract?.companyInfo,
            relativeTel: props?.contract?.relativeTel,
            addtionalInfo: props?.contract?.addtionalInfo
        }))
    }

    const queryClient = useQueryClient()
    const submitForm = async (data: ContractGroupReq) => {
        if (!auth) return;
        data.rentFrom = rentFrom?.format("YYYY-MM-DD");
        data.rentTo = rentTo?.format("YYYY-MM-DD");
        data.carClass = Number(data.carClass)
        data.requireDescriptionInfo.seatNumber = Number(data.requireDescriptionInfo?.seatNumber)
        data.requireDescriptionInfo.yearCreate = Number(data.requireDescriptionInfo?.yearCreate)
        if (data.expertiseInfo.isFirstTimeRent) {
            data.expertiseInfo.isFirstTimeRent = true
            data.expertiseInfo.status = 0
        }
        else {
            data.expertiseInfo.isFirstTimeRent = false
            data.expertiseInfo.status = Number(data.expertiseInfo.status)
        }


        const res = await fetch(process.env.REACT_APP_API + contractGroupEndpoints.updateContract + props?.contract?.id, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(data)
        })
        const data_res = await res.json()

        if (res.status > 200) {
            // console.log(`update contract status ${res.status} => `, data_res)
            handleOpenNotify(data_res.message, "error")
            return
        }
        // console.log(`update user info status ${res.status} => `, data_res)
        queryClient.invalidateQueries(["getCarByGroup"])
        handleOpenNotify("Cập nhật thông tin thuê xe thành công!", "success")
        props.reloadPage();
        props.close();
    }

    useEffect(() => {
        props?.contract?.expertiseInfo?.isFirstTimeRent ? setEnableExpertiseInfo(true) : setEnableExpertiseInfo(false)
    }, [])

    return (
        <>

            {isShow ?
                <Modal
                    open={isShow}
                    onClose={() => { props.close(); resetFields() }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Cập nhật thông tin thuê xe
                        </Typography>

                        <form id="formUpdateContract" className="container_create" onSubmit={handleSubmit(data => { submitForm(data) })}>

                            <div className="info">
                                <div className="grid grid-cols-1 md:grid-cols-2 gap-x-5">

                                    <div className="">

                                        <DesktopDatePicker
                                            {...register("rentFrom")}
                                            label="Thuê từ ngày"
                                            inputFormat="DD/MM/YYYY"
                                            className="w-full"
                                            value={rentFrom}
                                            onChange={handleChangeRentFrom}
                                            renderInput={(params) =>
                                                <TextField
                                                    {...params}
                                                    size='small'
                                                    sx={{ my: 1 }}
                                                />}
                                        />

                                        <DesktopDatePicker
                                            {...register("rentTo")}
                                            label="Đến ngày"
                                            className="w-full"
                                            inputFormat="DD/MM/YYYY"
                                            value={rentTo}
                                            onChange={handleChangeRentTo}
                                            renderInput={(params) =>
                                                <TextField
                                                    {...params}
                                                    size='small'
                                                    sx={{ my: 1 }}
                                                />}
                                        />

                                        <TextField
                                            sx={{ my: 1 }}
                                            id="outlined-basic"
                                            label="Mục đích thuê"
                                            variant="outlined"
                                            size="small"
                                            required
                                            className="w-full"
                                            defaultValue={props?.contract?.rentPurpose}
                                            {...register("rentPurpose")}
                                        />

                                        <FormControl
                                            required
                                            sx={{ my: 1 }}
                                            className="w-full"
                                        >
                                            <InputLabel>Hãng xe</InputLabel>
                                            <Select
                                                size="small"
                                                label={"Hãng xe"}
                                                required
                                                MenuProps={MenuProps}
                                                defaultValue={props?.contract?.requireDescriptionInfo?.carBrand}
                                                {...register("requireDescriptionInfo.carBrand")}
                                            >
                                                {configCar?.carBrand?.sort().map((value) => (
                                                    <MenuItem
                                                        aria-selected="true"
                                                        key={value}
                                                        value={value}
                                                    >
                                                        {value}
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                        </FormControl>

                                        <FormControl
                                            required
                                            sx={{ my: 1 }}
                                            className="w-full"
                                        >
                                            <InputLabel>Phân khúc xe</InputLabel>
                                            <Select
                                                defaultValue={props?.contract?.carClass}
                                                size="small"
                                                label={"Phân khúc xe"}
                                                required
                                                className="w-full"
                                                {...register("carClass")}
                                            >
                                                {Object.keys(CarClassType)
                                                    .filter((value) => isNaN(value as any))
                                                    .map((value) => (
                                                        <MenuItem
                                                            key={value}
                                                            value={CarClassType[value as any]}
                                                        >
                                                            {value}
                                                        </MenuItem>
                                                    ))}
                                            </Select>
                                        </FormControl>

                                        <FormControl
                                            required
                                            sx={{ my: 1 }}
                                            className="w-full"
                                        >
                                            <InputLabel>Số chỗ</InputLabel>
                                            <Select
                                                size="small"
                                                label={"Số chỗ"}
                                                MenuProps={MenuProps}
                                                defaultValue={props?.contract?.requireDescriptionInfo?.seatNumber}
                                                required
                                                {...register("requireDescriptionInfo.seatNumber")}
                                            >
                                                {configCar?.carSeat.map((value) => (
                                                    <MenuItem
                                                        aria-selected="true"
                                                        key={value}
                                                        value={value}
                                                    >
                                                        {value} chỗ
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                        </FormControl>

                                        <FormControl
                                            required
                                            sx={{ my: 1 }}
                                            className="w-full"
                                        >
                                            <InputLabel>Đời xe</InputLabel>
                                            <Select
                                                size="small"
                                                defaultValue={props?.contract?.requireDescriptionInfo?.yearCreate}
                                                label={"Đời xe"}
                                                required
                                                MenuProps={MenuProps}
                                                className="w-full"
                                                {...register("requireDescriptionInfo.yearCreate")}
                                            >
                                                {
                                                    years.map((item, index) => {
                                                        return (
                                                            <MenuItem key={index} value={item}>{item}</MenuItem>
                                                        )
                                                    })
                                                }
                                            </Select>
                                        </FormControl>

                                        <FormControl required className="w-full" sx={{ my: "8px" }}>
                                            <InputLabel>Màu xe</InputLabel>
                                            <Select
                                                MenuProps={MenuProps}
                                                defaultValue={props?.contract?.requireDescriptionInfo?.carColor}
                                                label={"Màu xe"}
                                                size="small"
                                                {...register("requireDescriptionInfo.carColor")}
                                            >
                                                {configCar?.carColor?.sort().map((value) => (
                                                    <MenuItem
                                                        aria-selected="true"
                                                        key={value}
                                                        value={value}
                                                    >
                                                        Màu {value}
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                        </FormControl>

                                    </div>

                                    <div className="">

                                        <FormControl className="w-full" required>
                                            <FormLabel>Đã từng thuê xe</FormLabel>
                                            <RadioGroup
                                                {...register("expertiseInfo.isFirstTimeRent")}
                                                row
                                                onChange={(e) =>
                                                    setEnableExpertiseInfo(e.target.value === "true" ? true : false)
                                                }
                                                defaultValue={props?.contract?.expertiseInfo?.isFirstTimeRent ? true : false}
                                            >
                                                <FormControlLabel
                                                    value={true}
                                                    control={<Radio />}
                                                    label="Thuê lần đầu"
                                                />
                                                <FormControlLabel
                                                    value={false}
                                                    control={<Radio />}
                                                    label="Đã từng thuê"
                                                />
                                            </RadioGroup>
                                            <FormControl disabled={enableExpertiseInfo}>
                                                <InputLabel>Thời gian thẩm định gần nhất</InputLabel>
                                                <Select
                                                    className="my-[3px]"
                                                    size="small"
                                                    label={"Thời gian thẩm định gần nhất"}
                                                    required
                                                    defaultValue={props?.contract?.expertiseInfo?.status}
                                                    {...register("expertiseInfo.status")}
                                                >
                                                    {Object.keys(ExpertiseInfo)
                                                        .filter((value) => isNaN(value as any))
                                                        .map((value) => (
                                                            <MenuItem
                                                                aria-selected="true"
                                                                key={value}
                                                                value={ExpertiseInfo[value as any]}
                                                            >
                                                                {value}
                                                            </MenuItem>
                                                        ))}
                                                </Select>
                                            </FormControl>
                                        </FormControl>

                                        <TextField
                                            sx={{ my: 1 }}
                                            id="outlined-basic"
                                            label="Facebook"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            defaultValue={props?.contract?.customerSocialInfo?.facebook}
                                            {...register("customerSocialInfo.facebook")}
                                        />

                                        <TextField
                                            sx={{ my: 1 }}
                                            id="outlined-basic"
                                            label="Zalo"
                                            defaultValue={props?.contract?.customerSocialInfo?.zalo}
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            {...register("customerSocialInfo.zalo")}
                                        />

                                        <TextField
                                            sx={{ my: 1 }}
                                            id="outlined-basic"
                                            label="Linkedin"
                                            variant="outlined"
                                            size="small"
                                            defaultValue={props?.contract?.customerSocialInfo?.linkedin}
                                            className="w-full"
                                            {...register("customerSocialInfo.linkedin")}
                                        />
                                        <TextField
                                            sx={{ my: 1 }}
                                            id="outlined-basic"
                                            defaultValue={props?.contract?.customerSocialInfo?.other}
                                            label="Mạng xã hội khác"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            {...register("customerSocialInfo.other")}
                                        />

                                        <TextField
                                            sx={{ my: 1 }}
                                            id="outlined-basic"
                                            label="Thông tin công ty"
                                            variant="outlined"
                                            defaultValue={props?.contract?.companyInfo}
                                            size="small"
                                            className="w-full"
                                            {...register("companyInfo")}
                                        />

                                        <TextField
                                            sx={{ my: 1 }}
                                            id="outlined-basic"
                                            label="Số điện thoại"
                                            defaultValue={props?.contract?.relativeTel}
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            {...register("relativeTel")}
                                        />

                                        <TextField
                                            sx={{ my: 1 }}
                                            id="outlined-basic"
                                            label="Thông tin bổ sung"
                                            defaultValue={props?.contract?.addtionalInfo}
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            {...register("addtionalInfo")}
                                        />

                                    </div>

                                </div>
                            </div>

                        </form>

                        <div className="action">

                            <Button
                                size='small'
                                variant="contained"
                                startIcon={<BorderColorIcon />}
                                className="btnCreate"
                                type="submit"
                                form="formUpdateContract"
                            >
                                Cập nhật
                            </Button>
                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className='btnCancel'
                                onClick={() => { props.close() }}
                            >
                                Hủy bỏ
                            </Button>
                        </div>

                    </Box>
                </Modal>
                : <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}