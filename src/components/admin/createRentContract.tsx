import { Alert, Box, Button, FormControl, InputLabel, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material"
// import { useState, useEffect, useContext } from "react";
// import "./styles/crud.scss"
// import TextField from '@mui/material/TextField';
// import AddIcon from '@mui/icons-material/Add';
// import CloseIcon from '@mui/icons-material/Close';
// import { DesktopDatePicker } from "@mui/x-date-pickers";
// import moment, { Moment } from "moment";
// import AuthContext from "context/AuthContext";
// import { RoleUser, User } from "interface/authenticate";
// import { Contract } from "interface/contract";
// import { userEndpoints } from "routers/apiEndpoints";

// export function CreateRentContract(props: { id: string, stateProps: boolean, close: any, reloadPage: any, contract: Contract }) {

//     const style = {
//         position: 'absolute' as 'absolute',
//         top: '50%',
//         left: '50%',
//         transform: 'translate(-50%, -50%)',
//         bgcolor: 'background.paper',
//         border: '1px solid #000',
//         boxShadow: 24,
//         p: 4,
//         borderRadius: '10px',
//         textAlign: 'center',
//     };

//     const ITEM_HEIGHT = 48;
//     const ITEM_PADDING_TOP = 8;
//     const MenuProps = {
//         PaperProps: {
//             style: {
//                 maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
//                 width: 220,
//             },
//         },
//     };

//     const [isShow, setIsShow] = useState(false);

//     useEffect(() => {
//         setIsShow(props.stateProps)
//     }, [props.stateProps, props.reloadPage])

//     const [typeNotifi, setTypeNotifi] = useState("success")
//     const [openNotify, setOpenNofity] = useState(false);
//     const [messageNotify, setMessageNotify] = useState("")

//     const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
//         if (reason === 'clickaway') {
//             return;
//         }
//         setOpenNofity(false);
//     };

//     const handleOpenNotify = (message: string, type: string) => {
//         setTypeNotifi(type)
//         setMessageNotify(message)
//         setOpenNofity(true)
//     }

//     const [pickupTime, setPickupTime] = useState<Moment | null>(moment(props.contract.rentFrom));
//     const [returnTime, setReturnTime] = useState<Moment | null>(moment(props.contract.rentTo));

//     const handleChangePickupTime = (newValue: Moment | null) => {
//         setPickupTime(newValue);
//     };
//     const handleChangeReturnTime = (newValue: Moment | null) => {
//         setReturnTime(newValue);
//     };

//     const [listUser, setListUser] = useState<User[]>();

//     useEffect(() => {
//         if (!auth) return;
//         fetch(process.env.REACT_APP_API + userEndpoints.base, {
//             method: "GET",
//             headers: {
//                 "Content-Type": "application/json",
//                 Authorization: "Bearer " + auth.accessToken,
//             },
//         })
//             .then((res) => {
//                 if (res.status >= 500) throw new Error("Lỗi hệ thống!");
//                 if (res.status >= 400) {
//                     // console.log(res.status);
//                 }
//                 return res.json();
//             })
//             .then((data) => setListUser(data.data))
//             .catch((err) => {

//                 // console.log(err)
//             }
//             );
//     }, []);

//     const initValue = {
//         contractGroupId: props.id,
//         representativeId: '',
//         maxKmPerDay: 0,
//         feeOverPerKm: 0,
//         carTransferFee: 0,
//         otherFee: 0,
//         promotionPercent: 0,
//         promotionAmount: 0,
//         vat: 0,
//         downPayment: 0
//     }

//     const [state, setState] = useState(initValue)

//     const { auth } = useContext(AuthContext);

//     const handleSubmit = async () => {
//         if (!auth) return;

//         const dataBody = {
//             contractGroupId: state.contractGroupId,
//             representativeId: state.representativeId,
//             rentFrom: pickupTime,
//             rentTo: returnTime,
//             maxKmPerDay: state.maxKmPerDay,
//             feeOverPerKm: state.feeOverPerKm,
//             carTransferFee: state.carTransferFee,
//             otherFee: state.otherFee,
//             promotionPercent: state.promotionPercent,
//             promotionAmount: state.promotionAmount,
//             vat: state.vat,
//             downPayment: state.downPayment
//         }

//         const checkNull = { a: false }

//         Object.values(dataBody).forEach((value, index) => {
//             const tmp = value as string
//             if (tmp.length === 0) {
//                 checkNull.a = true
//             }
//         })

//         if (checkNull.a) {
//             handleOpenNotify('Vui lòng nhập đầy đủ thông tin!', 'error')
//             return
//         }

//         const res = await fetch(process.env.REACT_APP_API + "/contractgroup/create-rent-contract", {
//             method: "POST",
//             headers: {
//                 "Content-Type": "application/json",
//                 Authorization: "Bearer " + auth.accessToken,
//             },
//             body: JSON.stringify(dataBody),
//         })

//         const data = await res.json()

//         if (res.status > 200) {
//             handleOpenNotify(data.message, "error")
//             // console.log("Rent-contract status ", res.status, data);
//             return
//         }

//         // console.log("create rent success => ", data)
//         handleOpenNotify("Tạo hợp đồng thuê thành công!", "success")
//         setState(initValue);
//         props.reloadPage();
//         props.close();
//     }

//     return (
//         <>
//             {isShow ?
//                 <Modal
//                     open={true}
//                     onClose={() => props.close()}
//                     aria-labelledby="modal-modal-title"
//                     aria-describedby="modal-modal-description"
//                 >
//                     <Box sx={style} style={{ color: "black" }}>
//                         <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
//                             Tạo thông tin hợp đồng thuê
//                         </Typography>

//                         <form className="container_create max-h-[500px] overflow-y-auto form_scroll pr-3">

//                             <div className="info w-full">

//                                 <FormControl sx={{ mt: 2, mb: 1 }} size="small" required>
//                                     <InputLabel id="demo-select-small">Nhân viên phụ trách</InputLabel>
//                                     <Select
//                                         labelId="demo-select-small"
//                                         id="demo-select-small"
//                                         label="Nhân viên phụ trách"
//                                         required
//                                         MenuProps={MenuProps}
//                                     >
//                                         {listUser?.filter(item => Number(RoleUser[item.role]) !== Number(RoleUser["Customer"])).map((item, index) => (
//                                             <MenuItem
//                                                 value={item.id}
//                                                 key={index}
//                                                 style={{ overflow: 'hidden' }}
//                                                 onClick={() => setState({ ...state, representativeId: item.id })}
//                                             >
//                                                 <Typography noWrap>
//                                                     {item.name} - {item.tel}
//                                                 </Typography>
//                                             </MenuItem>
//                                         ))}
//                                     </Select>
//                                 </FormControl>

//                                 <DesktopDatePicker
//                                     label="Thuê từ ngày"
//                                     inputFormat="DD/MM/YYYY"
//                                     value={pickupTime}
//                                     onChange={handleChangePickupTime}
//                                     renderInput={(params) =>
//                                         <TextField
//                                             {...params}
//                                             size='small'
//                                             sx={{ my: 1 }}
//                                         />}
//                                 />

//                                 <DesktopDatePicker
//                                     label="Đến ngày"
//                                     inputFormat="DD/MM/YYYY"
//                                     value={returnTime}
//                                     onChange={handleChangeReturnTime}
//                                     renderInput={(params) =>
//                                         <TextField
//                                             {...params}
//                                             size='small'
//                                             sx={{ my: 1 }}
//                                         />}
//                                 />

//                                 <TextField
//                                     sx={{ my: 1 }}
//                                     id="outlined-basic"
//                                     label="Km tối đa trên ngày (km)"
//                                     variant="outlined"
//                                     size="small"
//                                     required
//                                     type='number'
//                                     onChange={(e) => setState({
//                                         ...state,
//                                         maxKmPerDay: Number(e.target.value)
//                                     })}
//                                 />

//                                 <TextField
//                                     sx={{ my: 1 }}
//                                     id="outlined-basic"
//                                     label="Mức phí trên mỗi km (VND)"
//                                     variant="outlined"
//                                     size="small"
//                                     required
//                                     type='number'
//                                     onChange={(e) => setState({
//                                         ...state,
//                                         feeOverPerKm: Number(e.target.value)
//                                     })}
//                                 />

//                                 <TextField
//                                     sx={{ my: 1 }}
//                                     id="outlined-basic"
//                                     label="Phí vận chuyển (VND)"
//                                     variant="outlined"
//                                     size="small"
//                                     type='number'
//                                     required
//                                     onChange={(e) => setState({
//                                         ...state,
//                                         carTransferFee: Number(e.target.value)
//                                     })}
//                                 />

//                                 <TextField
//                                     sx={{ my: 1 }}
//                                     id="outlined-basic"
//                                     label="Chi phí khác (VND)"
//                                     variant="outlined"
//                                     size="small"
//                                     type='number'
//                                     onChange={(e) => setState({
//                                         ...state,
//                                         otherFee: Number(e.target.value)
//                                     })}
//                                 />

//                                 <TextField
//                                     sx={{ my: 1 }}
//                                     id="outlined-basic"
//                                     label="Khuyến mãi (%)"
//                                     variant="outlined"
//                                     size="small"
//                                     type='number'
//                                     onChange={(e) => setState({
//                                         ...state,
//                                         promotionPercent: Number(e.target.value)
//                                     })}
//                                 />

//                                 <TextField
//                                     sx={{ my: 1 }}
//                                     id="outlined-basic"
//                                     label="Số tiền khuyến mãi (VND)"
//                                     variant="outlined"
//                                     size="small"
//                                     type='number'
//                                     onChange={(e) => setState({
//                                         ...state,
//                                         promotionAmount: Number(e.target.value)
//                                     })}
//                                 />

//                                 <TextField
//                                     sx={{ my: 1 }}
//                                     id="outlined-basic"
//                                     label="Thuế GTGT (%)"
//                                     variant="outlined"
//                                     size="small"
//                                     type='number'
//                                     required
//                                     onChange={(e) => setState({
//                                         ...state,
//                                         vat: Number(e.target.value)
//                                     })}
//                                 />

//                                 <TextField
//                                     sx={{ my: 1 }}
//                                     id="outlined-basic"
//                                     label="Tiền đặt cọc (VND)"
//                                     variant="outlined"
//                                     size="small"
//                                     type='number'
//                                     required
//                                     onChange={(e) => setState({
//                                         ...state,
//                                         downPayment: Number(e.target.value)
//                                     })}
//                                 />

//                             </div>

//                         </form>

//                         <div className="action">

//                             <Button
//                                 size='small'
//                                 variant="contained"
//                                 startIcon={<AddIcon />}
//                                 className="btnCreate"
//                                 type="submit"
//                                 onClick={handleSubmit}
//                             >
//                                 Thêm mới
//                             </Button>

//                             <Button
//                                 size='small'
//                                 color='success'
//                                 variant="contained"
//                                 startIcon={<CloseIcon />}
//                                 className='btnCancel'
//                                 onClick={() => { props.close() }}
//                             >
//                                 Hủy bỏ
//                             </Button>

//                         </div>

//                     </Box>
//                 </Modal>
//                 :
//                 <></>
//             }

//             <Snackbar
//                 anchorOrigin={{ vertical: "top", horizontal: "right" }}
//                 key={"top right"}
//                 open={openNotify}
//                 autoHideDuration={3000}
//                 onClose={handleCloseNotify}
//             >
//                 {
//                     typeNotifi === "success"
//                         ?
//                         <Alert
//                             color={"info"}
//                             onClose={handleCloseNotify}
//                             severity={"success"}
//                             sx={{ width: '100%' }}
//                         >
//                             {messageNotify}
//                         </Alert>
//                         :
//                         <Alert
//                             color={"error"}
//                             onClose={handleCloseNotify}
//                             severity={"error"}
//                             sx={{ width: '100%' }}
//                         >
//                             {messageNotify}
//                         </Alert>
//                 }
//             </Snackbar>

//         </>
//     )
// }