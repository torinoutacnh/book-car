import { Alert, Box, Button, Modal, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext } from "react";
import "./styles/crud.scss"
import TextField from '@mui/material/TextField';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { Contract } from "interface/contract";
import { useMutation, useQuery } from "react-query";
import { contractGroupEndpoints } from "routers/apiEndpoints";
import { WordContractPreview } from "components/Word/WordContractPreview";
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import BorderColorIcon from '@mui/icons-material/BorderColor';

export function UpdateRentContract(props: { id: string, stateProps: boolean, close: any, reloadPage: any, contract: Contract }) {

    const initValue = {
        paymentAmount: props?.contract?.rentContract?.paymentAmount,
        description: props.contract?.rentContract?.depositItem?.description,
        asset: props.contract?.rentContract?.depositItem?.asset,
        downPayment: props.contract?.rentContract?.depositItem?.downPayment
    }

    const [isShow, setIsShow] = useState(false);
    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")
    const [state, setState] = useState(initValue)
    const { auth } = useContext(AuthContext);

    const [rentFrom, setRentFrom] = useState<Moment | null>(moment());
    const [rentTo, setRentTo] = useState<Moment | null>(moment());

    const handleChangeRentFrom = (newValue: Moment | null) => {
        setRentFrom(newValue);
    }
    const handleChangeRentTo = (newValue: Moment | null) => {
        setRentTo(newValue);
    }
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    const [isPreviewWord, setIsPreviewWord] = useState(false);

    const onClickWordPreview = () => {
        setIsPreviewWord(true)

    }
    const onClickCloseWordPreview = () => {
        setIsPreviewWord(false)
    }

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    useEffect(() => {
        setIsShow(props.stateProps)
        if (!props?.contract?.rentContract) { return }

        setState({
            paymentAmount: props?.contract?.rentContract?.paymentAmount,
            downPayment: props?.contract?.rentContract?.depositItem?.downPayment,
            description: props?.contract?.rentContract?.depositItem?.description,
            asset: props?.contract?.rentContract?.depositItem?.asset
        })

        setRentFrom(moment(props?.contract?.rentContract?.rentFrom));
        setRentTo(moment(props?.contract?.rentContract?.rentTo));

    }, [props.stateProps, props.reloadPage])


    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }


    const ApiUpdateRentContract = async () => {
        if (!auth) return;

        if (state.downPayment.toString().length === 0) {
            handleOpenNotify('Vui lòng nhập đầy đủ thông tin!', 'error')
            return Promise.reject("Vui lòng nhập đầy đủ thông tin")
        }

        const dataBody = {
            rentFrom: rentFrom.format("YYYY-MM-DD[T]00:00:00[Z]").slice(0, 20),
            rentTo: rentTo.format("YYYY-MM-DD[T]00:00:00[Z]").slice(0, 20),
            paymentAmount: state.paymentAmount,
            depositItem: {
                description: state.description,
                asset: state.asset,
                downPayment: state.downPayment
            }
        }

        return fetch(process.env.REACT_APP_API + contractGroupEndpoints.updateRent + props?.contract?.id, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(dataBody),
        })

    }

    const mutationUpdateRentContract = useMutation(ApiUpdateRentContract,
        {
            onSuccess: async (res, variable, context) => {
                const data = await res.json()
                if (res.status > 200) {
                    // console.log(`update deposit status ${res.status} => `, data)
                    handleOpenNotify(data.message, "error")
                }
                else {
                    // console.log("mutationCreateCar create car status 200", data)
                    handleOpenNotify("Cập nhật hợp đồng thuê xe thành công!", "success")
                    props.reloadPage()
                }
                props.close()
            },
            onError: (error, variable, context) => {
                // console.log("error", error)
            }
        })

    return (
        <>
            {isShow ?
                <Modal
                    open={true}
                    onClose={() => { props.close(); setState(initValue); }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Cập nhật hợp đồng thuê xe
                        </Typography>

                        <form className="container_create">

                            <div className="info w-full">

                                <DesktopDatePicker
                                    label="Thuê từ ngày"
                                    inputFormat="DD/MM/YYYY"
                                    value={rentFrom}
                                    onChange={handleChangeRentFrom}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ my: 1 }}
                                        />}
                                />

                                <DesktopDatePicker
                                    label="Thuê đến ngày"
                                    inputFormat="DD/MM/YYYY"
                                    value={rentTo}
                                    onChange={handleChangeRentTo}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ my: 1 }}
                                        />}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Tài sản đặt cọc"
                                    defaultValue={props.contract?.rentContract?.depositItem?.asset}
                                    variant="outlined"
                                    size="small"
                                    onChange={(e) => setState({
                                        ...state,
                                        asset: e.target.value.trim()
                                    })}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Mô tả đặt cọc"
                                    defaultValue={props.contract?.rentContract?.depositItem?.description}
                                    variant="outlined"
                                    size="small"
                                    onChange={(e) => setState({
                                        ...state,
                                        description: e.target.value.trim()
                                    })}
                                />

                                <TextField
                                    type="number"
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Số tiền đặt cọc (VND)"
                                    defaultValue={props.contract?.rentContract?.depositItem?.downPayment}
                                    variant="outlined"
                                    size="small"
                                    onChange={(e) => setState({
                                        ...state,
                                        downPayment: Number(e.target.value.trim())
                                    })}
                                />

                                <TextField
                                    type={"number"}
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Số tiền cần thanh toán (VND)"
                                    defaultValue={props.contract?.rentContract?.paymentAmount}
                                    variant="outlined"
                                    size="small"
                                    onChange={(e) => setState({
                                        ...state,
                                        paymentAmount: Number(e.target.value.trim())
                                    })}
                                />

                            </div>

                        </form>

                        <div className="action">

                            <Button
                                size='small'
                                variant="contained"
                                startIcon={<BorderColorIcon />}
                                className="btnCreate"
                                type="submit"
                                onClick={() => { mutationUpdateRentContract.mutate() }}
                            >
                                Cập nhật
                            </Button>
                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className='btnCancel'
                                onClick={() => { props.close(); setState(initValue); }}
                            >
                                Hủy bỏ
                            </Button>
                        </div>
                        <div className="action">
                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<RemoveRedEyeIcon />}
                                className='btnCancel'
                                onClick={() => { onClickWordPreview() }}
                                style={{ whiteSpace: "nowrap" }}
                            >
                                Xem trước
                            </Button>

                        </div>

                    </Box>
                </Modal>
                :
                <></>
            }

            {
                props?.contract?.rentContract && !props?.contract?.rentContract?.isExported &&
                <WordContractPreview
                    stateProps={isPreviewWord}
                    pathApi={contractGroupEndpoints.previewRent}
                    idGroup={props?.contract?.id}
                    close={onClickCloseWordPreview}
                    data={
                        {
                            rentFrom: rentFrom.format("YYYY-MM-DD[T]hh:mm:ss"),
                            rentTo: rentTo.format("YYYY-MM-DD[T]hh:mm:ss"),
                            downPayment: state.downPayment
                        }
                    }
                />
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}