import { FormControl, OutlinedInput, InputAdornment, IconButton, Button, Alert, Snackbar, MenuItem, Select, SelectChangeEvent, Box, InputLabel, Typography } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import "./styles/manager.scss";
import { CreateUser } from "./createUser";
import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import { RoleUser, User, VieRole } from "../../interface/authenticate";
import { useNavigate } from "react-router-dom";
import AuthContext from "context/AuthContext";
import CloseIcon from "@mui/icons-material/Close";
import { adminEndpoints } from "routers/endpoints";
import { Loading } from "components/loading/loading";
import { Confirm } from "components/layout/confirm";
import { userEndpoints } from "routers/apiEndpoints";
import { FilterNotFound } from "components/loading/contractNotFound";

export const ManagerUser = () => {

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.7 + ITEM_PADDING_TOP,
        width: 220,
      },
    },
  }

  const [typeNotifi, setTypeNotifi] = useState("success");
  const [openNotify, setOpenNofity] = useState(false);
  const [messageNotify, setMessageNotify] = useState("");
  const [listUser, setListUser] = useState<User[]>();
  const [listUserTmp, setListUserTmp] = useState<User[]>();

  const navigate = useNavigate();

  const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNofity(false);
  };

  const handleOpenNotify = (message: string, type: string) => {
    setTypeNotifi(type);
    setMessageNotify(message);
    setOpenNofity(true);
  };

  const [reRender, setReRender] = useState(0);
  const reloadPage = () => {
    setReRender(reRender + 1);
  };

  const [isShowModal, setIsShowModal] = useState(false);
  const onClickShowModal = () => setIsShowModal(true);
  const onClickCloseModal = () => setIsShowModal(false);

  const [idUserDelete, setIdUserDelete] = useState<string>()
  const [isShowModalDeleteUser, setIsShowModalDeleteUser] = useState(false);
  const onClickShowModalDeleteUser = (id: string, e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation()
    setIdUserDelete(id)
    setIsShowModalDeleteUser(true)
  };
  const onClickCloseModalDeleteUser = () => {
    setIdUserDelete(null)
    setIsShowModalDeleteUser(false);
  }

  const { auth } = useContext(AuthContext);

  useEffect(() => {
    if (!auth) return;
    fetch(process.env.REACT_APP_API + userEndpoints.base, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + auth.accessToken,
      },
    })
      .then((res) => {
        if (res.status >= 500) throw new Error("Lỗi hệ thống!");
        if (res.status >= 400) {
          console.log(res.status);
        }
        return res.json();
      })
      .then((data) => {
        setListUser(data.data);
        setListUserTmp(data.data);
      })
      .catch((err) => {
        // console.log(err)
      });
  }, [auth?.userProfile?.id, reRender]);

  const onClickDeleteUser = async (id: string) => {

    const res = await fetch(process.env.REACT_APP_API + userEndpoints.deleteUser + idUserDelete, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + auth.accessToken,
      },
    })
    const data = await res.json()

    if (res.status > 200) {
      handleOpenNotify(data.message, "error")
      // console.log("finish status ", res.status, " => ", data);
      return
    }
    reloadPage()
    handleOpenNotify("Xóa người dùng thành công", "success")
  };

  const onClickUserInfo = (id: string) => {
    navigate("/" + adminEndpoints.base + "/" + adminEndpoints.userInfo + "?" + new URLSearchParams({ idUser: id }))
  };

  const [filterContentSearch, setFilterContentSearch] = useState("");
  const handleSearch = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setFilterContentSearch(e.target.value);
    const content = e.target.value as string;
    const tmp: User[] = [];

    if (content.trim().length === 0) {
      setListUser(listUserTmp);
      return;
    } else {
      switch (filterSearch) {
        case "name": {
          listUserTmp.map((item) => item.name.toUpperCase().includes(filterContentSearch.toUpperCase()) && tmp.push(item));
          break;
        }

        case "tel": {
          listUserTmp.map((item) => item.tel.toUpperCase().includes(filterContentSearch.toUpperCase()) && tmp.push(item));
          break;
        }
        case "email": {
          listUserTmp.map((item) => item.email.toUpperCase().includes(filterContentSearch.toUpperCase()) && tmp.push(item));
          break;
        }
        case "role": {
          listUserTmp.map((item) => String(item.role).toUpperCase().includes(filterContentSearch.toUpperCase()) && tmp.push(item));
          break;
        }
      }
      setListUser(tmp);
    }
  };

  const [filterSearch, setFilterSearch] = useState("name");
  const handleChangeFilterSearch = (event: SelectChangeEvent) => {
    handleClearContentSearch();
    setFilterSearch(event.target.value as string);
  };

  const handleClearContentSearch = () => {
    setFilterContentSearch("");
    setListUser(listUserTmp);
  };

  return (
    <>
      {listUser ? (
        <>
          <div className="nav">
            <div style={{ display: "flex" }}>
              <Box>
                <FormControl>
                  <InputLabel id="demo-simple-select-label" size="small">
                    Tìm kiếm theo
                  </InputLabel>
                  <Select
                    sx={{ minWidth: 150, mr: 2 }}
                    size="small"
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={filterSearch}
                    label="Tìm kiếm theo"
                    onChange={handleChangeFilterSearch}
                    defaultValue={filterSearch}
                  >
                    <MenuItem key={0} value={"name"}>
                      Tên
                    </MenuItem>
                    <MenuItem key={1} value={"tel"}>
                      Số điện thoại
                    </MenuItem>
                    <MenuItem key={2} value={"email"}>
                      Email
                    </MenuItem>
                    <MenuItem key={3} value={"role"}>
                      Chức vụ
                    </MenuItem>
                  </Select>
                </FormControl>
              </Box>
              {filterSearch !== "role" ?
                <FormControl className="search" variant="outlined">
                  <OutlinedInput
                    type="text"
                    value={filterContentSearch}
                    size="small"
                    placeholder="Bạn đang tìm kiếm ..."
                    className="w-full"
                    onChange={(e) => {
                      handleSearch(e);
                    }}
                    endAdornment={
                      <>
                        {filterContentSearch.length > 0 && (
                          <InputAdornment position="end">
                            <IconButton
                              onClick={handleClearContentSearch}
                              edge="end"
                            >
                              <CloseIcon />
                            </IconButton>
                          </InputAdornment>
                        )}
                      </>
                    }
                  />
                </FormControl>
                :
                <FormControl size="small" className="lg:w-[220px] w-full">
                  <InputLabel id="demo-select-small">Vị trí</InputLabel>
                  <Select
                    labelId="demo-select-small"
                    id="demo-select-small"
                    label="Vị trí..."
                    MenuProps={MenuProps}
                    onChange={(e) => {
                      const tmp: User[] = [];
                      listUserTmp.map((item) => String(item.role).includes(e.target.value.toString()) && tmp.push(item));
                      setListUser(tmp);
                    }}
                  >
                    <MenuItem value={RoleUser[2]}>Quản lí</MenuItem>
                    <MenuItem value={RoleUser[1]}>NV thẩm định</MenuItem>
                    <MenuItem value={RoleUser[3]}>NV điều hành</MenuItem>
                    <MenuItem value={RoleUser[4]}>NV sale</MenuItem>
                    <MenuItem value={RoleUser[0]}>Khách hàng</MenuItem>
                  </Select>
                </FormControl>
              }
            </div>

            <Button
              className="addDesktop"
              variant="outlined"
              startIcon={<AddIcon />}
              onClick={() => {
                onClickShowModal();
              }}
              style={{
                whiteSpace: "nowrap",
                minWidth: "100px",
                marginLeft: "10px",
              }}
            >
              Thêm mới
            </Button>

            <button className="addMobile" onClick={() => onClickShowModal()}>
              +
            </button>
          </div>

          {listUser.length > 0 ?
            <div className="mt-5 overflow-y-auto h-[70vh] list_contract">
              {listUser?.map((item, index) => {
                return (
                  <>
                    <div
                      className="flex justify-between items-center flex-wrap lg:flex-nowrap border-2 mb-2 mr-2 p-3 rounded-md cursor-pointer leading-4 custom__bs"
                      key={index}
                      onClick={() => { onClickUserInfo(item.id) }}
                    >

                      <div
                        className="contract__items w-full lg:w-[40%]"
                        style={{ marginTop: "5px" }}
                      >
                        <span className="title">
                          Họ tên:
                          <span className="content"> {item.name}</span>
                        </span>
                        <span className="title">
                          Công việc:
                          <span className="content"> {item.job}</span>
                        </span>
                        <span className="title">
                          Địa chỉ hiện tại:
                          <span className="content"> {item.currentAddress}</span>
                        </span>
                      </div>

                      <div className="contract__items w-full lg:w-[40%]">
                        <span className="title">
                          Số điện thoại:
                          <span className="content"> {item.tel}</span>
                        </span>
                        <span className="title">
                          Email:
                          <span className="content"> {item.email}</span>
                        </span>
                        <span className="title">
                          Chức vụ:
                          <span className="content"> {VieRole[Number(RoleUser[item.role])]} </span>
                        </span>
                      </div>

                      <div className="action_info text-center lg:text-end w-full lg:w-[20%]">
                        {Number(RoleUser[auth?.userProfile?.role]) === Number(RoleUser["Admin"]) &&

                          <Button
                            sx={{ mt: 1, mb: 1 }}
                            variant="outlined"
                            color="warning"
                            startIcon={<DeleteIcon />}
                            onClick={(e) => { onClickShowModalDeleteUser(item.id, e) }}
                          >
                            Xóa
                          </Button>
                        }
                      </div>
                    </div>
                  </>
                );
              })}
            </div>
            : <FilterNotFound title={"Không tìm thấy người dùng phù hợp!"} />
          }

          <CreateUser
            stateProps={isShowModal}
            close={onClickCloseModal}
            reloadPage={reloadPage}
          />

          <Confirm
            close={onClickCloseModalDeleteUser}
            stateOpen={isShowModalDeleteUser}
            title={"Xác nhận xóa người dùng"}
            function={onClickDeleteUser}
          />

          <Snackbar
            anchorOrigin={{ vertical: "top", horizontal: "right" }}
            key={"top right"}
            open={openNotify}
            autoHideDuration={3000}
            onClose={handleCloseNotify}
          >
            {typeNotifi === "success" ? (
              <Alert
                color={"info"}
                onClose={handleCloseNotify}
                severity={"success"}
                sx={{ width: "100%" }}
              >
                {messageNotify}
              </Alert>
            ) : (
              <Alert
                color={"error"}
                onClose={handleCloseNotify}
                severity={"error"}
                sx={{ width: "100%" }}
              >
                {messageNotify}
              </Alert>
            )}
          </Snackbar>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};
