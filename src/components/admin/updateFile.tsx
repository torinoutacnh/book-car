import { Alert, Box, Button, FormControl, FormControlLabel, InputLabel, MenuItem, Modal, Radio, RadioGroup, Select, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext, FormEvent, ChangeEvent, createElement } from "react";
import "./styles/file.scss"
import TextField from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { useForm } from "react-hook-form";
import { RoleUser } from "interface/authenticate";
import UploadFile from "@mui/icons-material/UploadFile";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import { Contract } from "interface/contract";
import { contractGroupEndpoints } from "routers/apiEndpoints";

export const UpdateFile = (props: { stateProps: boolean, close: any, reloadPage: any, contract: Contract }) => {
    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const [isShow, setIsShow] = useState(false);

    type FormCreateFile = {
        CitizenIdentifyImage1?: FileList,
        CitizenIdentifyImage2?: FileList,
        DrivingLisenceImage1?: FileList,
        DrivingLisenceImage2?: FileList,
        HousePaperImages?: FileList,
        PassportImages?: FileList,
        OtherImages?: FileList,
    };
    const { register, handleSubmit } = useForm<FormCreateFile>()

    useEffect(() => {
        setIsShow(props.stateProps)
    }, [props.stateProps, props.reloadPage])

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")
    const [listFile1, setListFile1] = useState<FileList>()
    const [listFile2, setListFile2] = useState<FileList>()
    const [listFile3, setListFile3] = useState<FileList>()

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }



    const { auth } = useContext(AuthContext);

    const handleClickSubmit = async (data: FormCreateFile) => {
        // if (!auth) return;
        const formData = new FormData()

        console.log("contract ", props?.contract?.id)

        formData.append("CitizenIdentifyImage1", data.CitizenIdentifyImage1[0])
        formData.append("CitizenIdentifyImage2", data.CitizenIdentifyImage2[0])
        formData.append("DrivingLisenceImage1", data.DrivingLisenceImage1[0])
        formData.append("DrivingLisenceImage2", data.DrivingLisenceImage2[0])

        for (let i = 0; i < data.HousePaperImages.length; i++) {
            formData.append("HousePaperImages", data.HousePaperImages[i])
        }

        for (let i = 0; i < data.OtherImages.length; i++) {
            formData.append("OtherImages", data.OtherImages[i])
        }

        for (let i = 0; i < data.PassportImages.length; i++) {
            formData.append("PassportImages", data.PassportImages[i])
        }

        if (!data.CitizenIdentifyImage1[0] || !data.CitizenIdentifyImage2[0]
            || !data.DrivingLisenceImage1[0] || !data.DrivingLisenceImage2[0]) {
            handleOpenNotify("Vui lòng chọn đầy đủ ảnh", "error")
            return
        }

        const res = await fetch(process.env.REACT_APP_API + contractGroupEndpoints.updateFile + props?.contract?.id, {
            method: "POST",
            headers: {
                Authorization: "Bearer " + auth.accessToken,
            },
            body: formData,
        })
        const data_res = await res.json()

        if (res.status > 200) {
            handleOpenNotify(data_res.message, "error")
            // console.log("Update file status ", res.status, " => ", data_res)
            return
        }

        handleOpenNotify("Cập nhật hình ảnh thành công!", "success")

        const listImg = document.querySelectorAll("img")
        listImg.forEach((value, key) => {
            value.classList.remove("active")
            value.src = ""
        })
        props.close();
        props.reloadPage()

    }

    const preViewImage = (event: ChangeEvent<HTMLInputElement>, id_class_Element: string) => {


        const fileList = event.target.files

        switch (id_class_Element) {
            case "PassportImages": {
                setListFile1(fileList)
                break
            }

            case "HousePaperImages": {
                setListFile2(fileList)
                break
            }
            case "OtherImages": {
                setListFile3(fileList)
                break
            }
            default: {

                const output = document.getElementById(id_class_Element) as HTMLImageElement;
                output.src = URL.createObjectURL(fileList[0]);
                output.classList.add("active")

                break
            }

        }

        // console.log("onchange", fileList);

    }

    return (
        <>

            {isShow ?
                <Modal
                    open={isShow}
                    onClose={() => props.close()}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box
                        className="Box_create_file"
                        sx={style}
                        component="form"
                        noValidate
                        autoComplete="off"
                        onSubmit={handleSubmit(data => { handleClickSubmit(data) })}
                    >
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Cập nhật thông tin hình ảnh
                        </Typography>

                        <div className="listImage">

                            <div className="item_box_image">
                                <div className="select">
                                    <label htmlFor="InputCitizenIdentifyImage1"> <AddPhotoAlternateIcon /> CCCD/CMND mặt trước *</label>
                                    <input
                                        id="InputCitizenIdentifyImage1"
                                        name="CitizenIdentifyImage1"
                                        required
                                        {...register("CitizenIdentifyImage1")}
                                        type="file"
                                        accept="image/*"
                                        onChange={event => preViewImage(event, "CitizenIdentifyImage1")}
                                    />
                                </div>
                                <img id="CitizenIdentifyImage1" className="image" />
                            </div>

                            <div className="item_box_image">
                                <div className="select">
                                    <label htmlFor="InputCitizenIdentifyImage2"> <AddPhotoAlternateIcon /> CCCD/CMND mặt sau *</label>
                                    <input
                                        id="InputCitizenIdentifyImage2"
                                        name="CitizenIdentifyImage2"
                                        required
                                        {...register("CitizenIdentifyImage2")}
                                        type="file"
                                        accept="image/*"
                                        onChange={event => preViewImage(event, "CitizenIdentifyImage2")}
                                    />
                                </div>
                                <img id="CitizenIdentifyImage2" className="image" />
                            </div>

                            <div className="item_box_image">
                                <div className="select">
                                    <label htmlFor="InputDrivingLisenceImage1"> <AddPhotoAlternateIcon /> GPLX mặt trước *</label>
                                    <input
                                        id="InputDrivingLisenceImage1"
                                        name="CitizenIdentifyImage2"
                                        required
                                        {...register("DrivingLisenceImage1")}
                                        type="file"
                                        accept="image/*"
                                        onChange={event => preViewImage(event, "DrivingLisenceImage1")}
                                    />
                                </div>
                                <img id="DrivingLisenceImage1" className="image" />
                            </div>

                            <div className="item_box_image">
                                <div className="select">
                                    <label htmlFor="InputDrivingLisenceImage2" > <AddPhotoAlternateIcon />GPLX mặt sau *</label>
                                    <input
                                        id="InputDrivingLisenceImage2"
                                        name="DrivingLisenceImage2"
                                        required
                                        {...register("DrivingLisenceImage2")}
                                        type="file"
                                        accept="image/*"
                                        onChange={event => preViewImage(event, "DrivingLisenceImage2")}
                                    />
                                </div>
                                <img id="DrivingLisenceImage2" className="image" />
                            </div>

                            <div className="item_box_image">
                                <div className="select">
                                    <label htmlFor="InputPassportImages"> <AddPhotoAlternateIcon /> InputPassportImages</label>
                                    <input
                                        id="InputPassportImages"
                                        name="PassportImages"
                                        required
                                        {...register("PassportImages")}
                                        type="file"
                                        accept="image/*"
                                        onChange={event => preViewImage(event, "PassportImages")}
                                        multiple
                                    />
                                </div>
                                <div className="box_image">
                                    {
                                        listFile1 &&
                                        Array.from(listFile1).map((item, index) => {
                                            return (
                                                <>
                                                    <img src={URL.createObjectURL(item)} />
                                                </>
                                            )
                                        })
                                    }
                                </div>

                            </div>

                            <div className="item_box_image">
                                <div className="select">
                                    <label htmlFor="InputHousePaperImages"> <AddPhotoAlternateIcon /> InputHousePaperImages</label>
                                    <input
                                        id="InputHousePaperImages"
                                        name="HousePaperImages"
                                        required
                                        {...register("HousePaperImages")}
                                        type="file"
                                        accept="image/*"
                                        multiple
                                        onChange={event => preViewImage(event, "HousePaperImages")}
                                    />
                                </div>
                                <div className="box_image">
                                    {
                                        listFile2 &&
                                        Array.from(listFile2).map((item, index) => {
                                            return (
                                                <>
                                                    <img src={URL.createObjectURL(item)} />
                                                </>
                                            )
                                        })
                                    }
                                </div>
                            </div>

                            <div className="item_box_image">
                                <div className="select">

                                    <label htmlFor="InputOtherImages"> <AddPhotoAlternateIcon /> InputOtherImages</label>
                                    <input
                                        id="InputOtherImages"
                                        name="OtherImages"
                                        required
                                        {...register("OtherImages")}
                                        type="file"
                                        accept="image/*"
                                        multiple
                                        onChange={event => preViewImage(event, "OtherImages")}
                                    />
                                </div>
                                <div className="box_image">
                                    {
                                        listFile3 &&
                                        Array.from(listFile3).map((item, index) => {
                                            return (
                                                <>
                                                    <img src={URL.createObjectURL(item)} />
                                                </>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>



                        <Button type="submit" startIcon={<UploadFile />}>Cập nhật</Button>
                    </Box>
                </Modal>
                : <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}