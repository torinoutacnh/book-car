import { FormControl, InputLabel, Select, MenuItem, SelectChangeEvent, Typography, TextField, Pagination } from "@mui/material";
import { DesktopDatePicker } from "@mui/x-date-pickers";
import { FilterNotFound } from "components/loading/contractNotFound";
import { Loading } from "components/loading/loading";
import AuthContext from "context/AuthContext";
import { User } from "interface/authenticate";
import { CarClassType, XPagination } from "interface/car";
import { Contract, RentStatus } from "interface/contract";
import { FilterContract } from "interface/filterContract";
import { Moment } from "moment";
import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { adminEndpoints } from "routers/endpoints";
import "./styles/manager.scss";
import { contractGroupEndpoints, userEndpoints } from "routers/apiEndpoints";

export const ManagerContract = () => {

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.7 + ITEM_PADDING_TOP,
                width: 220,
            },
        },
    }

    const { auth } = useContext(AuthContext);
    const [listContract, setListContract] = useState<Contract[]>();
    const [listContractTmp, setListContractTmp] = useState<Contract[]>();
    const [listUser, setListUser] = useState<User[]>();
    const navigate = useNavigate();

    const [fromdate, setFromdate] = useState<Moment | null>(null);

    const [xpagination, setXpagination] = useState<XPagination>()


    const handleChangeFromdate = (newValue: Moment | null) => {
        setFromdate(newValue);
        setState({ ...state, query: { ...state.query, fromDate: newValue } })
        setCount(count + 1)
    }

    const filter: FilterContract = {
        query: {
            carClass: null,
            carNumber: null,
            email: null,
            rentStatus: null,
            fromDate: null
        },
        pageModel: {
            pageNumber: 1,
            pageSize: 10
        }
    }

    const handleClearSearch = () => {
        setState(filter);
        setListContract(listContractTmp);
    };

    const [state, setState] = useState<FilterContract>(filter);
    const [count, setCount] = useState(0);

    useEffect(() => {
        if (!auth) return;


        fetch(process.env.REACT_APP_API + contractGroupEndpoints.base, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(state),
        })
            .then(async (res) => {
                if (res.status > 200) throw new Error(JSON.stringify(await res.json()));

                const tmp: XPagination = JSON.parse(res.headers.get("x-pagination"))
                setXpagination(tmp)

                return res.json();
            })
            .then((data) => {

                setListContract(data.data)
                setListContractTmp(data.data)
            })
            .catch((err) => {
                // console.log(err)
            });

        fetch(process.env.REACT_APP_API + userEndpoints.base, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
        })
            .then((res) => {
                if (!res.ok) throw new Error("");
                return res.json();
            })
            .then((data) => setListUser(data.data))
            .catch((err) => {
                // console.log(err)
            });
    }, [auth?.userProfile?.id, count]);

    const onClickContractInfo = (id: string) => {
        navigate("/" + adminEndpoints.base + "/" + adminEndpoints.contractInfo + "?" + new URLSearchParams({ idContract: id }));
    };

    const [filterSearch, setFilterSearch] = useState("info");
    const handleChangeFilterSearch = (event: SelectChangeEvent) => {
        setFilterSearch(event.target.value as string);
        setCount(count + 1);
        setState(filter);
    };
    const [bodyByUser, setBodyByUser] = useState({ pageNumber: 1, pageSize: 10 })
    const [count2, setCount2] = useState(0)
    const [idUser, setIdUser] = useState("")
    const filterByUser = async () => {
        if (!auth) return;
        const res = await fetch(process.env.REACT_APP_API + contractGroupEndpoints.filterByUser + idUser, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(bodyByUser)
        })
        const data = await res.json()

        if (res.status > 200) {
            return
        }
        const tmp: XPagination = JSON.parse(res.headers.get("x-pagination"))
        setXpagination(tmp)
        setListContract(data.data)
        // console.log("tmp ", tmp)
    }
    useEffect(() => {
        if (count2 === 0) { return }
        filterByUser()
    }, [count2])

    return (
        <div >
            {listContract ? (
                <div className="page-manager-contract">
                    <div className="flex justify-start items-center lg:flex-nowrap flex-wrap gap-y-3 lg:gap-y-0">
                        {/* Tìm kiếm theo */}
                        <div className="w-full lg:w-[20%] lg:mr-3">
                            <FormControl className="w-full">
                                <InputLabel id="demo-simple-select-label" size="small">
                                    Tìm kiếm theo
                                </InputLabel>
                                <Select
                                    className="w-full"
                                    size="small"
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={filterSearch}
                                    label="Tìm kiếm theo"
                                    onChange={handleChangeFilterSearch}
                                    defaultValue={filterSearch}
                                >
                                    <MenuItem key={0} value={"info"}>
                                        Thông tin chi tiết
                                    </MenuItem>
                                    <MenuItem key={1} value={"user"}>
                                        Người tạo hợp đồng
                                    </MenuItem>
                                </Select>
                            </FormControl>
                        </div>

                        <div>
                            {filterSearch === "info" &&
                                <div className="ipt-contract flex justify-start items-center lg:flex-nowrap flex-wrap gap-x-3 gap-y-3 lg:gap-y-0">
                                    <div className="flex-contract">
                                        {/* Từ ngày */}
                                        <DesktopDatePicker
                                            label="Từ ngày"
                                            inputFormat="DD/MM/YYYY"
                                            className="w-full lg:w-[11vw]"
                                            value={fromdate}
                                            onChange={handleChangeFromdate}
                                            renderInput={(params) =>
                                                <TextField
                                                    className="w-full lg:w-[150px]"
                                                    {...params}
                                                    size='small'
                                                />}
                                        />

                                        {/* Phân khúc */}
                                        <FormControl>
                                            <InputLabel id="demo-select-label" size="small">
                                                Phân khúc
                                            </InputLabel>
                                            <Select
                                                size="small"
                                                className="w-full lg:w-[8vw]"
                                                labelId="demo-select-label"
                                                id="demo-simple-select"
                                                value={CarClassType[filter?.query?.carClass as number]}
                                                defaultValue={"null"}
                                                label="Phân khúc"
                                                onChange={(e) => {
                                                    setState({
                                                        ...state,
                                                        query: {
                                                            ...state?.query,
                                                            carClass: e.target.value === "null" ? null : CarClassType[e.target.value as keyof typeof CarClassType]
                                                        }
                                                    })
                                                    setCount(count + 1)
                                                }}
                                            >
                                                <MenuItem value={"null"}>Tất cả</MenuItem>
                                                {
                                                    Object.keys(CarClassType).filter((value) => isNaN(value as any))
                                                        .map((value, index) => {
                                                            return (
                                                                <MenuItem key={index} value={value}>
                                                                    {value}
                                                                </MenuItem>
                                                            )
                                                        })
                                                }
                                            </Select>
                                        </FormControl>
                                    </div>
                                    {/*  Trạng thái hợp đồng */}
                                    <div className="w-full lg:w-[18vw]">
                                        <FormControl className="w-full lg:w-[18vw]">
                                            <InputLabel id="demo-simple-select-label" size="small">
                                                Trạng thái hợp đồng
                                            </InputLabel>
                                            <Select
                                                MenuProps={MenuProps}
                                                size="small"
                                                className="w-full lg:w-[18vw]"
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={RentStatus[filter?.query?.rentStatus as number]}
                                                label="Trạng thái hợp đồng"
                                                defaultValue={"null"}
                                                onChange={(e) => {
                                                    setState({
                                                        ...state,
                                                        query: {
                                                            ...state?.query,
                                                            rentStatus: e.target.value === "null" ? null : Number(RentStatus[e.target.value as keyof typeof RentStatus])
                                                        }
                                                    })
                                                    setCount(count + 1)
                                                }}
                                            >
                                                <MenuItem value={"null"}>Tất cả</MenuItem>
                                                {
                                                    Object.keys(RentStatus).filter((value) => isNaN(value as any))
                                                        .map((value, index) => {
                                                            return (
                                                                <MenuItem key={index} value={value}>
                                                                    {value}
                                                                </MenuItem>
                                                            )
                                                        })
                                                }
                                            </Select>
                                        </FormControl>
                                    </div>
                                    {/* Biển kiểm soát */}
                                    <div className="flex-contract w-full">
                                        <TextField
                                            id="outlined-basic"
                                            className="w-full lg:w-[10vw]"
                                            label="Biển kiểm soát"
                                            variant="outlined"
                                            size="small"
                                            defaultValue={null}
                                            onChange={(e) => {
                                                setState({
                                                    ...state,
                                                    query: {
                                                        ...state.query,
                                                        carNumber: e.target.value
                                                    }
                                                })
                                                setCount(count + 1)
                                            }}
                                        />
                                        {/* Email */}
                                        <TextField
                                            id="outlined-basic"
                                            label="Email"
                                            variant="outlined"
                                            size="small"
                                            className="w-full lg:w-[14vw]"
                                            defaultValue={filter?.query?.carNumber}
                                            onChange={(e) => {
                                                setState({
                                                    ...state,
                                                    query: {
                                                        ...state.query,
                                                        email: e.target.value
                                                    }
                                                })
                                                setCount(count + 1)
                                            }}
                                        />
                                    </div>
                                </div>
                            }
                        </div>

                        <div className="lg:w-[220px] w-full">
                            {filterSearch === "user" &&
                                <FormControl size="small" className="w-full">
                                    <InputLabel id="demo-select-small">Danh sách</InputLabel>
                                    <Select
                                        labelId="demo-select-small"
                                        id="demo-select-small"
                                        label="Danh sách"
                                        MenuProps={MenuProps}
                                    >
                                        {listUser?.map((item, index) => (
                                            <MenuItem
                                                value={item.id}
                                                key={index}
                                                style={{ overflow: 'hidden' }}
                                                onClick={() => { setIdUser(item.id); setCount2(count2 + 1) }}
                                            >
                                                <Typography noWrap>
                                                    {item.name}
                                                </Typography>
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            }
                        </div>
                    </div>

                    {listContract.length > 0 ? (
                        <div className="list_contract">
                            {listContract?.map((item, index) => {
                                const User = listUser?.find(user => user.id === item.renterId)

                                return (
                                    <>
                                        <div
                                            className="grid grid-cols-1 lg:grid-cols-3 border-2 mb-2 gap-3 p-3 rounded-md cursor-pointer custom__bs"
                                            key={index}
                                            onClick={() => { onClickContractInfo(item.id) }}
                                        >
                                            <div className="contract__items">
                                                <span className="title">
                                                    Hãng xe:
                                                    <span className="content">{item?.requireDescriptionInfo?.carBrand}</span>
                                                </span>
                                                <span className="title">
                                                    Đời xe:
                                                    <span className="content">{item?.requireDescriptionInfo?.yearCreate}</span>
                                                </span>
                                                <span className="title">
                                                    Số chỗ:
                                                    <span className="content">{item?.requireDescriptionInfo?.seatNumber} chỗ</span>
                                                </span>
                                                <span className="title">
                                                    Màu xe:
                                                    <span className="content">{item?.requireDescriptionInfo?.carColor}</span>
                                                </span>
                                            </div>

                                            <div className="contract__items">
                                                <span className="title">
                                                    Tên bên thuê:
                                                    <span className="content">{User?.name}</span>
                                                </span>
                                                <span className="title">
                                                    Địa chỉ:
                                                    <span className="content">{User?.currentAddress}</span>
                                                </span>
                                                <span className="title">
                                                    Số điện thoại:
                                                    <span className="content"> {User?.tel}</span>
                                                </span>
                                                <span className="title">
                                                    Email:
                                                    <span className="content">{User?.email}</span>
                                                </span>
                                            </div>

                                            <div className="contract__items">
                                                <span className="title">
                                                    Thuê từ ngày:
                                                    <span className="content">{item?.rentFrom?.toString()?.slice(0, 10)}</span>
                                                </span>
                                                <span className="title">
                                                    Đến ngày:
                                                    <span className="content">{item?.rentTo?.toString()?.slice(0, 10)}</span>
                                                </span>
                                                <span className="title">
                                                    Mục đích thuê:
                                                    <span className="content">{item?.rentPurpose}</span>
                                                </span>
                                                <span className="title">
                                                    Trạng thái hợp đồng:
                                                    <span className="content">{RentStatus[item?.status]}</span>
                                                </span>
                                            </div>

                                        </div>
                                    </>
                                );
                            })}
                        </div>

                    ) : (<FilterNotFound title={"Không tìm thấy hợp đồng phù hợp!"} />)}

                    {xpagination?.TotalPages > 0 &&
                        <div className="flex justify-center items-center">
                            <Pagination
                                size="small"
                                count={xpagination?.TotalPages} variant="outlined" color="secondary"
                                onChange={(event, page) => {
                                    console.log(filterSearch)
                                    if (filterSearch === "user" && idUser.length > 0) {
                                        setBodyByUser({ ...bodyByUser, pageNumber: page })
                                        setCount2(count2 + 1)
                                    }
                                    else {
                                        setState({
                                            ...state,
                                            pageModel: {
                                                ...state.pageModel,
                                                pageNumber: page
                                            }
                                        })
                                        setCount(count + 1)
                                    }
                                }}
                            />
                        </div>}

                </div>
            ) : (
                <Loading />
            )}
        </div>
    );
};
