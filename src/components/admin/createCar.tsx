import { Alert, Box, Button, FormControl, InputLabel, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext, ChangeEvent } from "react";
import "./styles/crud.scss"
import "./styles/file.scss"
import TextField from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import moment from "moment";
import AuthContext from "context/AuthContext";
import { useForm } from "react-hook-form";
import { Car, CarClassType, GeneralInfoCar } from "interface/car";
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import { useMutation, useQueryClient } from "react-query"
import { ConfigContext } from "provider";
import { carEndpoints } from "routers/apiEndpoints";
import { adminEndpoints } from "routers/endpoints";
import { Link } from "react-router-dom";

export function CreateCar(props: { stateProps: boolean, close: any, refetch: any }) {

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const { register, handleSubmit, watch, formState: { errors }, reset } = useForm<Car>();

    const [isShow, setIsShow] = useState(false);

    useEffect(() => {
        setIsShow(props.stateProps)
    }, [props.stateProps])

    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            },
        },
    };

    const { auth } = useContext(AuthContext);
    const { configCar } = useContext(ConfigContext)
    const [configPrice, setConfigPrice] = useState<GeneralInfoCar>(null)
    const [classSelect, setClassSelect] = useState(0)
    const configDefaultA = configCar?.carCreate?.find(item => item.carClass === 0)?.generalInfo

    useEffect(() => {
        const configDefault = configCar?.carCreate?.find(item => item.carClass === classSelect)?.generalInfo
        setConfigPrice(configDefault)
    }, [classSelect, configCar])

    const resetFields = () => {
        reset(formValues => ({
            ...formValues,
            descriptionInfo: {
                carColor: "",
                carNumber: "",
                carDescription: "",
                carName: "",
                carVersion: "",
            },
            carParkingLot: "",
            carClass: 0,
            generalInfo: {
                priceForNormalDay: configDefaultA?.priceForNormalDay,
                priceForWeekendDay: configDefaultA?.priceForWeekendDay,
                priceForHoliday: configDefaultA?.priceForHoliday,
                pricePerHourExceed: configDefaultA?.pricePerHourExceed,
                pricePerKmExceed: configDefaultA?.pricePerKmExceed,
                limitedKmForMonth: configDefaultA?.limitedKmForMonth,
            },
            carState: {
                carStatusDescription: "",
                currentEtcAmount: 0,
                fuelPercent: 0,
                speedometerNumber: 0
            },
            carImage: "",
            loanerId: ""
        }))
    }

    const submitForm = async (data: Car) => {
        // if (!auth) return;

        const list = data.carImage as FileList
        data.carImage = list[0]

        if (!data.carImage) {
            handleOpenNotify("Vui lòng chọn ảnh!", "error")
            return
        }

        const carCreate = {
            CarClass: data.carClass,
            CarParkingLot: data.carParkingLot,
            CarBrand: data.descriptionInfo.carBrand,
            CarName: data.descriptionInfo.carName,
            CarVersion: data.descriptionInfo.carVersion,
            CarStyle: data.descriptionInfo.carStyle,
            CarTransmission: data.descriptionInfo.carTransmission,
            CarFuelUse: data.descriptionInfo.carFuelUse,
            SeatNumber: data.descriptionInfo.seatNumber,
            YearCreate: data.descriptionInfo.yearCreate,
            CarColor: data.descriptionInfo.carColor,
            CarNumber: data.descriptionInfo.carNumber,
            CarDescription: data.descriptionInfo.carDescription,
            PriceForNormalDay: data.generalInfo.priceForNormalDay,
            PriceForWeekendDay: data.generalInfo.priceForWeekendDay,
            PriceForHoliday: data.generalInfo.priceForHoliday,
            PriceForMonth: data.generalInfo.priceForMonth,
            LimitedKmForMonth: data.generalInfo.limitedKmForMonth,
            PricePerKmExceed: data.generalInfo.pricePerKmExceed,
            PricePerHourExceed: data.generalInfo.pricePerHourExceed,
            SpeedometerNumber: data.carState.speedometerNumber,
            FuelPercent: data.carState.fuelPercent,
            CurrentEtcAmount: data.carState.currentEtcAmount,
            CarStatusDescription: data.carState.carStatusDescription,
        }

        const formData = new FormData()
        Object.entries(carCreate).forEach(value => {
            formData.append(value[0], String(value[1]));

            // console.log(value[0], value[1])
        })
        formData.append("CarImage", data.carImage);

        for (const [key, value] of formData.entries()) {
            console.log(key, " => ", value)
        }


        return fetch(process.env.REACT_APP_API + carEndpoints.createCar, {
            method: "POST",
            headers: {
                // "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: formData,
        })

    }
    const queryClient = useQueryClient()
    const mutationCreateCar = useMutation(
        (car: Car) => submitForm(car),
        {
            onSuccess: async (res, variable, context) => {
                const data = await res.json()
                if (res.status > 200) {
                    handleOpenNotify(data.message, "error")
                    // console.log(`mutationCreateCar create car status ${res.status} => `, data)
                    return
                }
                // console.log("mutationCreateCar create car status 200", data)
                handleOpenNotify("Tạo mới xe thành công!", "success")
                queryClient.invalidateQueries(["getAllCar"])
                props?.refetch()
                resetFields()
                props.close()
            },
            onError: (error, variable, context) => {
                // console.log("error", error)
            }
        })

    const years: number[] = Array.from(
        { length: (2000 - moment().year()) / -1 + 1 },
        (_, i) => moment().year() + i * -1
    );

    const preViewImage = (event: ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files[0]
        const output = document.getElementById("imgCarImage") as HTMLImageElement;
        output.src = URL.createObjectURL(file);
        output.classList.add("active")
        // console.log("onchange", file);
    }

    return (
        <>

            {isShow ?
                <Modal
                    open={isShow}
                    onClose={() => {
                        resetFields();
                        props.close();
                    }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} className="form_contract">
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Tạo mới thông tin xe
                        </Typography>
                        <Typography sx={{ color: "red" }}>
                            (*) Nếu không tìm thấy thông tin tạo xe phù hợp, vui lòng &nbsp;
                            <Link style={{ color: "blue", textDecoration: "underline" }} to={"/" + adminEndpoints.base + "/" + adminEndpoints.config}>tạo mới</Link>
                        </Typography>

                        <form id="formCreateCar" className="container_create" onSubmit={handleSubmit(data => mutationCreateCar.mutate(data))}>

                            <div className="info">

                                <TextField
                                    id="outlined-basic"
                                    label="Biển kiểm soát"
                                    sx={{ m: 1, width: '30ch' }}
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("descriptionInfo.carNumber")}
                                />

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Màu xe</InputLabel>
                                    <Select
                                        size="small"
                                        MenuProps={MenuProps}
                                        label={"Màu xe"}
                                        required
                                        {...register("descriptionInfo.carColor")}
                                    >
                                        {
                                            configCar?.carColor?.sort().map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>Màu {item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Kiểu dáng</InputLabel>
                                    <Select
                                        size="small"
                                        label={"Kiểu dáng"}
                                        MenuProps={MenuProps}
                                        required
                                        {...register("descriptionInfo.carStyle")}
                                    >
                                        {
                                            configCar?.carStyle?.sort().map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Truyền động</InputLabel>
                                    <Select
                                        size="small"
                                        label={"Truyền động"}
                                        required
                                        {...register("descriptionInfo.carTransmission")}
                                    >
                                        {
                                            configCar?.carTransmission?.sort().map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <TextField
                                        aria-label="minimum height"
                                        multiline
                                        maxRows={3}
                                        placeholder="Mô tả"
                                        size="small"
                                        {...register("descriptionInfo.carDescription")}
                                    />
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Nhiên liệu sử dụng</InputLabel>
                                    <Select
                                        size="small"
                                        label={"Nhiên liệu sử dụng"}
                                        required
                                        {...register("descriptionInfo.carFuelUse")}
                                    >
                                        {
                                            configCar?.carFuelUse?.sort().map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Trạng thái xe"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("carState.carStatusDescription")}
                                />

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Bãi xe</InputLabel>
                                    <Select
                                        size="small"
                                        label={"Bãi xe"}
                                        required
                                        MenuProps={MenuProps}
                                        {...register("carParkingLot")}
                                    >
                                        {
                                            configCar?.carParkingSlot?.sort().map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                                        <div className="select">
                                            <label htmlFor="InputcarImage" style={{ fontSize: "15px", color: "#1976d2", cursor: "pointer" }}> <AddPhotoAlternateIcon /> Hình ảnh*</label>
                                            <input
                                                style={{ position: "absolute", right: "2000%" }}
                                                id="InputcarImage"
                                                {...register("carImage")}
                                                type="file"
                                                accept="image/*"
                                                onChange={event => preViewImage(event)}
                                            />
                                        </div>
                                        <img id="imgCarImage" style={{ borderRadius: "4px", width: "100%", maxHeight: "300px" }} />
                                    </div>
                                </FormControl>

                            </div>

                            <div className="info">
                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Hãng xe</InputLabel>
                                    <Select
                                        size="small"
                                        label={"Hãng xe"}
                                        MenuProps={MenuProps}
                                        required
                                        {...register("descriptionInfo.carBrand")}
                                    >
                                        {
                                            configCar?.carBrand?.map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Tên xe"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("descriptionInfo.carName")}
                                />

                                <TextField
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Phiên bản"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("descriptionInfo.carVersion")}
                                />

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Số chỗ</InputLabel>
                                    <Select
                                        size="small"
                                        label={"Số chỗ"}
                                        required
                                        {...register("descriptionInfo.seatNumber")}
                                    >
                                        {
                                            configCar?.carSeat?.map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item} chỗ</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Năm sản suất</InputLabel>
                                    <Select
                                        size="small"
                                        MenuProps={MenuProps}
                                        label={"Năm sản suất"}
                                        required
                                        {...register("descriptionInfo.yearCreate")}
                                    >
                                        {
                                            years.map((item, index) => {
                                                return (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>

                                <FormControl required sx={{ m: 1, width: '30ch' }}>
                                    <InputLabel>Phân khúc xe</InputLabel>
                                    <Select
                                        size="small"
                                        displayEmpty
                                        label={"Phân khúc xe"}
                                        required
                                        {...register("carClass")}
                                        onChange={(e) => { setClassSelect(Number(e.target.value)) }}
                                    >
                                        {Object.keys(CarClassType)
                                            .filter((value) => isNaN(value as any))
                                            .map((value) => (
                                                <MenuItem
                                                    key={value}
                                                    value={CarClassType[value as keyof typeof CarClassType]}
                                                >
                                                    {value}
                                                </MenuItem>
                                            ))}
                                    </Select>
                                </FormControl>

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Giá thuê ngày thường (VND/Ngày)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    defaultValue={configPrice?.priceForNormalDay}
                                    {...register("generalInfo.priceForNormalDay")}
                                    value={configPrice?.priceForNormalDay}
                                    onChange={(e) => {
                                        setConfigPrice({
                                            ...configPrice,
                                            priceForNormalDay: Number(e.target.value)
                                        })
                                    }}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    defaultValue={configPrice?.priceForWeekendDay}
                                    label="Giá thuê cuối tuần (VND/Ngày)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.priceForWeekendDay")}
                                    value={configPrice?.priceForWeekendDay}
                                    onChange={(e) => {
                                        setConfigPrice({
                                            ...configPrice,
                                            priceForWeekendDay: Number(e.target.value)
                                        })
                                    }}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Giá thuê ngày lễ (VND/Ngày)"
                                    variant="outlined"
                                    defaultValue={configPrice?.priceForHoliday}
                                    size="small"
                                    required
                                    {...register("generalInfo.priceForHoliday")}
                                    value={configPrice?.priceForHoliday}
                                    onChange={(e) => {
                                        setConfigPrice({
                                            ...configPrice,
                                            priceForHoliday: Number(e.target.value)
                                        })
                                    }}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Giá thuê tháng"
                                    variant="outlined"
                                    defaultValue={configPrice?.priceForMonth}
                                    size="small"
                                    required
                                    {...register("generalInfo.priceForMonth")}
                                    value={configPrice?.priceForMonth}
                                    onChange={(e) => {
                                        setConfigPrice({
                                            ...configPrice,
                                            priceForMonth: Number(e.target.value)
                                        })
                                    }}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    defaultValue={configPrice?.pricePerKmExceed}
                                    label="Giá thuê vượt quá kilomet (VND/Km)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.pricePerKmExceed")}
                                    value={configPrice?.pricePerKmExceed}
                                    onChange={(e) => {
                                        setConfigPrice({
                                            ...configPrice,
                                            pricePerKmExceed: Number(e.target.value)
                                        })
                                    }}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Giá thuê vượt quá thời gian (VND/Giờ)"
                                    defaultValue={configPrice?.pricePerHourExceed}
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.pricePerHourExceed")}
                                    value={configPrice?.pricePerHourExceed}
                                    onChange={(e) => {
                                        setConfigPrice({
                                            ...configPrice,
                                            pricePerHourExceed: Number(e.target.value)
                                        })
                                    }}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Giới hạn số km/tháng"
                                    defaultValue={configPrice?.limitedKmForMonth}
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("generalInfo.limitedKmForMonth")}
                                    value={configPrice?.limitedKmForMonth}
                                    onChange={(e) => {
                                        setConfigPrice({
                                            ...configPrice,
                                            limitedKmForMonth: Number(e.target.value)
                                        })
                                    }}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Số trên đồng hồ hiện tại"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("carState.speedometerNumber")}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Phần trăm nhiên liệu hiện tại (%)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("carState.fuelPercent")}
                                />

                                <TextField
                                    type="number"
                                    sx={{ m: 1, width: '30ch' }}
                                    id="outlined-basic"
                                    label="Số tiền ETC hiện tại (VND)"
                                    variant="outlined"
                                    size="small"
                                    required
                                    {...register("carState.currentEtcAmount")}
                                />

                            </div>

                        </form>

                        <div className="action">

                            <Button
                                size='small'
                                variant="contained"
                                startIcon={<AddIcon />}
                                className="btnCreate"
                                type="submit"
                                form="formCreateCar"
                            >
                                Tạo xe
                            </Button>
                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className='btnCancel'
                                onClick={() => {
                                    props.close();
                                    resetFields();

                                }}
                            >
                                Hủy bỏ
                            </Button>
                        </div>

                    </Box>
                </Modal>
                : <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}