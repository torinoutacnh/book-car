import { Alert, Box, Button, FormControl, InputLabel, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material"
import { useState, useEffect, useContext } from "react";
import "./styles/crud.scss"
import TextField from '@mui/material/TextField';
import CloseIcon from '@mui/icons-material/Close';
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import AuthContext from "context/AuthContext";
import { Contract } from "interface/contract";
import { useMutation, useQuery } from "react-query";
import { carEndpoints, contractGroupEndpoints } from "routers/apiEndpoints";
import BorderColorIcon from '@mui/icons-material/BorderColor';
import { Car } from "interface/car";

export function UpdateExpertiseInfo(props: { id: string, stateProps: boolean, close: any, reloadPage: any, contract: Contract }) {

    interface initValue {
        description: string,
        carId: string,
        result: number,
        resultOther: string,
        trustLevel: number,
        paymentAmount: number,
        depositInfo: {
            description: string,
            asset: string,
            downPayment: number
        }
    }

    const [isShow, setIsShow] = useState(false);
    const [typeNotifi, setTypeNotifi] = useState("success")
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("")
    const [state, setState] = useState<initValue>(null)
    const { auth } = useContext(AuthContext);

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        p: 4,
        borderRadius: '10px',
        textAlign: 'center',
    };

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 220,
            },
        },
    };

    const getCarByGroup = async (): Promise<Car[]> => {
        const res = await fetch(process.env.REACT_APP_API + carEndpoints.filterByGroup + props?.id, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify({ pageNumber: 1, pageSize: 10000 })
        })
        const data = await res.json()

        if (!res.ok) {
            throw new Error(JSON.stringify(data))
        }

        return data.data
    }
    const { data: listCar } = useQuery({
        queryKey: ["getCarByGroup"],
        queryFn: () => getCarByGroup()
    })

    useEffect(() => {
        setIsShow(props.stateProps)
        const resetValue = {
            description: props?.contract?.expertiseContract?.description,
            carId: props?.contract?.car?.id ?? '',
            result: props?.contract?.expertiseContract?.result,
            resultOther: String(props?.contract?.expertiseContract?.resultOther),
            trustLevel: props?.contract?.expertiseContract?.trustLevel,
            paymentAmount: props?.contract?.expertiseContract?.paymentAmount,
            depositInfo: {
                description: props?.contract?.expertiseContract?.depositInfo?.description,
                asset: props?.contract?.expertiseContract?.depositInfo?.asset,
                downPayment: props?.contract?.expertiseContract?.depositInfo?.downPayment
            }
        }
        setState(resetValue)
    }, [props.stateProps, props.reloadPage])


    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type)
        setMessageNotify(message)
        setOpenNofity(true)
    }

    const [expertiseDate, setExpertiseDate] = useState<Moment | null>(moment(props?.contract?.expertiseContract?.expertiseDate));
    const handleChangeExpertiseDate = (newValue: Moment | null) => {
        setExpertiseDate(newValue);
    }

    const statusResult = [
        {
            key: 0,
            result: "Đạt"
        },
        // {
        //     key: 1,
        //     result: "Thất bại"
        // },
        // {
        //     key: 2,
        //     result: "Đạt (Chưa có xe)"
        // }
    ]

    const trustLevel: number[] = Array.from({ length: (10 - 1) / 1 + 1 }, (_, i) => 1 + (i * 1));

    const apiUpdateExpertiseInfo = () => {
        if (!auth) return;

        // const checkNull = { a: false }

        // // console.log(state)

        // Object.values(state).forEach((value, index) => {
        //     const tmp = value as string
        //     if (tmp.length === 0 && index > 2) {
        //         checkNull.a = true
        //     }
        // })

        // if (checkNull.a) {
        //     handleOpenNotify('Vui lòng nhập đầy đủ thông tin!', 'error')
        //     return
        // }




        const data_body = {
            groupId: props?.contract?.id,
            carId: state.carId,
            updateExpertise: {
                expertiseDate: expertiseDate.format("YYYY-MM-DD"),
                description: state.description,
                result: state.result,
                resultOther: state.resultOther,
                trustLevel: state.trustLevel,
                paymentAmount: state.paymentAmount ?? 0,
                depositInfo: {
                    description: state.depositInfo.description,
                    asset: state.depositInfo.asset,
                    downPayment: state.depositInfo.downPayment ?? 0
                }
            }
        }

        if (state?.carId?.length === 0) {
            data_body.carId = null;
        }
        console.log(data_body)

        const res = fetch(process.env.REACT_APP_API + contractGroupEndpoints.updateExpertise, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(data_body),
        })

        return res
    }
    const mutation_updateExpertiseInfo = useMutation(apiUpdateExpertiseInfo, {
        onSuccess: async (res, variables, context) => {
            const data = await res.json()
            if (res.status > 200) {
                // console.log("Expertise-contract status ", res.status, data);
                handleOpenNotify(data.message, "error")

                return
            }

            handleOpenNotify("Cập nhật thông tin thẩm định thành công!", "success")
            props.reloadPage();
            props.close();

        },
        onError: (error, variables, context) => {
            // console.log("Expertise update info", error)
        }
    })

    return (
        <>
            {isShow ?
                <Modal
                    open={true}
                    onClose={() => { props.close(); }}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style} style={{ color: "black" }}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                            Cập nhật thông tin thẩm định
                        </Typography>

                        <form className="container_create">
                            <>{console.log("car id => ", state.carId)}</>

                            <div className="info w-full">

                                <FormControl sx={{ my: 1 }} size="small" required>
                                    <InputLabel id="demo-select-small">Xe khả dụng</InputLabel>
                                    <Select
                                        labelId="demo-select-small"
                                        id="demo-select-small"
                                        label="Xe khả dụng"
                                        defaultValue={props?.contract?.car?.id}
                                        MenuProps={MenuProps}
                                        onChange={(event) => {
                                            setState({ ...state, carId: event.target.value as string })
                                        }}
                                        style={{ width: "326px" }}
                                    >
                                        {listCar && listCar.length > 0
                                            ?
                                            listCar.map((item, index) => (
                                                <MenuItem
                                                    value={item.id}
                                                    key={index}
                                                    style={{ overflow: 'hidden' }}
                                                >

                                                    <div style={{ display: "flex", alignItems: "center" }}>
                                                        <div
                                                            style={{
                                                                backgroundImage: `url(${process.env.REACT_APP_API}${item.carImage})`, backgroundPosition: "center center",
                                                                width: "80px", height: "50px", backgroundSize: "contain", backgroundRepeat: "no-repeat"
                                                            }}
                                                        ></div>
                                                        <div style={{ textAlign: "left", marginLeft: "5px" }}>
                                                            <h6 style={{ width: "200px", whiteSpace: "nowrap", textOverflow: "ellipsis", overflow: "hidden" }}>Hãng: {item.descriptionInfo.carBrand}</h6>
                                                            <h6 style={{ width: "200px", whiteSpace: "nowrap", textOverflow: "ellipsis", overflow: "hidden" }}>BKS: {item.descriptionInfo.carNumber}</h6>
                                                        </div>
                                                    </div>

                                                </MenuItem>
                                            ))
                                            :
                                            <MenuItem disabled >Không có xe phù hợp</MenuItem>
                                        }
                                    </Select>
                                </FormControl>

                                <DesktopDatePicker
                                    label="Ngày thẩm định"
                                    inputFormat="DD/MM/YYYY"
                                    value={expertiseDate}
                                    onChange={handleChangeExpertiseDate}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            size='small'
                                            sx={{ my: 1 }}
                                        />}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-multiline-flexible"
                                    label='Mô tả'
                                    variant="outlined"
                                    size="small"
                                    multiline
                                    maxRows={3}
                                    onChange={(e) => setState({
                                        ...state,
                                        description: e.target.value.trim()
                                    })}
                                    defaultValue={props.contract?.expertiseContract?.description}
                                />

                                <FormControl sx={{ mt: 2, mb: 1 }} size="small" required>
                                    <InputLabel id="demo-select-small">Kết quả</InputLabel>
                                    <Select
                                        defaultValue={props.contract?.expertiseContract?.result}
                                        labelId="demo-select-small"
                                        id="demo-select-small"
                                        label="Kết quả"
                                        MenuProps={MenuProps}
                                    >
                                        {statusResult?.map((item, index) => (
                                            <MenuItem
                                                value={item.key}
                                                key={index}
                                                onClick={() => setState({ ...state, result: item.key })}
                                            >
                                                <Typography>
                                                    {item.result}
                                                </Typography>
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Kết quả khác"
                                    defaultValue={props.contract?.expertiseContract?.resultOther}
                                    variant="outlined"
                                    size="small"
                                    onChange={(e) => setState({
                                        ...state,
                                        resultOther: e.target.value.trim()
                                    })}
                                />

                                <FormControl sx={{ mt: 2, mb: 1 }} size="small" required>
                                    <InputLabel id="demo-select-small">Độ tin cậy</InputLabel>
                                    <Select
                                        labelId="demo-select-small"
                                        id="demo-select-small"
                                        defaultValue={props.contract?.expertiseContract?.trustLevel}
                                        label="Độ tin cậy"
                                        MenuProps={MenuProps}
                                    >
                                        {trustLevel?.map((item) => (
                                            <MenuItem
                                                value={item}
                                                key={item}
                                                onClick={() => setState({ ...state, trustLevel: item })}
                                            >
                                                <Typography>
                                                    {item}
                                                </Typography>
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-multiline-flexible"
                                    label='Tài sản đặt cọc'
                                    variant="outlined"
                                    size="small"
                                    multiline
                                    maxRows={3}
                                    onChange={(e) => setState({
                                        ...state,
                                        depositInfo: { ...state.depositInfo, asset: e.target.value.trim() }
                                    })}
                                    defaultValue={props.contract?.expertiseContract?.depositInfo?.asset}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-multiline-flexible"
                                    label='Mô tả đặt cọc'
                                    variant="outlined"
                                    size="small"
                                    multiline
                                    maxRows={3}
                                    onChange={(e) => setState({
                                        ...state,
                                        depositInfo: { ...state.depositInfo, description: e.target.value.trim() }
                                    })}
                                    defaultValue={props.contract?.expertiseContract?.depositInfo?.description}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-multiline-flexible"
                                    label='Số tiền đặt cọc'
                                    variant="outlined"
                                    size="small"
                                    type="number"
                                    onChange={(e) => setState({
                                        ...state,
                                        depositInfo: { ...state.depositInfo, downPayment: Number(e.target.value.trim()) }
                                    })}
                                    defaultValue={props.contract?.expertiseContract?.depositInfo?.downPayment ?? 0}
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-multiline-flexible"
                                    label='Số tiền cần thanh toán'
                                    variant="outlined"
                                    size="small"
                                    type="number"
                                    onChange={(e) => setState({
                                        ...state,
                                        paymentAmount: Number(e.target.value)
                                    })}
                                    defaultValue={props.contract?.expertiseContract?.paymentAmount}
                                />

                            </div>

                        </form>

                        <div className="action">

                            <Button
                                size='small'
                                variant="contained"
                                startIcon={<BorderColorIcon />}
                                className="btnCreate"
                                type="submit"
                                onClick={() => { mutation_updateExpertiseInfo.mutate() }}
                            >
                                Cập nhật
                            </Button>
                            <Button
                                size='small'
                                color='success'
                                variant="contained"
                                startIcon={<CloseIcon />}
                                className='btnCancel'
                                onClick={() => { props.close(); }}
                            >
                                Hủy bỏ
                            </Button>
                        </div>

                    </Box>
                </Modal>
                :
                <></>
            }

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>

        </>
    )
}