import AuthContext from 'context/AuthContext';
import { Car } from 'interface/car';
import { useContext, useEffect, useState } from "react";
import { carEndpoints, publicEndpoints } from 'routers/apiEndpoints';
import { Box, FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import './statistical.scss';
import { Moment } from 'moment';
import { Statistic } from 'interface/report/statistic';
import moment from 'moment';

interface BodyGetCar {
    carClass: number,
    registerDescription: {
        carBrand: string,
        seatNumber: number,
        yearCreate: number,
        carColor: string
    },
    pageModel: {
        pageNumber: number,
        pageSize: number
    },
    timeRequirement: {
        from: Moment | null,
        to: Moment | null
    }
}

interface date {
    rentFrom: string,
    rentTo: string
}

export const Statistical = () => {

    const { auth } = useContext(AuthContext);

    const initBody: BodyGetCar = {
        carClass: null,
        registerDescription: {
            carBrand: null,
            carColor: null,
            seatNumber: null,
            yearCreate: null
        },
        pageModel: {
            pageNumber: 1,
            pageSize: 100
        },
        timeRequirement: {
            from: null,
            to: null,
        }
    }

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            },
        },
    };

    const [listCar, setListCar] = useState<Car[]>()
    const [listCarReport, setListCarReport] = useState<Statistic[]>()
    const [idCar, setIdCar] = useState<string>()

    useEffect(() => {
        (async () => {
            const res = await fetch(process.env.REACT_APP_API + carEndpoints.base, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + auth.accessToken,
                },
                body: JSON.stringify(initBody)
            })
            const data = await res.json();
            if (!res.ok) {
                // console.log(`get car status ${res.status} => `, data)
                throw new Error(JSON.stringify(data))
            }
            setListCar(data.data)
        })()
    }, [])

    const years: number[] = Array.from(
        { length: (2022 - moment().year()) / -1 + 1 },
        (_, i) => moment().year() + i * -1
    );

    const month: number[] = Array.from(
        { length: (moment().month()) },
        (_, i) => moment().month() + i * -1
    );

    const getCarReport = async (idCar: string, year: any) => {
        if (!auth) return;
        const res = await fetch(process.env.REACT_APP_API + publicEndpoints.getCarStatistic + "?carId=" + idCar + "&year=" + year, {
            method: "GET",
            headers: {
                // "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
        })
        const data = await res.json()

        if (!res.ok) {
            // console.log(`get car status ${res.status} => `, data)
            throw new Error(JSON.stringify(data))
        }
        setListCarReport(data.data)
    }

    // function countCertainDays(days: number[], d0: any, d1: any) {
    //     var ndays = 1 + Math.round((d1 - d0) / (24 * 3600 * 1000));
    //     var sum = function (a: any, b: any) {
    //         return a + Math.floor((ndays + (d0.getDay() + 6 - b) % 7) / 7);
    //     };
    //     return days.reduce(sum, 0);
    // }

    // function getWeekendCount(startDate: Date, endDate: Date) {
    //     let count = 0;
    //     const curDate = new Date(startDate.getTime());
    //     while (curDate <= endDate) {
    //         const dayOfWeek = curDate.getDay();
    //         if (dayOfWeek === 0 || dayOfWeek === 6) count++;
    //     }
    //     alert(count);
    //     return count;
    // }

    // const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]

    // const getNumberOfWeekDays = (arrayDate: date[], day: Date) => {

    //     const startDay = moment(arrayDate).format("ddd");

    //     const index = days.indexOf(startDay); // index of the start day

    //     const totalDays = moment(end_date).diff(moment(arrayDate?.), "days"); // 16

    //     const numberOfDays = Math.round(totalDays / 7); // 16/7 = 2; number of sessions by week

    //     const remainingDays = totalDays % 7; // 16 % 7 = 2

    //     const newArray = days.rotate(index).slice(0, remainingDays); // ["Thu","Fri"]

    //     if (newArray.includes(day)) {
    //         return numberOfDays + 1;
    //     }

    //     return numberOfDays;
    // };

    function serviceDays(listCar: Statistic[]) {
        var arr: number[] = []
        listCar?.forEach(element => {
            arr.push(element.totalday)
        });
        const sumServiceDays = arr.reduce((accumulator, current) => {
            return accumulator + current;
        }, 0);
        return sumServiceDays + listCar?.length
    }

    function serviceWeekendDays(listCar: Statistic[]) {

        var arrDate: date[] = []
        listCar?.forEach(element => {
            let item: date = {
                rentFrom: new Date(element.rentFrom).toISOString().slice(0, 10),
                rentTo: new Date(element.rentTo).toISOString().slice(0, 10)
            }
            arrDate.push(item)
        })

        return arrDate
    }

    return (
        <>
            {console.log(serviceWeekendDays(listCarReport))}
            {/* {console.log(listCarReport)} */}
            <Box>
                <div className="flex gap-3 flex-nowrap ">

                    <FormControl className="w-full lg:w-[200px]">
                        <InputLabel id="demo-simple-select-label" size="small">
                            Biển kiểm soát
                        </InputLabel>
                        <Select
                            size="small"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Biển kiểm soát"
                            MenuProps={MenuProps}
                            onChange={(e) => {
                                setIdCar(e.target.value.toString())
                            }}
                        >
                            {listCar?.map((item, index) => {
                                return (
                                    <MenuItem
                                        key={index}
                                        value={item.id}
                                    >
                                        {item?.descriptionInfo?.carNumber}
                                    </MenuItem>
                                )
                            })}
                        </Select>
                    </FormControl>

                    <FormControl className="w-full lg:w-[100px]">
                        <InputLabel id="demo-simple-select-label" size="small">
                            Năm
                        </InputLabel>
                        <Select
                            MenuProps={MenuProps}
                            size="small"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Năm"
                            defaultValue={"null"}
                            onChange={(e) => {
                                getCarReport(idCar, e.target.value.toString())
                            }}
                        >
                            <MenuItem key={9} value={"null"}>
                                Tất cả
                            </MenuItem>
                            {
                                years.map((value, index) => {
                                    return (
                                        <MenuItem key={index} value={value}
                                        >
                                            {value}
                                        </MenuItem>
                                    )
                                })
                            }
                        </Select>
                    </FormControl>
                </div>
            </Box>
            <br />
            <hr />
            <br />
            <div className='grid grid-cols-1 md:grid-cols-2 gap-5'>
                <div>
                    <h1 className='text-main font-bold text-[2rem]'>Tháng 1</h1>
                    <hr />
                    <br />
                    <div className='grid grid-cols-1 md:grid-cols-2 gap-3'>
                        <div className='col-span-2 border border-gray-500-100 shadow-lg shadow-indigo-500/40'>
                            <h3 className='bg-main text-white text-[1.4rem] p-2 text-center'>Số ngày chạy</h3>
                            <div className='p-2 flex gap-x-3 justify-center'>
                                <p>Tổng: <strong><span className='text-main text-[1.4rem]'>{serviceDays(listCarReport)} </span>ngày</strong></p>
                                <p>Ngày thường: <strong><span className='text-main text-[1.4rem]'>12 </span>ngày</strong></p>
                                <p>Cuối tuần: <strong><span className='text-main text-[1.4rem]'>4 </span>ngày</strong></p>
                                <p>Ngày lễ: <strong><span className='text-main text-[1.4rem]'>1 </span>ngày</strong></p>
                            </div>
                        </div>
                        <div className='border border-gray-500-100 shadow-lg shadow-indigo-500/40'>
                            <h3 className='bg-main text-white text-[1.4rem] p-2 text-center'>Số Km đã đi</h3>
                            <div className='p-2'>
                                <p className='font-bold text-[2.5rem] text-[#444] text-center'>1500 <span className="text-main text-[1rem]">Km</span></p>
                            </div>
                        </div>
                        <div className='border border-gray-500-100 shadow-lg shadow-indigo-500/40'>
                            <h3 className='bg-main text-white text-[1.4rem] p-2 text-center'>Số etc sử dụng</h3>
                            <div className='p-2'>
                                <p className='font-bold text-[2.5rem] text-[#444] text-center'>1.200.000 <span className="text-main text-[1rem]">Vnđ</span></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <h1 className='text-main font-bold text-[2rem]'>Tháng 2</h1>
                    <hr />
                    <br />
                    <div className='grid grid-cols-1 md:grid-cols-2 gap-3'>
                        <div className='col-span-2 border border-gray-500-100 shadow-lg shadow-indigo-500/40'>
                            <h3 className='bg-main text-white text-[1.4rem] p-2 text-center'>Số ngày chạy</h3>
                            <div className='p-2 flex gap-x-3 justify-center'>
                                <p>Ngày thường: <strong><span className='text-main text-[1.4rem]'>12 ngày</span></strong></p>
                                <p>Ngày cuối tuần: <strong><span className='text-main text-[1.4rem]'>4 ngày</span></strong></p>
                                <p>Ngày lễ: <strong><span className='text-main text-[1.4rem]'>1 ngày</span></strong></p>
                            </div>
                        </div>
                        <div className='border border-gray-500-100 shadow-lg shadow-indigo-500/40'>
                            <h3 className='bg-main text-white text-[1.4rem] p-2 text-center'>Số Km đã đi</h3>
                            <div className='p-2'>
                                <p className='font-bold text-[2.5rem] text-[#444] text-center'>1500 <span className="text-main text-[1rem]">Km</span></p>
                            </div>
                        </div>
                        <div className='border border-gray-500-100 shadow-lg shadow-indigo-500/40'>
                            <h3 className='bg-main text-white text-[1.4rem] p-2 text-center'>Số etc sử dụng</h3>
                            <div className='p-2'>
                                <p className='font-bold text-[2.5rem] text-[#444] text-center'>1.200.000 <span className="text-main text-[1rem]">Vnđ</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Statistical;