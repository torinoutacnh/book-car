import { Outlet } from "react-router-dom";
import Footer from "./footer/Footer";
import Header from "./header/Header";

const Layout = (props: { children: any }) => {
	return (
		<>
			<Header />
			{props.children}
			<Outlet />
			<Footer />
		</>
	);
};

export default Layout;
