import React, { useContext, useState } from "react";
import Login from "components/login/Login";
import { Link, useNavigate } from "react-router-dom";
import "./header.scss";
import HeaderMobi from "./HeaderMobi";
import { adminEndpoints, endpoints } from "routers/endpoints";
import AuthContext from "context/AuthContext";
import Cookies from "js-cookie";
import { RoleUser } from "interface/authenticate";
import { Snackbar, Alert } from "@mui/material";
import { UpdatePassword } from "components/users/updatePassword";

function Header() {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const { auth, LogOutAuth } = useContext(AuthContext)

    const [isShowUpdatePassword, setIsShowUpdatePassword] = useState(false);
    const onClickOpenUpdatePassword = () => setIsShowUpdatePassword(true);
    const onClickCloseUpdatePassword = () => setIsShowUpdatePassword(false);

    return (
        <>
            <header className="bg-white h-[80px] shadow-lg bs relative">
                <div className="header-desktop">
                    <div className="container">
                        <div className="flex justify-between items-center leading-[5rem]">
                            <Link to={"/"}>
                                <img
                                    src="https://apivshare.amazingtech.vn/static/logoVshare.jpg"
                                    alt=""
                                />
                            </Link>

                            <ul className="flex justify-between items-center gap-x-5">
                                <li>
                                    <Link
                                        to={endpoints.home}
                                        className="font-medium hover:text-main cursor-pointer"
                                    >
                                        Trang chủ
                                    </Link>
                                </li>
                                <li>
                                    <Link
                                        to={endpoints.rent_car}
                                        className="font-medium hover:text-main cursor-pointer"
                                    >
                                        Thuê xe
                                    </Link>
                                </li>

                                {Number(RoleUser[auth?.userProfile?.role]) !== Number(RoleUser["Customer"]) && auth &&
                                    <li>
                                        <Link
                                            to={adminEndpoints.base}
                                            className="font-medium hover:text-main cursor-pointer"
                                        >
                                            Quản trị
                                        </Link>
                                    </li>
                                }

                                {
                                    auth &&
                                    <li className="list__menu">
                                        <div className="dropdown">
                                            <span className="font-medium hover:text-main cursor-pointer">
                                                Quản lý
                                                <i className="fa-solid fa-chevron-down text-[0.7rem] pl-1"></i>
                                            </span>
                                            <ul className="dropdown-content leading-[2rem]">
                                                <li className="hover:bg-[#edede9]">
                                                    <Link to="/quan-ly-don-hang" className="ml-5">
                                                        Quản lý đơn hàng
                                                    </Link>
                                                </li>
                                                <li className="hover:bg-[#edede9]">
                                                    <Link to="/quan-ly-hop-dong" className="ml-5">
                                                        Quản lý hợp đồng
                                                    </Link>
                                                </li>
                                                <li className="hover:bg-[#edede9]">
                                                    <Link to="/user" className="ml-5">
                                                        Hồ sơ cá nhân
                                                    </Link>
                                                </li>
                                                <hr />
                                                <li className="hover:bg-[#edede9]">
                                                    <Link to="" className="ml-5"
                                                        onClick={() => {
                                                            onClickOpenUpdatePassword()
                                                        }}
                                                    >
                                                        Đổi mật khẩu
                                                    </Link>
                                                </li>
                                                {auth && <li className="hover:bg-[#edede9]">
                                                    <a href={endpoints.home} className="ml-5" onClick={() => { LogOutAuth() }}>Đăng xuất</a>
                                                </li>}
                                            </ul>
                                        </div>
                                    </li>
                                }


                                <li>
                                    <Link
                                        to={"/"}
                                        className="text-[16px] text-main font-medium ml-[30px] border-[1.8px] border-main px-[25px] py-[6px] rounded-[5px] hover:opacity-90"
                                    >
                                        <i className="fa-solid fa-phone mr-3 text-[15px]"></i>
                                        <span>Hotline</span>
                                    </Link>
                                </li>
                                <li>
                                    <Login />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="header-mobi">
                    <Link to={"/"} className="text-3xl font-bold pr-[30px]">
                        <img
                            src="https://apivshare.amazingtech.vn/static/logoVshare.jpg"
                            alt=""
                            className="d-unset absolute pl-[20px] top-[16px]"
                        />
                    </Link>
                    <HeaderMobi />
                </div>
            </header>

            <UpdatePassword
                close={onClickCloseUpdatePassword}
                stateProps={isShowUpdatePassword}
                id={auth?.userProfile?.id}
            />


        </>

    );
}

export default Header;
