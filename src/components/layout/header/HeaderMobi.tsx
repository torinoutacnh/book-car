import React, { useContext, useState } from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import Login from "components/login/Login";
import { Link, useNavigate } from "react-router-dom";
import AuthContext from "context/AuthContext";
import { adminEndpoints, endpoints } from "routers/endpoints";

type Anchor = "top";

export default function HeaderMobi() {
    const [state, setState] = React.useState({
        top: false,
    });

    const toggleDrawer =
        (anchor: Anchor, open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
            if (
                event.type === "keydown" &&
                ((event as React.KeyboardEvent).key === "Tab" ||
                    (event as React.KeyboardEvent).key === "Shift")
            ) {
                return;
            }

            setState({ ...state, [anchor]: open });
        };

    const navigate = useNavigate()
    const { auth, LogOutAuth } = useContext(AuthContext)

    const list = (anchor: Anchor) => (
        <Box
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <div className="p-5 leading-5">
                <List className="flex justify-between items-center">
                    <div className="text-center mt-3"><Login /></div>
                    <div className="text-right text-[18px]">
                        <i className="fa-solid fa-xmark"></i>
                    </div>
                </List>
                <List>
                    <Link to="/" className="block">
                        <i className="inline-block w-[50px] fa-sharp fa-solid fa-house text-[15px] text-main"></i>
                        <span className="font-bold">Trang chủ</span>
                    </Link>
                </List>
                <List>
                    <Link to="/thue-xe" className="block">
                        <i className="inline-block w-[50px] fa-sharp fa-solid fa-car text-[15px] text-main"></i>
                        <span className="font-bold">Thuê Xe</span>
                    </Link>
                </List>
                <List>
                    <Link to={adminEndpoints.base} className="block">
                        <i className="inline-block w-[50px] fa-solid fa-people-roof text-[15px] text-main"></i>
                        <span className="font-bold">Quản trị</span>
                    </Link>
                </List>
                <List>
                    <Link to="/user" className="block">
                        <i className="inline-block w-[50px] fa-solid fa-user text-[15px] text-main"></i>
                        <span className="font-bold">Quản lý hồ sơ</span>
                    </Link>
                </List>
            </div>
            <Divider />
            <div className="p-5 leading-5">
                <List>
                    <Link to="/" className="block">
                        <i className="inline-block w-[50px] fa-solid fa-lock text-[15px] text-main"></i>
                        <span className="font-bold">Chính Sách Bảo Mật</span>
                    </Link>
                </List>
                <List>
                    <Link to="/" className="block">
                        <i className="inline-block w-[50px] fa-sharp fa-solid fa-credit-card text-[15px] text-main"></i>
                        <span className="font-bold">Chính Sách Thanh Toán</span>
                    </Link>
                </List>
                <List>
                    <Link to="/" className="block">
                        <i className="inline-block w-[50px] fa-solid fa-question text-[15px] text-main"></i>
                        <span className="font-bold">Câu Hỏi Thường Gặp</span>
                    </Link>
                </List>
                <List>
                    <Link to="/" className="block">
                        <i className="inline-block w-[50px] fa-solid fa-headset text-[15px] text-main"></i>
                        <span className="font-bold">Hỗ Trợ</span>
                    </Link>
                </List>
            </div>
            <Divider />
            <List>
                {auth && <li className="text-center my-3">
                    <a
                        href={endpoints.home}
                        onClick={() => { LogOutAuth() }}
                        className="text-[#fff] font-bold bg-[#ef233c] px-3 py-2"
                    >
                        Đăng xuất
                    </a>
                </li>}
            </List>
        </Box>
    );

    return (
        <div className="info-mobi">
            {(["top"] as const).map((anchor) => (
                <React.Fragment key={anchor}>
                    <Button onClick={toggleDrawer(anchor, true)}>{anchor}</Button>
                    <Drawer
                        anchor={anchor}
                        open={state[anchor]}
                        onClose={toggleDrawer(anchor, false)}
                    >
                        {list(anchor)}
                    </Drawer>
                </React.Fragment>
            ))}
        </div>
    );
}
