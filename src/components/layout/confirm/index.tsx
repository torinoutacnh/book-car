import { Button, Modal, Box, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import CloseIcon from '@mui/icons-material/Close';
import CheckIcon from '@mui/icons-material/Check';
import "../../admin/styles/crud.scss"

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export const Confirm = (props: { title?: string, function?: any, stateOpen?: boolean, close?: any }) => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(props.stateOpen)
  }, [props.stateOpen])

  return (
    <div>

      <Modal
        open={open}
        onClose={() => props.close()}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} style={{ border: "none", borderRadius: "5px" }}>
          <Typography
            style={{ textAlign: "center" }}
            id="modal-modal-title" variant="h5" component="h5">
            {props.title}
          </Typography>

          <div className="action">
            <Button
              style={{ whiteSpace: "nowrap" }}
              size='small'
              color='success'
              variant="contained"
              startIcon={<CloseIcon />}
              className='btnCancel'
              onClick={() => { props.close() }}
            >
              Hủy bỏ
            </Button>

            <Button
              style={{ whiteSpace: "nowrap" }}
              size='small'
              variant="contained"
              startIcon={<CheckIcon />}
              className="btnCreate"
              onClick={() => {
                props.function()
                props.close()
              }}
            >
              Xác nhận
            </Button>



          </div>

        </Box>
      </Modal>

    </div>
  );
}