import React from "react";
import { Link } from "react-router-dom";

function Footer() {
    return (
        <footer className="bg-white">
            <div className="text-center mb-5">Power by Amazingtech</div>
        </footer>
    );
}

export default Footer;
