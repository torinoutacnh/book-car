import { Modal, Box, Typography, IconButton, Button } from "@mui/material";
import { Loading } from "components/loading/loading";
import path from "node:path/win32";
import { useContext, useEffect, useState } from "react";
import { useQuery } from "react-query";
import CloseIcon from '@mui/icons-material/Close';
import AuthContext from "context/AuthContext";


export interface BodyPreviewRent {
    rentFrom: string,
    rentTo: string,
    downPayment: number
}

export interface BodyPreviewTranfer {
    dateTransfer: string,
    carState: {
        speedometerNumber: number,
        fuelPercent: number,
        currentEtcAmount: number,
        carStatusDescription: string
    },
    depositItem: {
        paper: string,
        standbyDevice: string,
        standbyDeviceInfo: string
    }
}

export interface BodyPreviewReceive {
    dateReceive: string,
    currentCarState: {
        speedometerNumber: number,
        fuelPercent: number,
        currentEtcAmount: number,
        carStatusDescription: string
    }
}


interface Props {
    stateProps: boolean,
    close: any,
    pathApi: string,
    idGroup: string,
    data: BodyPreviewReceive | BodyPreviewRent | BodyPreviewTranfer,
}

export const WordContractPreview = (props: Props) => {

    const { auth } = useContext(AuthContext)
    const [isShow, setIsShow] = useState(false)
    useEffect(() => {
        setIsShow(props?.stateProps)
        if (props.stateProps) {
            refetch()
        }
    }, [props.stateProps])

    const getApi = async (): Promise<string> => {
        const res = await fetch(process.env.REACT_APP_API + props?.pathApi + props?.idGroup, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(props?.data),
        })

        const data = await res.text()

        if (!res.ok) {
            throw new Error("error")
        }

        return data
    }

    const { data: html, isLoading, isError, refetch } = useQuery({
        queryKey: ["WordContractPreview"],
        queryFn: () => getApi(),
        retry: false
    })

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        borderRadius: '10px',
        textAlign: 'center',

    };


    return (
        <>
            <Modal
                open={isShow}
                onClose={() => {
                    props.close();
                }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <>
                    {
                        isLoading && <Loading />
                    }
                    {
                        isError && props?.close()
                    }
                    {
                        html &&

                        <Box sx={style}>
                            <Button
                                onClick={() => { props?.close() }}
                                color="error"
                                size="large"
                            >
                                <CloseIcon style={{ fontSize: "30px" }} />
                            </Button>
                            <iframe
                                style={{
                                    width: "90vw", height: "85vh", border: "none",
                                    margin: "0", padding: "0", borderRadius: "10px", overflow: "hidden"
                                }}
                                srcDoc={html}
                            />
                        </Box>
                    }
                </>
            </Modal>
        </>
    );
}
