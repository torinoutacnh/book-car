import { Modal, Box, Typography, Button } from "@mui/material";
import { useEffect, useState } from "react";
import CloseIcon from '@mui/icons-material/Close';


export const WordContractPath = (props: { stateProps?: boolean, close?: any, filePath?: string }) => {

    const [isShow, setIsShow] = useState(false)
    const [path, setPath] = useState("")

    useEffect(() => {
        setIsShow(props?.stateProps)
        setPath(props?.filePath)
    }, [props?.stateProps])

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 24,
        borderRadius: '10px',
        textAlign: 'center',
    };


    return (
        <>
            <Modal
                open={isShow}
                onClose={() => {
                    props.close();
                }}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >

                <Box sx={style}>

                    <Button
                        onClick={() => { props?.close() }}
                        color="error"
                        size="large"
                    >
                        <CloseIcon style={{ fontSize: "30px" }} />
                    </Button>

                    <iframe
                        src={`https://view.officeapps.live.com/op/embed.aspx?src=${encodeURI(
                            process.env.REACT_APP_API + path
                        )}`}
                        frameBorder="0"
                        style={{
                            width: "90vw", height: "85vh", border: "none",
                            margin: "0", padding: "0", borderRadius: "10px", overflow: "hidden"
                        }}
                    >
                        This is an embedded{" "}
                        <a target="_blank" href="http://office.com">
                            Microsoft Office
                        </a>{" "}
                        document, powered by{" "}
                        <a target="_blank" href="http://office.com/webapps">
                            Office Online
                        </a>
                        .
                    </iframe>
                </Box>

            </Modal>

        </>
    );
}
