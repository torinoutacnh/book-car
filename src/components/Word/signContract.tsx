import React, { useContext, useState, useEffect } from "react";
import SignaturePad from "react-signature-canvas";
import SignatureCanvas from "react-signature-canvas";
import CreateIcon from "@mui/icons-material/Create";
import { Alert, Button, Snackbar } from "@mui/material";
import ClearIcon from "@mui/icons-material/Clear";
import DoneIcon from "@mui/icons-material/Done";
import "./index.scss";
import AuthContext from "context/AuthContext";
import { User } from "interface/authenticate";
import { Contract } from "interface/contract";
import DocViewer, { DocViewerRenderers } from "react-doc-viewer";
import { contractGroupEndpoints } from "routers/apiEndpoints";

export const SignContractComponent = (props: {
  userId?: string;
  Id?: string;
  reloadPage?: any;
  handleOpenNotify?: any;
  role: number;
}) => {
  const { auth } = useContext(AuthContext);
  const [dataURL, setDataURL] = React.useState<string | null>(null);
  const [ImageFile, setImageFile] = React.useState<File>(null);
  const [contract, setContract] = useState<Contract>();

  const padRef = React.useRef<SignatureCanvas>(null);

  const clear = () => {
    padRef.current?.clear();
    setDataURL(null);
    setImageFile(null);
  };

  const trim = async () => {
    const dataUrl = padRef.current?.getTrimmedCanvas().toDataURL("image/png");

    // if (url) setDataURL(url);

    const blob = await (await fetch(dataUrl)).blob();
    const file = new File([blob], "fileName.jpg", {
      type: "image/jpeg",
      lastModified: 1,
    });

    setDataURL(URL.createObjectURL(file));
    setImageFile(file);

    // const image: HTMLImageElement = document.querySelector("#testImage")
    // image.src = URL.createObjectURL(file)

    handleSubmitSignContract(file);
  };

  const handleSubmitSignContract = async (file?: File) => {
    if (!auth) {
      // console.log("auth null");
      return;
    }
    // if (!ImageFile) {
    //     console.log("ImageFile null");
    //     return
    // }

    const formData = new FormData();
    formData.append("Id", props.Id);
    formData.append("Path", file);

    const rolePath = props?.role === 0 ? contractGroupEndpoints.customerSign : contractGroupEndpoints.staffSign

    const res = await fetch(
      process.env.REACT_APP_API + rolePath,
      {
        method: "POST",
        headers: {
          // "Content-Type": "application/json",
          Authorization: "Bearer " + auth.accessToken,
          userId: props?.userId,
        },
        body: formData,
      }
    );

    const data = await res.json();

    if (res.status > 200) {
      // console.log("sign-contract status ", res.status, " => ", data);
      return;
    }

    props.handleOpenNotify("Đã ký tên", "success");
    // console.log("sign-contract status 200", data);
    props.reloadPage();
  };

  // useEffect(() => {
  //   if (!auth) {
  //     return;
  //   }

  //   (async () => {
  //     const res = await fetch(
  //       process.env.REACT_APP_API +
  //       "/contractgroup/" +
  //       "ea4db96e-12e3-4051-8930-2594d1dee70f",
  //       {
  //         method: "GET",
  //         headers: {
  //           "Content-Type": "application/json",
  //           Authorization: "Bearer " + auth.accessToken,
  //         },
  //       }
  //     );

  //     const data = await res.json();


  //     if (res.status > 200) {
  //       console.log("sign-contract status ", res.status, " => ", data);
  //       return;
  //     }

  //     console.log("getContract", data.data);
  //     setContract(data.data);
  //   })();
  // }, []);

  // const [fileData, setFileData] = useState<string | ArrayBuffer>();
  // const [blob, setBlob] = useState<Blob>();
  // const generateWord = async () => {
  //   if (contract && ImageFile) {
  //     const blobTmp = await CreateDataWord(ImageFile, contract);
  //     setBlob(blobTmp);
  //     console.log("URL", URL.createObjectURL(blobTmp));

  //     // const reader = new FileReader();
  //     // reader.addEventListener("loadend", () => {
  //     //     console.log("reader result", reader.result);
  //     //     setFileData(reader.result);
  //     // });
  //     // reader.readAsDataURL(blobTmp);
  //   }
  // };

  // const docs = [{ uri: require(URL.createObjectURL(blob)) }];

  /////////////////////////////////////////////////////////

  return (
    <>
      <div className="container_signContract">
        <span style={{ fontSize: "15px", fontWeight: "700" }}>Chữ ký {props?.role === 0 ? "Khách hàng" : "Nhân viên"}</span>
        <div className="Wrapper">
          <SignaturePad
            ref={padRef}
            canvasProps={{ className: "SignaturePad" }}
          />

          <div className="action">
            <Button
              startIcon={<CreateIcon />}
              onClick={() => {
                trim();
              }}
            >
              Xác nhận
            </Button>

            <Button
              color="error"
              startIcon={<ClearIcon />}
              onClick={() => {
                clear();
              }}
            >
              Ký lại
            </Button>
          </div>
          {/* <img
                        className="SignaturePad"
                        src={dataURL}
                        alt="user generated signature"
                    /> */}

          {/* <div>
                        <Button
                            startIcon={<DoneIcon />}
                            onClick={() => { handleSubmitSignContract() }}
                        >
                            Xác nhận
                        </Button>
                    </div> */}
        </div>

        {/* {contract && ImageFile ? (
          <>
            <button
              style={{
                border: "1px solid black",
                borderRadius: "3px",
                margin: "20px",
              }}
              onClick={() => {
                generateWord();
              }}
            >
              Generate DOCX
            </button>
          </>
        ) : null} */}

        {/* <img
                    id="testImage"
                    className={"sigImage"}
                    // src={dataURL}
                    alt="user generated signature"
                /> */}

        {/* {blob && (
          <>
            {console.log("URLLL", URL.createObjectURL(blob))}
            <iframe
              src={URL.createObjectURL(blob)}
              style={{ width: "500px", height: "500px" }}
            />
            <DocViewer
              pluginRenderers={DocViewerRenderers}
              documents={[
                {
                  uri: "https://docs.google.com/document/d/15t7zjAGCd8nm8eB98KayPDhs8b0DN_cr/edit?rtpof=true",
                },
              ]}
              style={{ background: "red" }}
            />
          </>
        )} */}
      </div>
    </>
  );
};
