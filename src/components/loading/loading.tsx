import "./loading.scss"
export const Loading = () => {
    return (
        <div className="container_loading">
            <div className="loader">
                <div>
                    <div className="outer"></div>
                    <div className="middle"></div>
                    <div className="inner"></div>
                </div>
            </div>
        </div>
    )
}