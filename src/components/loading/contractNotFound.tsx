import './loading.scss'
import ContractNotFoundGif from "../../assets/contract-not-found.gif"

export const FilterNotFound = (props: {title: string}) => {
    return (
        <div className='ContractNotFound'>
            <div className='text-center text-[1.2rem] text-[#999]'>
                {props.title}
            </div>
            <div className='w-[300px]'>
                <img src={ContractNotFoundGif} alt="" />
            </div>
        </div>
    );
}