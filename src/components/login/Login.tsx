import AuthContext from "context/AuthContext";
import { RoleUser, VieRole } from "interface/authenticate";
import { useContext } from "react";
import { GoogleLogin, GoogleLoginResponse, GoogleLoginResponseOffline } from "react-google-login";
import { useNavigate } from "react-router-dom";
import { endpoints } from "routers/endpoints";

function Login() {
    const { onSuccess, auth } = useContext(AuthContext);
    const navigate = useNavigate()

    return (
        <>
            {auth ? (
                <>
                    <div className="leading-[25px] flex align-middle">
                        <p className=" justify-start items-center lg:block lg:text-center font-bold text-[#f0c808]"> {auth.userProfile.name}
                            <p className="text-center italic text-sm ml-2 lg:ml-0 text-[#333]">({VieRole[Number(RoleUser[auth.userProfile.role])]})</p>
                        </p>

                    </div>
                </>
            ) : (
                // <div id="signInButton">
                //     <GoogleLogin
                //         clientId={process.env.REACT_APP_CLIENT_ID || ""}
                //         buttonText="Đăng nhập"
                //         onSuccess={(res) => onSuccess(res)}
                //         onFailure={(err: any) => {
                //             // console.log
                //         }}
                //         cookiePolicy={"single_host_origin"}
                //         isSignedIn={false}
                //     />
                // </div>
                <button style={{ fontWeight: "600", fontFamily: "sans-serif", textTransform: "uppercase" }} onClick={() => { navigate(endpoints.loginPage) }}>Đăng nhập</button>
            )}
        </>
    );
}

export default Login;
