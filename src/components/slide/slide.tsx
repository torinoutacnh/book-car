import React from "react";
import Slider from "react-slick";
import "./slide.scss";

export default function SlideItemsFist() {
    var settings = {
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        initialSlide: 0,
        arrows: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };
    return (
        <div>
            <Slider {...settings}>
                <div className="pr-5">
                    <img
                        src="https://cdnimg.vietnamplus.vn/uploaded/qrndqxjwp/2022_01_15/5c13378d057c480a8e98c64a413e20613682161636517973.jpg"
                        alt=""
                        className="rounded-t-lg w-[275px] h-[175px]"
                    />
                    <h3 className="text-center py-2 text-[1rem] font-bold bg-main text-white max-w-[275px] rounded-b-md">
                        MG
                    </h3>
                </div>
                <div className="pr-5">
                    <img
                        src="https://hondaotophattien.com.vn/vnt_upload/news/03_2021/Bien-so-xe-oto_1.jpg"
                        alt=""
                        className="rounded-t-lg w-[275px] h-[175px]"
                    />
                    <h3 className="text-center py-2 text-[1rem] font-bold bg-main text-white max-w-[275px] rounded-b-md">
                        Honda
                    </h3>
                </div>
                <div className="pr-5">
                    <img
                        src="https://static.chotot.com/storage/chotot-kinhnghiem/xe/2021/12/aa7970a9-o-to-1.webp"
                        alt=""
                        className="rounded-t-lg w-[275px] h-[175px]"
                    />
                    <h3 className="text-center py-2 text-[1rem] font-bold bg-main text-white max-w-[275px] rounded-b-md">
                        Mazda
                    </h3>
                </div>
                <div className="pr-5">
                    <img
                        src="https://vcdn1-vnexpress.vnecdn.net/2022/04/21/BMW-i7-1-2574-1650510542.jpg?w=0&h=0&q=100&dpr=2&fit=crop&s=1QrbxeTX7pHBTAC01pF5Nw"
                        alt=""
                        className="rounded-t-lg w-[275px] h-[175px]"
                    />
                    <h3 className="text-center py-2 text-[1rem] font-bold bg-main text-white max-w-[275px] rounded-b-md">
                        BMW
                    </h3>
                </div>
                <div className="pr-5">
                    <img
                        src="https://storage.googleapis.com/f1-cms/2020/09/2dba5efe-20200912_042658.jpg"
                        alt=""
                        className="rounded-t-lg w-[275px] h-[175px]"
                    />
                    <h3 className="text-center py-2 text-[1rem] font-bold bg-main text-white max-w-[275px] rounded-b-md">
                        Suzuki
                    </h3>
                </div>
                <div className="pr-5">
                    <img
                        src="https://danviet.mediacdn.vn/296231569849192448/2021/11/2/base64-1635823343654760663315.png"
                        alt=""
                        className="rounded-t-lg w-[275px] h-[175px]"
                    />
                    <h3 className="text-center py-2 text-[1rem] font-bold bg-main text-white max-w-[275px] rounded-b-md">
                        Vinfast
                    </h3>
                </div>
            </Slider>
        </div>
    );
}
