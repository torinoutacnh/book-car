import { GoogleLogin, GoogleLoginResponse, GoogleLoginResponseOffline } from "react-google-login";
import React, { FormEvent, useContext, useEffect, useState } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import "./index.scss";
import ImgLogin from "../../assets/login.svg";
import AuthContext from "context/AuthContext";
import { adminEndpoints, endpoints } from "routers/endpoints";
import { Link, useNavigate } from "react-router-dom";
import { Alert, Snackbar } from "@mui/material";

interface LoginUser {
    userName: string;
    passWord: string;
}

function LoginPage() {
    const { loginAuth } = useContext(AuthContext);

    const navigate = useNavigate();

    const { onSuccess, auth } = useContext(AuthContext);

    const [isRemember, setIsRemember] = useState(false)

    const [typeNotifi, setTypeNotifi] = useState("success");
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("");
    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === "clickaway") {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type);
        setMessageNotify(message);
        setOpenNofity(true);
    };


    const [loginUser, setLoginUser] = useState<LoginUser>();

    useEffect(() => {
        const account_remember: LoginUser = JSON.parse(localStorage.getItem("VShareAccount"))
        if (account_remember) {
            setLoginUser(account_remember)
            setIsRemember(true)
        }
    }, [])

    const handleSubmitLogin = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const res = await fetch(process.env.REACT_APP_API + "/user/authenticate", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                // Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify(loginUser),
        });
        const data = await res.json();
        // console.log("data login => ", data)
        if (!res.ok) {
            handleOpenNotify(data.message, "error")
            return;
        }



        if (isRemember) {
            localStorage.setItem("VShareAccount", JSON.stringify(loginUser))
        }
        else {
            localStorage.removeItem("VShareAccount")
        }

        loginAuth(data)
        navigate("/")
    };

    return (
        <div className="my-[50px] login-page">
            <div className="container">
                <div className="grid grid-cols-1 lg:grid-cols-3">
                    <div className="col-span-2 hidden lg:block">
                        <img src={ImgLogin} alt="" />
                    </div>
                    <div className="col-span-1 px-[20px] flex items-center">
                        <Box
                            component="form"
                            noValidate
                            autoComplete="off"
                            onSubmit={handleSubmitLogin}
                        >
                            <h1 className="text-[1.6rem] font-bold text-center">ĐĂNG NHẬP</h1>
                            <TextField
                                id="standard-basic"
                                label="Tài khoản/Email"
                                variant="standard"
                                className="w-full"
                                name="userName"
                                value={loginUser?.userName}
                                onChange={(e) =>
                                    setLoginUser({ ...loginUser, userName: e.target.value })
                                }
                            />
                            <TextField
                                id="standard-password-input"
                                label="Password"
                                type="password"
                                value={loginUser?.passWord}
                                variant="standard"
                                className="w-full"
                                name="passWord"
                                onChange={(e) =>
                                    setLoginUser({ ...loginUser, passWord: e.target.value })
                                }
                            />
                            <div className="cursor-pointer">
                                <label>
                                    <input type="checkbox" checked={isRemember} onChange={(e) => { setIsRemember(e.target.checked) }} />
                                    <span className="ml-2">Ghi nhớ đăng nhập</span>
                                </label>
                            </div>
                            <div className="text-center">
                                <div style={{ display: "flex", justifyContent: "space-evenly" }} className="mt-[30px] gap-3">
                                    <input
                                        type="submit"
                                        value="Đăng Nhập"
                                        className="bg-sky-500 hover:op rounded-md py-2 text-white cursor-pointer"
                                        style={{ width: "50%" }}
                                    />
                                    <GoogleLogin
                                        className="w-[160px] radius-login"
                                        clientId={process.env.REACT_APP_CLIENT_ID || ""}
                                        buttonText="Google"
                                        onSuccess={(res) => onSuccess(res)}
                                        onFailure={(err: any) => {
                                            // console.log
                                        }}
                                        cookiePolicy={"single_host_origin"}
                                        isSignedIn={false}
                                    />
                                </div>
                                <div className="text-center cursor-pointer">
                                    <Link to={"/" + endpoints.registerPage} className="bg-green-500 hover:bg-green-700 rounded-md w-full mt-[30px] py-2 text-white block">Đăng ký</Link>
                                </div>
                            </div>


                            <div className="text-sky-500 underline text-center mt-[30px]">
                                <Link to="#">Quên mật khẩu ?</Link>
                            </div>
                        </Box>

                    </div>
                </div>
            </div>

            {/* snackbar */}
            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {typeNotifi === "success" ? (
                    <Alert
                        color={"info"}
                        onClose={handleCloseNotify}
                        severity={"success"}
                        sx={{ width: "100%" }}
                    >
                        {messageNotify}
                    </Alert>
                ) : (
                    <Alert
                        color={"error"}
                        onClose={handleCloseNotify}
                        severity={"error"}
                        sx={{ width: "100%" }}
                    >
                        {messageNotify}
                    </Alert>
                )}
            </Snackbar>
        </div>
    );
}

export default LoginPage;
