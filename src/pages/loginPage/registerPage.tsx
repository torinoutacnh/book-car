import { Alert, Box, Button, FormControlLabel, Radio, RadioGroup, Snackbar } from "@mui/material";
import { useState, useContext } from "react";
import "./index.scss";
import TextField from "@mui/material/TextField";
import AddIcon from "@mui/icons-material/Add";
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import ImgRegister from '../../assets/register.svg';
import { userEndpoints } from "routers/apiEndpoints";
import { endpoints } from "routers/endpoints";
import { useNavigate } from "react-router-dom";

function RegisterPage() {

    const navigate = useNavigate()
    const [typeNotifi, setTypeNotifi] = useState("success");
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("");

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === "clickaway") {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type);
        setMessageNotify(message);
        setOpenNofity(true);
    };

    const [dateReceiveCCCD, setDateReceiveCCCD] = useState<Moment | null>(moment());
    const [dateReceivePassPort, setDateReceivePassPort] = useState<Moment | null>(moment());
    const [dateReceiveGPLX, setDateReceiveGPLX] = useState<Moment | null>(moment());

    const handleChangeDateCCCD = (newValue: Moment | null) => {
        setDateReceiveCCCD(newValue);
    };

    const handleChangeDatePassPort = (newValue: Moment | null) => {
        setDateReceivePassPort(newValue);
    };

    const handleChangeDateGPLX = (newValue: Moment | null) => {
        setDateReceiveGPLX(newValue);
    };

    const [valid, setValid] = useState(false);

    const initValue = {
        typeInfo: "citizenIdentificationInfo",
        name: "",
        tel: "",
        job: "",
        currentAddress: "",
        email: "",
        passWord: "",
        numberCCCD: "",
        addressCCCD: "",
        numberPassPort: "",
        addressPassPort: "",
        numberGPLX: "",
    };

    const [state, setState] = useState(initValue);

    const handleSubmit = async () => {

        const isNull = { a: false };

        const tmp1 = [
            state.name,
            state.tel,
            state.job,
            state.currentAddress,
            state.email,
            state.numberGPLX,
        ];
        tmp1.map((item) => {
            if (item.length === 0) {
                isNull.a = true;
                return;
            }
        });

        const tmp2 =
            state.typeInfo === "passportInfo"
                ? [state.numberPassPort, state.addressPassPort]
                : [state.numberCCCD, state.addressCCCD];
        tmp2.map((item) => {
            if (item.length === 0) {
                isNull.a = true;
                return;
            }
        });

        if (isNull.a) {
            handleOpenNotify("Vui lòng nhập đầy đủ thông tin!", "error");
            return;
        }

        if (state.passWord.length < 8 || state.passWord.length > 20) {
            handleOpenNotify("Mật khẩu từ 8 đến 20 ký tự!", "error");
            return;
        }

        const dataBody = {
            name: state.name,
            tel: state.tel,
            job: state.job,
            currentAddress: state.currentAddress,
            email: state.email,
            passWord: state.passWord,
            citizenIdentificationInfo: {
                number: state.numberCCCD,
                address: state.addressCCCD,
                dateReceive: dateReceiveCCCD,
            },
            passportInfo: {
                number: state.numberPassPort,
                address: state.addressPassPort,
                dateReceive: dateReceivePassPort,
            },
            drivingLicenseInfo: {
                number: state.numberGPLX,
                dateReceive: dateReceiveGPLX,
            },
        };

        const res = await fetch(process.env.REACT_APP_API + userEndpoints.createUser, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(dataBody),
        })

        const data = await res.json()

        if (!res.ok) {
            handleOpenNotify(data.message, "error")
            return
        }
        handleOpenNotify("Tạo tài khoản thành công!", "success")
        setTimeout(() => { navigate("/" + endpoints.loginPage) }, 700)
    };

    return (
        <div className="my-[20px]">
            <div className="container">
                <div className="register-page">
                    <div className="grid grid-cols-1 lg:grid-cols-5 gap-x-3">
                        <div className="col-span-2">
                            <Box className="bg-white rounded-md bs px-5 py-2">
                                <h1 className="text-main text-[1.6rem] font-bold text-center capitalize">Đăng ký tài khoản người dùng</h1>

                                <TextField
                                    id="outlined-basic"
                                    label="Tên"
                                    variant="standard"
                                    size="small"
                                    className="w-full"
                                    required
                                    onChange={(e) => setState({ ...state, name: e.target.value, })}
                                />

                                <div className="grid grid-cols-1 lg:grid-cols-2 gap-x-3 mt-[10px]">
                                    <TextField
                                        id="outlined-basic"
                                        label="Số điện thoại"
                                        variant="standard"
                                        size="small"
                                        className="w-full"
                                        required
                                        onChange={(e) => setState({ ...state, tel: e.target.value, })}
                                    />

                                    <TextField
                                        id="outlined-basic"
                                        label="Công việc"
                                        variant="standard"
                                        size="small"
                                        className="w-full"
                                        required
                                        onChange={(e) => setState({ ...state, job: e.target.value, })}
                                    />
                                </div>

                                <div className="mt-[10px]">
                                    <TextField
                                        id="outlined-basic"
                                        label="Địa chỉ hiện tại"
                                        variant="standard"
                                        size="small"
                                        className="w-full"
                                        required
                                        onChange={(e) => setState({ ...state, currentAddress: e.target.value, })}
                                    />
                                </div>

                                <div className="mt-[10px]">
                                    <TextField
                                        type="email"
                                        id="outlined-basic"
                                        label="Email"
                                        className="w-full"
                                        variant="standard"
                                        // inputProps={{ pattern: "[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$" }}
                                        size="small"
                                        required
                                        // onChange={(e) => {
                                        //     setState({ ...state, email: e.target.value });
                                        //     const reg = new RegExp("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$");
                                        //     setValid(reg.test(e.target.value));
                                        // }}
                                        onChange={(e) => setState({ ...state, email: e.target.value, })}
                                    />
                                </div>

                                <div className="mt-[10px]">
                                    <TextField
                                        id="outlined-basic"
                                        label="Mật khẩu"
                                        variant="standard"
                                        size="small"
                                        className="w-full"
                                        required
                                        onChange={(e) => setState({ ...state, passWord: e.target.value, })}
                                    />
                                </div>

                                <div className="mt-[10px]">
                                    <RadioGroup
                                        row
                                        className="w-full"
                                        aria-labelledby="demo-radio-buttons-group-label"
                                        defaultValue={state.typeInfo}
                                        name="radio-buttons-group"
                                        onChange={(e) => { setState({ ...state, typeInfo: e.target.value }) }}
                                    >
                                        <FormControlLabel value="citizenIdentificationInfo" control={<Radio />} label="CCCD" />
                                        <FormControlLabel value="passportInfo" control={<Radio />} label="Passport" />
                                    </RadioGroup>
                                </div>

                                <div className="">
                                    <TextField
                                        type="text"
                                        id="outlined-basic"
                                        label={`Số ${state.typeInfo === "passportInfo" ? "Passport" : "CCCD"}`}
                                        variant="standard"
                                        size="small"
                                        className="w-full"
                                        required
                                        onChange={(e) => {
                                            state.typeInfo === "passportInfo"
                                                ? setState({ ...state, numberPassPort: e.target.value })
                                                : setState({ ...state, numberCCCD: e.target.value })
                                        }}
                                    />
                                </div>

                                <div className="grid grid-cols-1 lg:grid-cols-2 gap-x-3 mt-5">
                                    <TextField
                                        id="outlined-basic"
                                        label="Nơi cấp"
                                        variant="standard"
                                        size="small"
                                        className="w-full"
                                        required
                                        onChange={(e) => {
                                            state.typeInfo === "passportInfo"
                                                ? setState({ ...state, addressPassPort: e.target.value })
                                                : setState({ ...state, addressCCCD: e.target.value });
                                        }}
                                    />
                                    <DesktopDatePicker
                                        label={`Ngày cấp ${state.typeInfo === "passportInfo" ? "Passport" : "CCCD"}`}
                                        inputFormat="DD/MM/YYYY"
                                        className="w-full"
                                        value={state.typeInfo === "passportInfo" ? dateReceivePassPort : dateReceiveCCCD}
                                        onChange={state.typeInfo === "passportInfo" ? handleChangeDatePassPort : handleChangeDateCCCD}
                                        renderInput={(params) => (<TextField variant="standard" {...params} size="small" />)}
                                    />
                                </div>

                                <div className="grid grid-cols-1 lg:grid-cols-2 gap-x-3 mt-5">
                                    <TextField
                                        type="text"
                                        id="outlined-basic"
                                        label="Số GPLX"
                                        variant="standard"
                                        size="small"
                                        className="w-full"
                                        required
                                        onChange={(e) => setState({ ...state, numberGPLX: e.target.value })}
                                    />
                                    <DesktopDatePicker
                                        label="Ngày cấp GPLX"
                                        inputFormat="DD/MM/YYYY"
                                        className="w-full"
                                        value={dateReceiveGPLX}
                                        onChange={handleChangeDateGPLX}
                                        renderInput={(params) => (<TextField variant="standard" {...params} size="small" />)}
                                    />
                                </div>

                                <div className="action mt-5">
                                    <Button
                                        size="small"
                                        variant="contained"
                                        startIcon={<AddIcon />}
                                        className="btnCreate"
                                        type="submit"
                                        onClick={handleSubmit}
                                    >
                                        Đăng ký
                                    </Button>
                                </div>
                            </Box>
                        </div>
                        <div className="col-span-3 flex items-center">
                            <img src={ImgRegister} alt="" />
                        </div>
                    </div>

                </div>
                <Snackbar
                    anchorOrigin={{ vertical: "top", horizontal: "right" }}
                    key={"top right"}
                    open={openNotify}
                    autoHideDuration={3000}
                    onClose={handleCloseNotify}
                >
                    {typeNotifi === "success" ? (
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: "100%" }}
                        >
                            {messageNotify}
                        </Alert>
                    ) : (
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: "100%" }}
                        >
                            {messageNotify}
                        </Alert>
                    )}
                </Snackbar>
            </div>
        </div>
    );
}

export default RegisterPage;
