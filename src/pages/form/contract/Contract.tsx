import React, { useState, useContext, FormEvent } from "react";
import Box from "@mui/material/Box";
import {
    Alert,
    Button,
    FormControl,
    FormLabel,
    InputLabel,
    MenuItem,
    Select,
    Snackbar,
    TextField,
    TextFieldProps,
} from "@mui/material";
import { DesktopDatePicker } from "@mui/x-date-pickers";
import moment from "moment";
import AuthContext from "context/AuthContext";
import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import { CarClassType } from "interface/car";
import { ContractGroupReq, ExpertiseInfo } from "interface/request/contractGroupReq";
import { useNavigate } from "react-router-dom";
import { endpoints } from "routers/endpoints";
import { contractGroupEndpoints } from "routers/apiEndpoints";
import { ConfigContext } from "provider";

const BaseInputField = (props: TextFieldProps) => (
    <TextField
        sx={{ my: 1 }}
        label={props.label}
        variant="outlined"
        className="w-full"
        required={props.required}
        onChange={props.onChange}
    />
);

function Contract(props: any) {

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            },
        },
    };

    const { auth } = useContext(AuthContext);
    const { configCar } = useContext(ConfigContext);

    const navigate = useNavigate()

    const [typeNotifi, setTypeNotifi] = useState("success");
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("");

    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === "clickaway") {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type);
        setMessageNotify(message);
        setOpenNofity(true);
    };

    const [state, setState] = useState<ContractGroupReq>();
    const [enableExpertiseInfo, setEnableExpertiseInfo] = useState<boolean>(true);
    const years: number[] = Array.from(
        { length: (2000 - moment().year()) / -1 + 1 },
        (_, i) => moment().year() + i * -1
    );

    const handleSubmitFormContract = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (!auth) return;

        if (state.expertiseInfo.isFirstTimeRent) {
            state.expertiseInfo.status = 0
        }
        state.rentFrom = moment(state.rentFrom).format('YYYY-MM-DD')
        state.rentTo = moment(state.rentTo).format('YYYY-MM-DD')

        // console.log("state => ", state)
        const res = await fetch(process.env.REACT_APP_API + contractGroupEndpoints.createContract, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + auth.accessToken,
            },
            body: JSON.stringify({ ...state, renterId: auth?.userProfile?.id }),
        });

        const data = await res.json();

        if (res.status > 200) {
            handleOpenNotify(data.message, "error")
            // console.log("Create-contract status ", res.status, data);
            return;
        }
        // console.log("create success => ", data);
        handleOpenNotify("Tạo hợp đồng thuê thành công!", "success");
        setTimeout(() => { navigate(-1); }, 1500)
    };

    return (

        <div className="form-page">
            <div className="container">
                <div className="bs bg-white p-5">
                    <h3 className="title text-center">Đăng ký thuê xe</h3>

                    <Box
                        component="form"
                        action="#"
                        method="POST"
                        noValidate
                        autoComplete="off"
                        onSubmit={handleSubmitFormContract}
                    >
                        <div className="grid grid-cols-1 md:grid-cols-2 gap-x-3">
                            <div>

                                <DesktopDatePicker
                                    className="w-full"
                                    label="Thuê từ ngày"
                                    inputFormat="DD/MM/YYYY"
                                    value={state?.rentFrom}
                                    onChange={(event) => setState({ ...state, rentFrom: event })}
                                    renderInput={(params) => (
                                        <TextField {...params} sx={{ my: 1 }} />
                                    )}
                                />

                                <DesktopDatePicker
                                    className="w-full"
                                    label="Đến ngày"
                                    inputFormat="DD/MM/YYYY"
                                    value={state?.rentTo}
                                    onChange={(event) => setState({ ...state, rentTo: event })}
                                    renderInput={(params) => (
                                        <TextField {...params} sx={{ my: 1 }} />
                                    )}
                                />

                                <BaseInputField
                                    label={"Mục đích thuê"}
                                    required
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            rentPurpose: e.target.value,
                                        })
                                    }
                                />

                                <FormControl required className="w-full" sx={{ my: "8px" }}>
                                    <InputLabel>Hãng xe</InputLabel>
                                    <Select
                                        MenuProps={MenuProps}
                                        value={state?.requireDescriptionInfo?.carBrand ?? ""}
                                        label={"Hãng xe"}
                                        onChange={(e) =>
                                            setState({
                                                ...state,
                                                requireDescriptionInfo: {
                                                    ...state?.requireDescriptionInfo,
                                                    carBrand: e.target.value
                                                }
                                            })
                                        }
                                    >
                                        {configCar?.carBrand?.sort().map((value) => (
                                            <MenuItem
                                                aria-selected="true"
                                                key={value}
                                                value={value}
                                            >
                                                {value}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>

                                <FormControl required className="w-full" sx={{ my: "8px" }}>
                                    <InputLabel>Đời xe</InputLabel>
                                    <Select
                                        MenuProps={MenuProps}
                                        value={state?.requireDescriptionInfo?.yearCreate ?? ""}
                                        label={"Đời xe"}
                                        onChange={(e) =>
                                            setState({
                                                ...state,
                                                requireDescriptionInfo: {
                                                    ...state?.requireDescriptionInfo,
                                                    yearCreate: Number(e.target.value)
                                                }
                                            })
                                        }
                                    >
                                        {years.map((x) => (
                                            <MenuItem key={x} value={x}>
                                                {x}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>

                                <FormControl required className="w-full" sx={{ my: "8px" }}>
                                    <InputLabel>Phân khúc xe</InputLabel>
                                    <Select
                                        value={state?.carClass ?? ""}
                                        label={"Phân khúc xe"}
                                        required
                                        onChange={(e) =>
                                            setState({
                                                ...state,
                                                carClass: Number(e.target.value),
                                            })
                                        }
                                    >
                                        {Object.keys(CarClassType)
                                            .filter((value) => isNaN(value as any))
                                            .map((value) => (
                                                <MenuItem
                                                    aria-selected="true"
                                                    key={value}
                                                    value={CarClassType[value as any]}
                                                >
                                                    {value}
                                                </MenuItem>
                                            ))}
                                    </Select>
                                </FormControl>

                                <FormControl required className="w-full" sx={{ my: "8px" }}>
                                    <InputLabel>Số chỗ</InputLabel>
                                    <Select
                                        value={state?.requireDescriptionInfo?.seatNumber ?? ""}
                                        label={"Số chỗ"}
                                        onChange={(e) =>
                                            setState({
                                                ...state,
                                                requireDescriptionInfo: {
                                                    ...state?.requireDescriptionInfo,
                                                    seatNumber: Number(e.target.value)
                                                }
                                            })
                                        }
                                    >
                                        {configCar?.carSeat.map((value) => (
                                            <MenuItem
                                                aria-selected="true"
                                                key={value}
                                                value={value}
                                            >
                                                {value} chỗ
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>

                                <FormControl required className="w-full" sx={{ my: "8px" }}>
                                    <InputLabel>Màu xe</InputLabel>
                                    <Select
                                        MenuProps={MenuProps}
                                        value={state?.requireDescriptionInfo?.carColor ?? ""}
                                        label={"Màu xe"}
                                        onChange={(e) =>
                                            setState({
                                                ...state,
                                                requireDescriptionInfo: {
                                                    ...state?.requireDescriptionInfo,
                                                    carColor: e.target.value
                                                }
                                            })
                                        }
                                    >
                                        {configCar?.carColor?.sort().map((value) => (
                                            <MenuItem
                                                aria-selected="true"
                                                key={value}
                                                value={value}
                                            >
                                                Màu {value}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </div>

                            <div>
                                <FormControl className="w-full" required>
                                    <FormLabel>Đã từng thuê xe</FormLabel>
                                    <RadioGroup
                                        row
                                        onChange={(e) => {
                                            setEnableExpertiseInfo(e.target.value === "true");
                                            setState({
                                                ...state,
                                                expertiseInfo: {
                                                    ...state?.expertiseInfo,
                                                    isFirstTimeRent: e.target.value === "true",
                                                },
                                            })
                                        }
                                        }
                                    >
                                        <FormControlLabel
                                            value={true}
                                            control={<Radio />}
                                            label="Thuê lần đầu"
                                        />
                                        <FormControlLabel
                                            value={false}
                                            control={<Radio />}
                                            label="Đã từng thuê"
                                        />
                                    </RadioGroup>
                                    <FormControl className="w-full" disabled={enableExpertiseInfo}>
                                        <InputLabel className="mt-[15px]">Thời gian thẩm định gần nhất</InputLabel>
                                        <Select
                                            className="mt-[15px] mb-2"
                                            value={state?.expertiseInfo?.status?.toString() ?? ""}
                                            label={"Thời gian thẩm định gần nhất"}
                                            required
                                            onChange={(event) =>
                                                setState({
                                                    ...state,
                                                    expertiseInfo: {
                                                        ...state?.expertiseInfo,
                                                        isFirstTimeRent: false,
                                                        status: Number(event.target.value),
                                                    },
                                                })
                                            }
                                        >
                                            {Object.keys(ExpertiseInfo)
                                                .filter((value) => isNaN(value as any))
                                                .map((value) => (
                                                    <MenuItem
                                                        aria-selected="true"
                                                        key={value}
                                                        value={ExpertiseInfo[value as any]}
                                                    >
                                                        {value}
                                                    </MenuItem>
                                                ))}
                                        </Select>
                                    </FormControl>
                                </FormControl>

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Facebook"
                                    variant="outlined"
                                    className="w-full"
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            customerSocialInfo: {
                                                ...state?.customerSocialInfo,
                                                facebook: e.target.value,
                                            },
                                        })
                                    }
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Zalo"
                                    variant="outlined"
                                    className="w-full"
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            customerSocialInfo: {
                                                ...state?.customerSocialInfo,
                                                zalo: e.target.value,
                                            },
                                        })
                                    }
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Linkedin"
                                    variant="outlined"
                                    className="w-full"
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            customerSocialInfo: {
                                                ...state?.customerSocialInfo,
                                                linkedin: e.target.value,
                                            },
                                        })
                                    }
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Mạng xã hội khác"
                                    variant="outlined"
                                    className="w-full"
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            customerSocialInfo: {
                                                ...state?.customerSocialInfo,
                                                other: e.target.value,
                                            },
                                        })
                                    }
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Thông tin công ty"
                                    variant="outlined"
                                    className="w-full"
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            companyInfo: e.target.value,
                                        })
                                    }
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Số điện thoại"
                                    variant="outlined"
                                    className="w-full"
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            relativeTel: e.target.value,
                                        })
                                    }
                                />

                                <TextField
                                    sx={{ my: 1 }}
                                    id="outlined-basic"
                                    label="Thông tin bổ sung"
                                    variant="outlined"
                                    className="w-full"
                                    onChange={(e) =>
                                        setState({
                                            ...state,
                                            addtionalInfo: e.target.value,
                                        })
                                    }
                                />
                            </div>
                        </div>
                        <div className="text-center mt-5">
                            <Button onClick={() => { navigate(-1) }} color="error" className="btn-choose-car" variant="contained">
                                HỦY BỎ
                            </Button>
                            <Button type="submit" className="btn-choose-car" sx={{ ml: 2 }} variant="contained">
                                HOÀN TẤT
                            </Button>
                        </div>
                    </Box>
                </div>
            </div >

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {
                    typeNotifi === "success"
                        ?
                        <Alert
                            color={"info"}
                            onClose={handleCloseNotify}
                            severity={"success"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                        :
                        <Alert
                            color={"error"}
                            onClose={handleCloseNotify}
                            severity={"error"}
                            sx={{ width: '100%' }}
                        >
                            {messageNotify}
                        </Alert>
                }
            </Snackbar>
        </div >
    );
}

export default Contract;