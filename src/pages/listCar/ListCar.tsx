import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import "./index.scss";
import { Car } from "../../interface/car";
import AuthContext from "context/AuthContext";
import { Snackbar, Alert, TextField } from "@mui/material";
import { DateTimePicker, DesktopDatePicker } from "@mui/x-date-pickers";
import moment, { Moment } from "moment";
import { carEndpoints } from "routers/apiEndpoints";

function Detail(props: any) {
    const { auth } = useContext(AuthContext);
    const [listCar, setListCar] = useState<Car[]>();
    const navigate = useNavigate();
    const onClickInfo = (id: string) => {
        if (!auth) {
            handleOpenNotify("Bạn cần phải đăng nhập để thuê xe", "error");
            return;
        }
        navigate(
            "/thue-xe" + "?" + new URLSearchParams({ idUser: auth.userProfile.id, idCar: id })
        );
    };

    const [typeNotifi, setTypeNotifi] = useState("success");
    const [openNotify, setOpenNofity] = useState(false);
    const [messageNotify, setMessageNotify] = useState("");
    const handleCloseNotify = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === "clickaway") {
            return;
        }
        setOpenNofity(false);
    };

    const handleOpenNotify = (message: string, type: string) => {
        setTypeNotifi(type);
        setMessageNotify(message);
        setOpenNofity(true);
    };

    useEffect(() => {
        fetch(process.env.REACT_APP_API + carEndpoints.base, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        })
            .then((res) => {
                if (res.status >= 400) throw new Error();
                return res.json();
            })
            .then((data) => {
                setListCar(data.data);
                // console.log(data.data);
            })
            .catch((err) => {
                // console.log(err)
            });
    }, [auth?.userProfile?.id]);

    const [value, setValue] = useState<Moment | null>(moment());
    const handleChange = (newValue: Moment | null) => {
        setValue(newValue);
    };

    return (
        <div className="form-page">
            <div className="detail">
                <div className="container">
                    <div className="bg-white rounded-md bs">
                        <div className="container">
                            <div className="grid grid-cols-1 lg:grid-cols-3 bg-white p-[20px] m-auto rounded-md ">
                                <div className="lg:mr-3 mb-[30px] lg:mb-0">
                                    <DateTimePicker
                                        label="Ngày bắt đầu"
                                        value={value}
                                        onChange={handleChange}
                                        renderInput={(params) => <TextField {...params} />}
                                    />
                                </div>
                                <div className="lg:mr-3 mb-[30px] lg:mb-0">
                                    <DateTimePicker
                                        label="Ngày kết thúc"
                                        value={value}
                                        onChange={handleChange}
                                        renderInput={(params) => <TextField {...params} />}
                                    />
                                </div>

                                <div className="btn-book-car text-right">
                                    <button>ĐẶT NGÀY</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="grid grid-cols-1 md:grid-cols-4 md:gap-4">
                        <section className="md:mt-[50px] order-2 md:order-1">
                            <div className="bg-white rounded-md bs p-5 mb-5">
                                <h3 className="title">thương hiệu</h3>
                                <FormGroup>
                                    <FormControlLabel control={<Checkbox />} label="BMW" />
                                    <FormControlLabel control={<Checkbox />} label="Lamborghini" />
                                    <FormControlLabel control={<Checkbox />} label="Mazda" />
                                    <FormControlLabel control={<Checkbox />} label="Toyota" />
                                    <FormControlLabel control={<Checkbox />} label="Suzuki" />
                                </FormGroup>
                            </div>
                            <div className="bg-white rounded-md bs p-5 mb-5">
                                <h3 className="title">chỗ ngồi</h3>
                                <FormGroup>
                                    <FormControlLabel control={<Checkbox />} label="4 Chỗ" />
                                    <FormControlLabel control={<Checkbox />} label="7 Chỗ" />
                                    <FormControlLabel control={<Checkbox />} label="16 Chỗ" />
                                </FormGroup>
                            </div>
                            <div className="bg-white rounded-md bs p-5 mb-5">
                                <h3 className="title">phân khúc</h3>
                                <FormGroup>
                                    <FormControlLabel control={<Checkbox />} label="A" />
                                    <FormControlLabel control={<Checkbox />} label="B" />
                                    <FormControlLabel control={<Checkbox />} label="C" />
                                    <FormControlLabel control={<Checkbox />} label="D" />
                                </FormGroup>
                            </div>
                            <div className="bg-white rounded-md bs p-5">
                                <h3 className="title">đời xe</h3>
                                <FormGroup>
                                    <FormControlLabel control={<Checkbox />} label="2018" />
                                    <FormControlLabel control={<Checkbox />} label="2019" />
                                    <FormControlLabel control={<Checkbox />} label="2020" />
                                    <FormControlLabel control={<Checkbox />} label="2021" />
                                    <FormControlLabel control={<Checkbox />} label="2022" />
                                </FormGroup>
                            </div>
                        </section>
                        <section className="mt-[50px] col-span-3 order-1 md:order-2">
                            {listCar?.map((item) => {
                                return (
                                    <div key={item?.id} className="rounded-md p-5 mb-5 bs bg-white">
                                        <div className="grid grid-cols-1 md:grid-cols-3 gap-5 ">
                                            <div className="h-[200px]">
                                                <img
                                                    src="https://i1-vnexpress.vnecdn.net/2021/09/18/TOPBMWX1NOWM7792jpg-1631954512.jpg?w=500&h=300&q=100&dpr=1&fit=crop&s=Di90w0yDDhbOMf9JE4Xm_Q&t=image"
                                                    alt=""
                                                    className="w-full h-full object-contain"
                                                />
                                            </div>
                                            <div>
                                                <h3 className="text-[1.5rem] uppercase font-bold text-center md:text-left">
                                                    BMW
                                                </h3>
                                                <p>Hãng xe: {item?.descriptionInfo?.carBrand}</p>
                                                <p>Màu: {item?.descriptionInfo?.carColor}</p>
                                                <p>Số chỗ: {item?.descriptionInfo?.seatNumber}</p>
                                                <p>Phân khúc: {item?.carClass}</p>
                                                <p className="mt-3 font-bold">
                                                    Giá Thuê:
                                                    {item?.generalInfo?.priceForNormalDay}
                                                    <span className="ml-3">VND/ngày</span>
                                                </p>
                                            </div>
                                            <div className="flex justify-end items-end md:flex-col">
                                                <p>
                                                    <button
                                                        className="btn-choose-car"
                                                        onClick={() => {
                                                            onClickInfo(item.id);
                                                        }}
                                                    >
                                                        CHỌN XE
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </section>
                    </div>
                    <section className="mt-[50px]">
                        <h3 className="title">Thuê xe chất lượng cao và ưu đãi nhất</h3>
                        <div>
                            <div className="gird-layout scroll-custom">
                                <div className="scroll-snap-start">
                                    <div className="bg-white rounded-md">
                                        <img
                                            src="https://storage.googleapis.com/vex-config/cms-tool/post/images/159/img_card.png"
                                            alt=""
                                            className="w-full h-full object-cover rounded-t-md"
                                        />
                                        <h3 className="px-3 pt-2 text-[1rem] font-bold two-line h-[60px] md:h-[65px]">
                                            Dành cho KH mới - Giảm 25% khi đặt Dành cho KH mới -
                                            Giảm 25% khi đặt
                                        </h3>
                                    </div>
                                </div>
                                <div className="scroll-snap-start">
                                    <div className="bg-white rounded-md">
                                        <img
                                            src="https://storage.googleapis.com/vex-config/cms-tool/post/images/157/img_card.png"
                                            alt=""
                                            className="w-full h-full object-cover rounded-t-md"
                                        />
                                        <h3 className="px-3 pt-2 text-[1rem] font-bold two-line h-[60px] md:h-[65px]">
                                            Dành cho KH mới - Giảm 25% khi đặt
                                        </h3>
                                    </div>
                                </div>
                                <div className="scroll-snap-start">
                                    <div className="bg-white rounded-md">
                                        <img
                                            src="https://storage.googleapis.com/vex-config/cms-tool/post/images/123/img_card.png"
                                            alt=""
                                            className="w-full h-full object-cover rounded-t-md"
                                        />
                                        <h3 className="px-3 pt-2 text-[1rem] font-bold two-line h-[60px] md:h-[65px]">
                                            Dành cho KH mới - Giảm 25% khi đặt
                                        </h3>
                                    </div>
                                </div>
                                <div className="scroll-snap-start">
                                    <div className="bg-white rounded-md">
                                        <img
                                            src="https://storage.googleapis.com/vex-config/cms-tool/post/images/123/img_card.png"
                                            alt=""
                                            className="w-full h-full object-cover rounded-t-md"
                                        />
                                        <h3 className="px-3 pt-2 text-[1rem] font-bold two-line h-[60px] md:h-[65px]">
                                            Dành cho KH mới - Giảm 25% khi đặt
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <Snackbar
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                key={"top right"}
                open={openNotify}
                autoHideDuration={3000}
                onClose={handleCloseNotify}
            >
                {typeNotifi === "success" ? (
                    <Alert
                        color={"info"}
                        onClose={handleCloseNotify}
                        severity={"success"}
                        sx={{ width: "100%" }}
                    >
                        {messageNotify}
                    </Alert>
                ) : (
                    <Alert
                        color={"error"}
                        onClose={handleCloseNotify}
                        severity={"error"}
                        sx={{ width: "100%" }}
                    >
                        {messageNotify}
                    </Alert>
                )}
            </Snackbar>
        </div>
    );
}

export default Detail;
