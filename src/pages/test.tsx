import { useEffect, useState } from "react";

export default function Testttt() {
  const [html, setHtml] = useState<string>();

  useEffect(() => {
    fetch(process.env.REACT_APP_API + "/test/test", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        groupId: "5e4a3168-be27-4407-8249-0db61d1734f0",
        previewType: 0,
      }),
    })
      .then((res) => res.text())
      .then((data) => setHtml(data));
  }, []);
  return (
    <>
      <iframe
        style={{
          width: "100vw",
          height: "100vh",
          background: "white",
        }}
        // src={"data:text/html," + encodeURIComponent(html)}
        srcDoc={html}
      />
    </>
  );
}
