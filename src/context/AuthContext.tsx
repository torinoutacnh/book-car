import React, { createContext, useState, useEffect } from "react";
import { gapi } from "gapi-script";
import {
  GoogleLoginResponse,
  GoogleLoginResponseOffline,
} from "react-google-login";
import { Auth } from "interface/authenticate";
import Cookies from "js-cookie";
import { ConfigCar } from "interface/car";
import { useQuery } from "react-query";
import { userEndpoints } from "routers/apiEndpoints";

interface Context {
  auth: Auth;
  LogOutAuth: any;
  loginAuth: any;
  onSuccess: (
    response: GoogleLoginResponse | GoogleLoginResponseOffline
  ) => Promise<void>;
}

const AuthContext = createContext<Context>({
  auth: null,
  LogOutAuth: null,
  onSuccess: null,
  loginAuth: null

});

const clientId = process.env.REACT_APP_CLIENT_ID;

const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

function AuthProvider(props: { children: any }) {
  const [auth, setAuth] = useState<Auth>();

  const LogOutAuth = () => {
    setAuth(null);
    Cookies.remove("user-auth", { path: "" });
  };

  const loginAuth = (a: Auth) => {
    setAuth(a)
  }

  function SaveAuthToCookie(data: any) {
    const expired = new Date(new Date().getTime() + 60 * 60 * 1000);
    Cookies.set("user-auth", JSON.stringify(data), {
      path: "",
      expires: expired,
    });
  }

  async function onSuccess(
    response: GoogleLoginResponse | GoogleLoginResponseOffline
  ): Promise<void> {
    const gbj = (response as GoogleLoginResponse).profileObj;
    const res = await fetch(process.env.REACT_APP_API + userEndpoints.auth, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer my-token",
      },
      body: JSON.stringify({
        userName: gbj.email,
        password: gbj.googleId.substring(0, 20),
      }),
    });

    const data = await res.json();
    if (res.status >= 400 && res.status !== 404) throw new Error(data.message);

    if (res.status === 404) {
      const newUser = {
        name: gbj.name,
        tel: "",
        job: "",
        currentAddress: "",
        email: gbj.email,
        passWord: gbj.googleId.substring(0, 20),
        citizenIdentificationInfo: {
          number: "",
          address: "",
          dateReceive: new Date(),
        },
        passportInfo: {
          number: "",
          address: "",
          dateReceive: new Date(),
        },
        drivingLicenseInfo: {
          number: "",
          dateReceive: new Date(),
        },
      };

      await sleep(2000);

      const createUserRes = await fetch(
        process.env.REACT_APP_API + userEndpoints.createUser,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer my-token",
          },
          body: JSON.stringify(newUser),
        }
      );

      const createUserData = await createUserRes.json();
      if (createUserRes.status >= 400) throw new Error(createUserData.message);
      setAuth(createUserData);
      return;
    }

    setAuth(data);
  }

  useEffect(() => {
    const initClient = () => {
      gapi.client.init({
        clientId: clientId,
        scope: "",
      });
    };
    gapi.load("client:auth2", initClient);
  });

  useEffect(() => {
    const cookie = Cookies.get("user-auth");
    if (cookie && !auth) {
      const user = JSON.parse(cookie);
      setAuth(user);
    }
  }, []);

  useEffect(() => {
    const cookie = Cookies.get("user-auth");
    if (auth && !cookie) SaveAuthToCookie(auth);
  }, [{ ...auth }]);

  return (
    <AuthContext.Provider value={{ auth, LogOutAuth, onSuccess, loginAuth }}>
      {props.children}
    </AuthContext.Provider>
  );
}

export default AuthContext;
export { AuthProvider };
