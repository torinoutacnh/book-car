export interface ContractFile {
    id?: string,
    path?: string,
    citizenIdentifyImage1?: string,
    citizenIdentifyImage2?: string,
    drivingLisenceImage1?: string,
    drivingLisenceImage2?: string,
    housePaperImages?: string[],
    passportImages?: string[],
    otherImages?: string[]
}