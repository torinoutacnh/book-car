import { User } from "./authenticate";
import { Car } from "./car";
import { ContractFile } from "./contractFile";
import { ExpertiseContract } from "./expertiseContract";
import { ReceiveContract } from "./receiveContract";
import { RentContract } from "./rentContract";
import { TransferContract } from "./transferContract";

export interface Contract {
  id?: string,
  rentFrom?: Date,
  rentTo?: Date,
  carClass?: number,
  requireDescriptionInfo?: {
    carBrand?: string,
    seatNumber?: number,
    yearCreate?: number,
    carColor?: string,
  },
  rentPurpose?: string;
  expertiseInfo?: {
    isFirstTimeRent?: boolean,
    status: number
  },
  customerSocialInfo?: {
    facebook?: string,
    zalo?: string,
    linkedin?: string,
    other?: string
  };
  companyInfo?: string;
  relativeTel?: string;
  addtionalInfo?: string;
  status?: RentStatus;
  car?: Car;
  renterId?: string,
  renter?: User,
  contractFile?: ContractFile;
  expertiseContract?: ExpertiseContract;
  rentContract?: RentContract;
  transferContract?: TransferContract;
  receiveContract?: ReceiveContract;
  expertiser?: User;
  representative?: User;
  transferer?: User;
  receiver?: User;
}

export enum RentStatus {
  "Chưa thẩm định" = 0,
  "Đã thẩm định" = 1,
  "Đã hoàn thành HĐ thuê" = 2,
  "Đã hoàn thành HĐ giao xe" = 3,
  "Đã hoàn thành HĐ nhận xe" = 4,
}