import { User } from "./authenticate";

export interface DescriptionInfoCar {
  carBrand?: string,
  carName?: string,
  carVersion?: string,
  carStyle?: string,
  carTransmission?: string,
  carFuelUse?: string,
  seatNumber?: number,
  yearCreate?: number,
  carColor?: string,
  carNumber?: string,
  carDescription?: string,
}

export interface GeneralInfoCar {
  priceForNormalDay?: number,
  priceForWeekendDay?: number,
  priceForHoliday?: number,
  priceForMonth?: number,
  limitedKmForMonth?: number,
  pricePerKmExceed?: number,
  pricePerHourExceed?: number,
}

export interface CarState {
  speedometerNumber?: number,
  fuelPercent?: number,
  currentEtcAmount?: number,
  carStatusDescription?: string,
}

export interface CarCreateConfig {
  carClass?: number | string,
  generalInfo?: GeneralInfoCar
}

export interface ConfigCar {
  id?: string,
  createdDate?: string,
  createBy?: string,
  updatedDate?: string,
  updateBy?: string,
  isDeleted?: boolean
  carCreate?: CarCreateConfig[],
  carBrand?: string[],
  carStyle?: string[],
  carColor?: string[],
  carSeat?: string[],
  carFuelUse?: string[],
  carParkingSlot?: string[],
  carTransmission?: string[],
}

// export interface SchedulesCar {
//   id?: string,
//   dateStart?: Date,
//   dateEnd?: Date,
//   car?: Car,
//   user?: User
// }

export interface XPagination {
  TotalCount: number,
  PageSize: number,
  CurrentPage: number,
  TotalPages: number,
  HasPrevious: boolean,
  HasNext: boolean
}

export interface Car {
  id?: string,
  carClass?: number | string,
  carParkingLot?: string,
  descriptionInfo?: DescriptionInfoCar,
  generalInfo?: GeneralInfoCar,
  loanerId?: string,
  carImage?: string | FileList | File,
  loaner?: User,
  carState?: CarState,
  carSchedules?: any[],
}

export enum CarClassType {
  "A" = 0,
  "B" = 1,
  "C" = 2,
  "D" = 3,
}

export enum SeatNumber {
  "4 chỗ" = 4,
  "7 chỗ" = 7,
  "16 chỗ" = 16
}

