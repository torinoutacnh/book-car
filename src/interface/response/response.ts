export interface ApiResponse<T> {
    data: T;
    additionData: any;
}

export interface ErrorResponse {
    message: string;
    code: number;
}