export interface ContractCar {
    carId?: string;
    rentId?: string;
    rentFrom?: Date;
    rentTo?: Date;
    carType?: string;
    rentSchedule?: string;
    rentPurpose?: string;
    expertiseInfo?: {
        isFirstTimeRent?: boolean;
        status?: number;
    };
    customerSocialInfo?: {
        zalo?: string;
        facebook?: string;
        linkedin?: string;
        other?: string;
    };
    companyInfo?: string;
    relativeTel?: string;
    addtionalInfo?: string;
    status?: number;
}
