export interface Auth {
  accessToken?: string;
  refreshToken?: number;
  userProfile?: {
    id?: string;
    name?: string;
    tel?: string;
    job?: string;
    currentAddress?: string;
    email?: string;
    passWord?: string;
    role?: RoleUser;
    citizenIdentificationInfo?: {
      number?: string;
      address?: string;
      dateReceive?: Date;
    };
    passportInfo?: {
      number?: string;
      address?: string;
      dateReceive?: Date;
    };
    drivingLicenseInfo?: {
      number?: string;
      dateReceive?: Date;
    };
  };
}

export interface User {
  accessToken?: string;
  refreshToken?: number;
  id?: string;
  name?: string;
  tel?: string;
  job?: string;
  currentAddress?: string;
  email?: string;
  passWord?: string;
  role?: RoleUser;
  citizenIdentificationInfo?: {
    number?: string;
    address?: string;
    dateReceive?: Date;
  };
  passportInfo?: {
    number?: string;
    address?: string;
    dateReceive?: Date;
  };
  drivingLicenseInfo?: {
    number?: string;
    dateReceive?: Date;
  };
}

export enum RoleUser {
  "Customer" = 0,
  "ExpertiseStaff" = 1,
  "Admin" = 2,
  "OperatorStaff" = 3,
  "SaleStaff" = 4
}

export enum VieRole {
  "Khách hàng" = 0,
  "NV thẩm định" = 1,
  "Quản lí" = 2,
  "NV điều hành" = 3,
  "NV sale" = 4
}