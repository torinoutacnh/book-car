export interface TransferContract {
    contractGroupId?: string,
    dateTransfer?: string,
    currentCarState?: {
        speedometerNumber?: number,
        fuelPercent?: number,
        currentEtcAmount?: number,
        carStatusDescription?: string
    },
    transferItem?: {
        paper?: string,
        standbyDevice?: string,
        standbyDeviceInfo?: string
    },
    filePath?: string,
    customerSignature?: string,
    staffSignature?: string,
    isExported?: boolean,
    status?: number,
    outsideCarImage?: string[],
    insideCarImage?: string[]
}