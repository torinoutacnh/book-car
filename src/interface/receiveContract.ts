export interface ReceiveContract {
    contractGroupId?: string,
    dateReceive?: string,
    currentCarState?: {
        speedometerNumber?: number,
        fuelPercent?: number,
        currentEtcAmount?: number,
        carStatusDescription?: string
    },
    receiveItem?: {
        paper?: string,
        standbyDevice?: string,
        standbyDeviceInfo?: string
    },
    outSideCarImage?: string[],
    insideCarImage?: string[],
    customerSignature?: string,
    staffSignature?: string,
    isExported?: boolean,
    filePath?: string,
}