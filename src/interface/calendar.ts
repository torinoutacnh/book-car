import { Car } from "./car";

export interface Calendar {
    id: string,
    rentFrom: string,
    rentTo: string,
    dateTransfer: string,
    dateReceive: string,
    car: Car
}