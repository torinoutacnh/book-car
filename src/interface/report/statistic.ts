export interface Statistic {
    id?: string,
    rentFrom?: string,
    rentTo?: string,
    currentCarStateInfo?: {
        speedometerNumber?: number,
        fuelPercent?: number,
        currentEtcAmount?: number,
        carStatusDescription?: string,
    },
    transferCarStateInfo?: {
        speedometerNumber?: number,
        fuelPercent?: number,
        currentEtcAmount?: number,
        carStatusDescription?: string,
    },
    totalday?: number
}