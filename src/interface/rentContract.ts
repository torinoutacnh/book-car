import { DescriptionInfoCar, GeneralInfoCar } from "./car";

export interface RentContract {
    id?: string,
    rentFrom?: Date,
    rentTo?: Date,
    carGeneralInfoAtRent?: GeneralInfoCar,
    carDescriptionInfoAtRent?: DescriptionInfoCar,
    paymentAmount?: number,
    depositItem?: {
        description?: string,
        asset?: string,
        downPayment?: number,
    }
    customerSignature?: string,
    staffSignature?: string,
    filePath?: string,
    fileWithSignsPath?: string,
    isExported?: boolean,
    status?: number
}