export interface ContractGroupReq {
    renterId?: string,
    rentFrom?: string,
    rentTo?: string,
    carClass?: number,
    requireDescriptionInfo?: {
        carBrand?: string,
        seatNumber?: number,
        yearCreate?: number,
        carColor?: string,
    },
    rentPurpose?: string,
    expertiseInfo?: {
        isFirstTimeRent: boolean,
        status?: ExpertiseStatus,
    }
    customerSocialInfo?: {
        facebook?: string,
        zalo?: string,
        linkedin?: string,
        other?: string,
    }
    companyInfo?: string,
    relativeTel?: string,
    addtionalInfo?: string,
};

export enum ExpertiseInfo {
    "Chưa thẩm định" = 0,
    "Thẩm định dưới 3 tháng" = 1,
    "Thẩm định trên 3 tháng" = 2,
    "Thẩm định thất bại" = 3,
}

export enum ExpertiseStatus {
    "Đạt" = 0,
    "Thất bại" = 1,
    "Cần thêm thông tin" = 2,
    "Khác" = 3,
}