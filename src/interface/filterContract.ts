import { Moment } from "moment"
import { CarClassType } from "./car"
import { RentStatus } from "./contract"

export interface FilterContract {
    query?: {
        carNumber?: string | null,
        email?: string | null,
        carClass?: CarClassType | null,
        rentStatus?: RentStatus | null,
        fromDate?: Moment | null
    }
    pageModel: {
        pageNumber: number,
        pageSize: number
    }
}