export interface ExpertiseContract {
    id?: string,
    carId?: string,
    expertiseDate?: Date,
    description?: string,
    result?: number,
    resultOther?: number,
    trustLevel?: number,
    paymentAmount?: number,
    depositInfo?: {
        description?: string,
        asset?: string,
        downPayment?: number
    },
    isExported?: boolean
}