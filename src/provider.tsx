import Layout from "./components/layout";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AuthProvider } from "context/AuthContext";
import Routers from "routers/Routers";
import { QueryClient, QueryClientProvider } from "react-query";
import { createContext, useEffect, useState } from "react";
import { ConfigCar } from "interface/car";
import { publicEndpoints } from "routers/apiEndpoints";

interface IConfigContext {
  configCar: ConfigCar,
  refetchConfig: any
}

export const ConfigContext = createContext<IConfigContext>({ configCar: null, refetchConfig: null })

const Provider = (props: { children: JSX.Element }) => {
  const queryClient = new QueryClient();

  // const {auth} = useContext(AuthContext)
  const [state, setState] = useState<ConfigCar>()
  const [count, setCount] = useState(0)

  useEffect(() => {
    (async () => {
      const res = await fetch(process.env.REACT_APP_API + publicEndpoints.config, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          // Authorization: "Bearer " + auth.accessToken,
        },
      })
      const data = await res.json();

      if (!res.ok) {
        return
      }
      setState(data.data)
    })()
  }, [count])

  const refetch = () => {
    setCount(count + 1)
  }

  return (
    <AuthProvider>
      <ConfigContext.Provider value={{ configCar: state, refetchConfig: refetch }}>
        <QueryClientProvider client={queryClient}>
          <LocalizationProvider dateAdapter={AdapterMoment}>
            {props.children}
          </LocalizationProvider>
        </QueryClientProvider>
      </ConfigContext.Provider>
    </AuthProvider>
  );
};

export { Provider };


