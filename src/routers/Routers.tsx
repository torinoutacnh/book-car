import React, { useContext } from "react";
import { Routes, Route } from "react-router-dom";
import Home from "pages/home";
import ListCar from "pages/listCar/ListCar";
import ContractManagement from "pages/contractManagement/ContractManagement";
import Assessment from "pages/form/assessment/Assessment";
import Contract from "pages/form/contract/Contract";
import VehicleHanding from "pages/form/vehicleHanding/VehicleHanding";
import ReceiveCar from "pages/form/receiveCar/receiveCar";
import { AdminIndex } from "components/admin";
import { UserInfo } from "components/admin/userInfo";
import { SignContractComponent } from "components/Word/signContract";
import { ContractInfo } from "components/admin/contractInfo";
import UserPage from "components/users";
import { adminEndpoints, endpoints } from "./endpoints";
import ContractDetail from "components/users/infoContract/ContractDetail";
import { ManagerUser } from "components/admin/managerUser";
import { ManagerContract } from "components/admin/managerContract";
import { ManagerCar } from "components/admin/managerCar";
import { CarInfo } from "components/admin/carInfo";
import ConfigPage from "components/configPage/configPage";
import Testttt from "pages/test";
import { WordContractPreview } from "components/Word/WordContractPreview";
import LoginPage from "pages/loginPage/loginPage";
import RegisterPage from "pages/loginPage/registerPage";
import InfoContract from "components/users/infoContract/infoContract";
import { WordContractPath } from "components/Word/WordContractPath";
import { AdminCalendar } from "components/calendar";
import AuthContext from "context/AuthContext";
import Statistical from "components/statistical/statistical";

export default function Routers() {
    const { auth } = useContext(AuthContext)
    return (
        <Routes>
            <Route path={endpoints.home} element={<Home />} />

            <Route path={endpoints.listCar} element={<ListCar />} />

            <Route path={endpoints.assessment} element={<Assessment />} />

            <Route path={endpoints.orderManagement} element={<ContractManagement />} />

            <Route path={endpoints.contractManagement} element={<ContractManagement />} />

            <Route path={endpoints.rent_car} element={<Contract />} />

            <Route path={endpoints.contract_detail} element={<ContractDetail />} />

            <Route path={endpoints.giao_xe} element={<VehicleHanding />} />

            <Route path={endpoints.nhan_xe} element={<ReceiveCar />} />

            <Route path={endpoints.loginPage} element={!auth ? <LoginPage /> : <Home />} />

            <Route path={endpoints.registerPage} element={!auth ? <RegisterPage /> : <Home />} />

            <Route path={endpoints.infoContract} element={<InfoContract />} />

            <Route path={adminEndpoints.base} element={<AdminIndex />}>
                <Route path={adminEndpoints.default} element={<ManagerCar />} />
                <Route path={adminEndpoints.car} element={<ManagerCar />} />
                <Route path={adminEndpoints.carInfo} element={<CarInfo />} />
                <Route path={adminEndpoints.user} element={<ManagerUser />} />
                <Route path={adminEndpoints.userInfo} element={<UserInfo />} />
                <Route path={adminEndpoints.contract} element={<ManagerContract />} />
                <Route path={adminEndpoints.contractInfo} element={<ContractInfo />} />
                <Route path={adminEndpoints.calendar} element={<AdminCalendar />} />
                <Route path={adminEndpoints.config} element={<ConfigPage />} />
                <Route path={adminEndpoints.maintenance} />
                <Route path={adminEndpoints.statistical} element={<Statistical />} />
            </Route>

            <Route
                path="/test"
                element={
                    <>
                        <WordContractPath />
                    </>
                }
            />

            <Route path={endpoints.userPage} element={<UserPage />} />
        </Routes>
    );
}

export const MenuRouters = [
    {
        display: "Trang chủ",
        path: "/",
    },
    {
        display: "Thuê xe 4 chỗ",
        path: "/danh-sach-xe",
    },
    {
        display: "Thuê xe 7 chỗ",
        path: "/danh-sach-xe",
    },
    {
        display: "Thuê xe 16 chỗ",
        path: "/danh-sach-xe",
    },
];
