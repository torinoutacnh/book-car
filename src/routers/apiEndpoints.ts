export const carEndpoints = {
    base: "/car",
    filterByGroup: "/car/get-by-group/",
    carInfo: "/car/",
    createCar: "/car/create",
    updateCar: "/car/update/",
    deleteCar: "/car/delete/"
}

export const contractGroupEndpoints = {
    base: "/contractgroup",
    filterByUser: "/contractgroup/user/",
    contractInfo: "/contractgroup/",
    createContract: "/contractgroup/create-contractgroup",
    updateContract: "/contractgroup/update-contractgroup/",
    deleteContract: "/contractgroup/delete/",
    createFile: "/contractgroup/create-file",
    updateFile: "/contractgroup/update-file/",
    createExpertise: "/contractgroup/create-expertise",
    updateExpertise: "/contractgroup/update-expertise",
    exportExpertise: "/contractgroup/export-expertise-contract/",
    updateRent: "/contractgroup/update-rent-contract/",
    exportRent: "/contractgroup/export-rent-contract/",
    deleteRent: "/contractgroup/delete-rent-contract/",
    previewRent: "/preview-rent-contract/",
    createTransfer: "/contractgroup/create-transfer-contract",
    updateTransfer: "/contractgroup/update-transfer-contract/",
    exportTransfer: "/contractgroup/export-transfer-contract/",
    deleteTransfer: "/contractgroup/delete-transfer-contract/",
    previewTransfer: "/preview-transfer-contract/",
    createReceive: "/contractgroup/create-receive-contract",
    updateReceive: "/contractgroup/update-receive-contract/",
    exportReceive: "/contractgroup/export-receive-contract/",
    deleteReceive: "/contractgroup/delete-receive-contract/",
    previewReceive: "/preview-receive-contract/",
    staffSign: "/contractgroup/staff-sign",
    customerSign: "/contractgroup/customer-sign"
}

export const userEndpoints = {
    base: "/user",
    userInfo: "/user/",
    auth: "/user/authenticate",
    createUser: "/user/create",
    updateUser: "/user/update-info/",
    updateRole: "/user/update-role/",
    deleteUser: "/user/delete/",
    updatePassword: "/user/reset-password"
}

export const publicEndpoints = {
    config: "/config",
    getReportContract: "/report/get-contract-report",
    getCarReport: "/report/get-car-report",
    getCarStatistic: "/report/get-car-statistic-by-MonthYear",
    updateBrand: "/config/update-car-brand",
    updateStyle: "/config/update-car-style",
    updateColor: "/config/update-car-color",
    updateSeat: "/config/update-car-seat",
    updateFuelUse: "/config/update-car-fuel-use",
    updateTransmission: "/config/update-car-transmission",
    updateParkingLot: "/config/update-car-parking-lot",
    updateCarCreate: "/config/update-car-create"
}