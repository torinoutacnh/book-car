export const endpoints = {
    home: "",
    rent_car: "thue-xe",
    contract_detail: "contract-progress/:groupId",
    testing: "testing",
    listCar: "danh-sach-xe",
    assessment: "tham-dinh",
    orderManagement: "quan-ly-don-hang",
    contractManagement: "quan-ly-don-hang",
    giao_xe: "giao-xe",
    nhan_xe: "nhan-xe",
    userPage: "user",
    loginPage: "dang-nhap",
    registerPage: "dang-ky",
    infoContract: "thong-tin-hop-dong"
};

export const adminEndpoints = {
    base: "admin",
    default: "",
    car: "car",
    carInfo: "car/car-information",
    user: "user",
    userInfo: "user/user-information",
    contract: "contract",
    contractInfo: "contract/contract-information",
    calendar: "lich-thue-xe",
    config: "config",
    contractNotFound: "khum-tim-thay-hop-dong",
    maintenance: "bao-duong",
    statistical: "thong-ke",
};
