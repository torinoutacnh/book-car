import React from "react";
import "./App.scss";
import Routers from "routers/Routers";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "provider";
import Layout from "components/layout";

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Provider>
                    <Layout>
                        <Routers />
                    </Layout>
                </Provider>
            </BrowserRouter>
        </div>
    );
}

export default App;